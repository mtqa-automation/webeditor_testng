/**
 * 
 */
package com.setupfc.qa.pages.BaseEditor;

import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;


import com.setupfc.qa.util.TestUtil;

/**
 * @author smallika
 *
 */
public class ReferenceWindow extends TestUtil {

	//Initializing the page object 
		public ReferenceWindow()
		{
			PageFactory.initElements(driver, this);
		}
		
	//OR:
		@FindBy(className = "ui-dialog-title")
		private WebElement Headertitle;
		
		@FindBy(id = "inputSpecId")
		private WebElement SpecID_Txtfld;
		
		@FindBy(id = "selectClass")
		private WebElement ClassDropDown;
		
		@FindBy(id = "selectSubClass")
		private WebElement SubClassDropDown;
		
		@FindBy(id = "selectDepartment")
		private WebElement DepartmentDropdown;
		
		@FindBy(id = "selectKeywordType")
		private WebElement KeywordTypeDropdown;
		
		@FindBy(id = "selectKeyword")
		private WebElement KeywordDropdown;
		
		@FindBy(id = "start-search")
		private WebElement SearchBtn;
		
		@FindBy(xpath = "//button[@class='btn btn-secondary']")
		private WebElement CancelBtn;
		
		@FindBy(id = "add-ref-ok")
		private WebElement OkBtn;
		
		@FindBy(xpath="//div[contains(@class,'spinner-border')]")
		private WebElement LoadingSpinner;
		
		@FindBy(xpath = "//span[@class='pi pi-plus']")
		private WebElement ExpandSearchParameter;

		@FindBy(xpath = "//span[@class='pi pi-minus']")
		private WebElement ShrinkSearchParameter;
		
		@FindBy(xpath ="//thead[@class='ui-table-thead']//th")
		private List<WebElement> ReferenceColumnNames;
		
		@FindBy(xpath ="//tbody[@class='ui-table-tbody']//tr")
		private List<WebElement> References;
		
		@FindBy(xpath ="//tbody[@class='ui-table-tbody']//tr/td[1]")
		private List<WebElement> ReferencesID;
		
		@FindBy(xpath ="//tbody[@class='ui-table-tbody']//tr[1]/td")
		private List<WebElement> FirstReferenceDetails;
		
//Actions:
		public String GetHeaderTitle()
		{
			return Headertitle.getText();
		}
		
		public boolean SearchReferenceByID(String SpecID)
		{
			if(ExpandSearchParameter.isDisplayed())
				ExpandSearchParameter.click();
			SpecID_Txtfld.sendKeys(SpecID);
			SearchBtn.click();
			return true;
			
		}
		public boolean SelectClass(String ClassID)
		{
			if(ExpandSearchParameter.isDisplayed())
				ExpandSearchParameter.click();
			Select options=new Select(ClassDropDown);
			options.selectByVisibleText(ClassID);
			WaitforElementInVisible(LoadingSpinner);
			return true;
		}
		public boolean SelectSubClass(String Subclass)
		{
			if(ExpandSearchParameter.isDisplayed())
				ExpandSearchParameter.click();
			Select options=new Select(SubClassDropDown);
			options.selectByVisibleText(Subclass);
			return true;
		}
		public boolean SelectDepartment(String Depart)
		{
			if(ExpandSearchParameter.isDisplayed())
				ExpandSearchParameter.click();
			Select options=new Select(DepartmentDropdown);
			options.selectByVisibleText(Depart);
			return true;
		}
		public boolean SelectKeyWord(String Keyword)
		{
			if(ExpandSearchParameter.isDisplayed())
				ExpandSearchParameter.click();
			Select options=new Select(KeywordTypeDropdown);
			options.selectByVisibleText(" EquipmentID");
			WaitforElementInVisible(LoadingSpinner);
			Select options1=new Select(KeywordTypeDropdown);
			options1.selectByVisibleText(Keyword);
			return true;
		}
		
		public String GetRefernceDetails(String SpecID)
		{
			String values="~#~ ";
			int index=1;
			waitForVisibility(ReferenceColumnNames.get(0));
			ListIterator<WebElement> specidList = ReferencesID.listIterator();
			while(specidList.hasNext())
			{
				if(specidList.next().getText().contains(SpecID))
					break;
				index++;
			}
			Iterator<WebElement> details=driver.findElements(By.xpath("//tbody[@class='ui-table-tbody']//tr["+index+"]/td")).iterator();
			
			while(details.hasNext())
			{
				values+=details.next().getText()+"~#";
			}
			values+="~";
			values=values.replace("~#~","");
			return values;
		}
		
		public boolean SelectReference(String SpecID)
		{
			ListIterator<WebElement> specidList = ReferencesID.listIterator();
			while(specidList.hasNext())
			{
				WebElement element=specidList.next();
				if(element.getText().contains(SpecID))
				{	element.click();
					break;
				}
			}
			return OkBtn.isEnabled();
		}
		
		public boolean AddReference(String SpecID)
		{
			OkBtn.click();
			WaitforElementInVisible(Headertitle);
			return !OkBtn.isDisplayed();
		}
		public boolean CancelAddingReference(String SpecID)
		{
			CancelBtn.click();
			WaitforElementInVisible(CancelBtn);
			return !CancelBtn.isDisplayed();
		}
		
		
}
