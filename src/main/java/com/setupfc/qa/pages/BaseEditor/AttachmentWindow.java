/**
 * 
 */
package com.setupfc.qa.pages.BaseEditor;

import java.util.Iterator;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.setupfc.qa.util.TestUtil;

/**
 * @author smallika
 *
 */
public class AttachmentWindow extends TestUtil {
	//Initializing the page object 
	public AttachmentWindow()
	{
		PageFactory.initElements(driver, this);
	}
	
//OR:
	//File upload details
		
		
		@FindBy(className="ui-dialog-title")
		private WebElement HeaderTitle;
		
		@FindBy(xpath="//span[contains(@class,'ui-fileupload-choose ui-button')]")
		private WebElement ChooseBtn;
		
		@FindBy(xpath="//span[@class='ui-button-text ui-clickable' and text()='Upload']")
		private WebElement UploadBtn;
		
		@FindBy(xpath="//span[@class='ui-button-text ui-clickable' and text()='Cancel']")
		private WebElement CancelBtn;
		
		
		@FindBy(xpath="//div[@class='ui-fileupload-row ng-star-inserted']/div[2]")
		private List<WebElement> AddedFiles;
		
		@FindBy(xpath="//*[@class='pi pi-times']")
		private WebElement CloseIcon;
		
		
		
//Actions:
		public String GetHeaderTitle()
		{
			return HeaderTitle.getText();
		}
		
		public boolean ChooseFile(String FileName) {
			if (!FileName.contains(":")) {
				FileName = FileName.replace("./", "").replace("/", "\\");
				FileName = System.getProperty("user.dir") + "\\" + FileName;
			}
			ChooseBtn.findElement(By.xpath("//input[@type='file']")).sendKeys(FileName);
			return true;
		}
		public String GetAddedFileNames()
		{
			String values="~#~ ";
			Iterator<WebElement> Elementlist = AddedFiles.iterator();
			while(Elementlist.hasNext())
			{
				values+=Elementlist.next().getText()+"~#";
			}
			values+="~";
			values=values.replace("~#~","");
			return values;
		}
		public WebbaseMainPage Uploadfile()
		{
			UploadBtn.click();
			WaitforElementInVisible(HeaderTitle);
			return new WebbaseMainPage();
		}
		
		public WebbaseMainPage CloseWindow()
		{
			CloseIcon.click();
			WaitforElementInVisible(HeaderTitle);
			return new WebbaseMainPage();
		}
		
		
}
