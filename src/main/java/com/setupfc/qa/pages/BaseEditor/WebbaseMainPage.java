/**
 * 
 */
package com.setupfc.qa.pages.BaseEditor;

import java.util.List;
import java.util.ListIterator;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.setupfc.qa.util.TestUtil;

/**
 * @author smallika
 *
 */
public class WebbaseMainPage extends TestUtil{
	//Initializing the page object 
			public WebbaseMainPage()
			{
				PageFactory.initElements(driver, this);
			}
			
	//OR:
			
			@FindBy(className="navbar-title")
			private WebElement EditorTitle;
			
			@FindBy(xpath="//a[@id='account-menu']//span/span")
			private WebElement LoggedUser;
			
			@FindBy(id="menu-spec")
			private WebElement SpecificationDropDown;
			
			@FindBy(id="menu-add")
			private WebElement AddDropDown;
			
			//Specification Metainformation>>>>>>>>>>>>>>>>>>>>>>>>>>>>
			
			@FindBy(xpath="//table[@id='MetaInformation']//tbody//tr//th[1]")
			private WebElement MetaInfo_SpecID;

			@FindBy(xpath="//table[@id='MetaInformation']//tbody//tr//th[@colspan='2']")
			private WebElement MetaInfo_Status;

			@FindBy(xpath="//table[@id='MetaInformation']//tbody//tr//td[@colspan='3']")
			private WebElement MetaInfo_Name;

			@FindBy(xpath="//span[contains(text(),'Author:')]/following-sibling::span")
			private WebElement MetaInfo_Author;

			@FindBy(xpath="//span[contains(text(),'Department:')]/following-sibling::span")
			private WebElement MetaInfo_Department;

			@FindBy(xpath="//span[contains(text(),'Owner:')]/following-sibling::span")
			private WebElement MetaInfo_Owner;

			@FindBy(xpath="//span[contains(text(),'WorkflowConfiguration:')]/following-sibling::span")
			private WebElement MetaInfo_Workflow;

			@FindBy(xpath="//span[contains(text(),'SubClass:')]/following-sibling::span")
			private WebElement MetaInfo_Subclass;

			@FindBy(xpath="//span[contains(text(),'GroupAccessLevel:')]/following-sibling::span")
			private WebElement MetaInfo_GroupAccessLevel;

			@FindBy(xpath="//span[contains(text(),'Template:')]/following-sibling::span")
			private WebElement MetaInfo_Template;
			
			@FindBy(xpath="//span[contains(text(),'UseCase:')]/following-sibling::span")
			private WebElement MetaInfo_UseCase;

			//Change Main view details>>>>>>>>>>>>>>>>>>>>>>>>>>>>
			
			@FindBy(id="field_pcrb")
			private WebElement PCRB;
			
			@FindBy(id="field_changeDescription")
			private WebElement ChangeDescription;
			
			@FindBy(id="field_changeReason")
			private WebElement ChangeReason;
			
			@FindBy(xpath="//*[@id='edit-purpose']//editor/div")
			private WebElement Purpose;
			
			@FindBy(xpath="//*[@id='edit-scope']//editor/div")
			private WebElement Scope;
			
			@FindBy(xpath="//gf-text-editor[@id='edit-definitions']//editor/div")
			private WebElement Definitions;
		
			//Mandate fields Error message>>>>>>>>>>>>>>>>>>>>>>>>>>>>
			

			@FindBy(xpath="//*[@id='field_pcrb']/following-sibling::div//small")
			private WebElement ErrorMsg_PCRB;
			
			@FindBy(xpath="//*[@id='field_changeDescription']/following-sibling::div//small")
			private WebElement ErrorMsg_ChangeDescription;
			
			@FindBy(xpath="//*[@id='field_changeReason']/following-sibling::div//small")
			private WebElement ErrorMsg_ChangeReason;
			
			@FindBy(xpath="//*[@id='edit-purpose']//div//small")
			private WebElement ErrorMsg_Purpose;
			
			@FindBy(xpath="//*[@id='edit-scope']//div//small")
			private WebElement ErrorMsg_Scope;
			
			//Specifications and Add drop down list details>>>>>>>>>>>>>>>>>>>>>>>>>>>>
			@FindBy(xpath="//a[contains(@id,'menu-spec-')]/span[2]")
			private List<WebElement> SpecificationOptions;
			
			@FindBy(xpath="//a[contains(@id,'menu-add-')]/span[2]")
			private List<WebElement> AddOptions;
			
			
			
		  //Attachments section:>>>>>>>>>>>>>>>>>>>>>>>>>>>> 
			
			@FindBy(xpath="//div[@id='attachments-container']//div[@class='alert alert-warning']/span")
			private WebElement Attachment_WarningMsg;
			
			@FindBy(xpath="//div[@id='attachments-container']//fa-icon/parent::div")
			private List<WebElement> AttachmentNameList;
			
			@FindBy(xpath="//div[@id='attachments-container']//a[@rel='noopener']")
			private List<WebElement> AttachmentViewBtn;
			
			@FindBy(xpath="//div[@id='attachments-container']//div//button[@class='btn btn-danger btn-sm ng-star-inserted']")
			private List<WebElement> AttachmentDeleteBtn;
			
		//Reference Section:>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
			@FindBy(xpath="//div[@id='spec-references-container']//div[@class='alert alert-warning']/span")
			private WebElement Reference_WarningMsg;
			
			@FindBy(xpath="//div[@id='spec-references-container']//fa-icon/parent::div")
			private List<WebElement>  ReferenceNameList;
			
			@FindBy(xpath="//div[@id='spec-references-container']//a[@rel='noopener']")
			private List<WebElement>  ReferenceViewBtn;
			
			@FindBy(xpath="//div[@id='spec-references-container']//div//button[@class='btn btn-danger btn-sm ng-star-inserted']")
			private List<WebElement>  ReferenceDeleteBtn;
		
			
		//PLMReference Section:>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
			@FindBy(xpath="//div[@id='plm-references-container']//div[@class='alert alert-warning']/span")
			private WebElement PLMReference_WarningMsg;
			
			@FindBy(xpath="//div[@id='plm-references-container']//fa-icon/parent::div")
			private List<WebElement>  PLMReferenceNameList;
			
			@FindBy(xpath="//div[@id='plm-references-container']//a[@rel='noopener']")
			private List<WebElement>  PLMReferenceViewBtn;
			
			@FindBy(xpath="//div[@id='plm-references-container']//div//button[@class='btn btn-danger btn-sm ng-star-inserted']")
			private List<WebElement>  PLMReferenceDeleteBtn;
		
			
			
			//Common Delete window details>>>>>>>>>>>>>>
			@FindBy(className ="ui-dialog-title")
			private WebElement DeleteWarningHdr;
			
			@FindBy(xpath ="//div[@class='ui-dialog-content ui-widget-content']//p")
			private WebElement DeleteContentMsg;
			
			@FindBy(id ="gf-confirm-delete-attachment")
			private WebElement DeleteConfirmBtn;
			
			@FindBy(xpath ="//button[@class='btn btn-secondary']")
			private WebElement CancelConfirm;
			
			
	//Actions:
			//Text fields verifications
			public String GetEditorTitle()
			{
				return EditorTitle.getText();
			}
			
			public String GetLoggedUserID()
			{
				return LoggedUser.getText();
			}
			
			public String VerifyMetaInformation()
			{
				String value="";
				value+=MetaInfo_SpecID.getText().split(" - ")[0].trim();
				value+=","+MetaInfo_Status.getText().trim();
				value+=","+MetaInfo_Name.getText().trim();
				value+=","+MetaInfo_Author.getText().trim();
				value+=","+MetaInfo_Department.getText().trim();
				value+=","+MetaInfo_Workflow.getText().trim();
				value+=","+MetaInfo_Subclass.getText().trim();
				value+=","+MetaInfo_Template.getText().trim();
				value+=","+MetaInfo_UseCase.getText().trim();
				return value;
			}
			
			public String GetMetadata_SpecIDwithVersion()
			{
				
				return MetaInfo_SpecID.getText();
				
			}
			public String GetMetadata_Status()
			{
				
				return MetaInfo_Status.getText();
				
			}
			public String GetMetadata_Name()
			{
				
				return MetaInfo_Name.getText();
				
			}
			public String GetMetadata_Author()
			{
				
				return MetaInfo_Author.getText();
				
			}
			public String GetMetadata_Owner()
			{
				
				return MetaInfo_Owner.getText();
				
			}
			public String GetMetadata_Depart()
			{
				
				return MetaInfo_Department.getText();
				
			}
			public String GetMetadata_Subclass()
			{
				
				return MetaInfo_Subclass.getText();
				
			}
			public String GetMetadata_Template()
			{
				
				return MetaInfo_Template.getText();
				
			}
			public String GetMetadata_UseCase()
			{
				
				return MetaInfo_UseCase.getText();
				
			}
			public String GetMetadata_Workflow()
			{
				
				return MetaInfo_Workflow.getText();
				
			}
			
			
			public boolean AddPCRB(String value)
			{
				PCRB.click();
				ClearByAction();
				if(!value.isEmpty())
				PCRB.sendKeys(value);
				return true;
			}
			public String Verify_PCRBErrorMsg()
			{
				return ErrorMsg_PCRB.getText();
			}
			public boolean AddChangeReason(String value)
			{
				ChangeReason.click();
				ClearByAction();
				if(!value.isEmpty())
				ChangeReason.sendKeys(value);
				return true;
			}
			public String Verify_ChangeReasonErrorMsg()
			{
				return ErrorMsg_ChangeReason.getText();
			}
			
			
			public boolean AddChangeDescription(String value)
			{
				ChangeDescription.click();
				ClearByAction();
				if(!value.isEmpty())
				ChangeDescription.sendKeys(value);
				return true;
			}
			
			public String Verify_ChangeDescErrorMsg()
			{
				return ErrorMsg_ChangeDescription.getText();
			}
			
			public boolean AddPurpose(String value)
			{
				ScrollToViewElement(Purpose);
				Purpose.click();
				Purpose.clear();
				if(!value.isEmpty())
				Purpose.sendKeys(value);
				return true;
			}
			public String Verify_PurposeErrorMsg()
			{
				return ErrorMsg_Purpose.getText();
			}
			
			public boolean Verify_PurposeNoErrorMsg()
			{
				WaitforElementInVisible(ErrorMsg_Purpose);
				return true;
			}
			
			public boolean AddScope(String value)
			{
				ScrollToViewElement(Scope);
				Scope.click();
				Scope.clear();
				if(!value.isEmpty())
				Scope.sendKeys(value);
				return true;
			}
			public String Verify_ScopeErrorMsg()
			{
				return ErrorMsg_Scope.getText();
			}
			public boolean Verify_ScopeNoErrorMsg()
			{
				WaitforElementInVisible(ErrorMsg_Scope);
				return true;
			}
			public boolean AddDefinitions(String value)
			{
				Definitions.click();
				Definitions.clear();
				if(!value.isEmpty())
				Definitions.sendKeys(value);
				return true;
			}
			
			//Specification drop down verification
			public boolean SelectSpecificationOption(String Option)
			{
				SpecificationDropDown.click();
				ListIterator<WebElement> Listiterator = SpecificationOptions.listIterator();
				while(Listiterator.hasNext())
				{
					WebElement element=Listiterator.next();
					if(element.getText().toUpperCase().contains(Option.toUpperCase()))
					{
						element.click();
						return true;
					}
				}
				return false;
			}
			public boolean ValidateSpecification()
			{
				return SelectSpecificationOption("Validate");
			}
			
			public boolean SaveToSetupFC()
			{
				return SelectSpecificationOption("Save to setup.FC");
			}
			
			public boolean ReloadFromSetupFC()
			{
				return SelectSpecificationOption("Reload from setup.FC");
			}
			public SubmitPage Submit()
			{
				 SelectSpecificationOption("Submit");
				 return new SubmitPage();
			}
			public PreviewPage Preview()
			{
				 SelectSpecificationOption("Preview");
				 return new PreviewPage();
			}
			public CompareViewPage CompareView()
			{
				 SelectSpecificationOption("Compare");
				 return new CompareViewPage();
			}
			
			
			//Add drop down validation
			public boolean SelectAddOption(String Option)
			{
				AddDropDown.click();
				ListIterator<WebElement> Listiterator = AddOptions.listIterator();
				while(Listiterator.hasNext())
				{
					WebElement element=Listiterator.next();
					if(element.getText().toUpperCase().contains(Option.toUpperCase()))
					{
						element.click();
						return true;
					}
				}
				return false;
			}
			public AttachmentWindow NavigateToAttachament()
			{
				 SelectAddOption("Attachment");
				 return new AttachmentWindow();
			}
			public ReferenceWindow NavigateToReference()
			{
				 SelectAddOption("Reference");
				 System.out.println("test");
				 return new ReferenceWindow();
			}

			
			
			public KeyWordWindow NavigateToKeyWord()
			{
				 SelectAddOption("Keyword");
				 return new KeyWordWindow();
			}
			
			//Attachments Section
			public String ViewAttachment(String AttachmentName)
			{
				String value="";
				ParentWindow=GetParentWindow();
				int temp=0;
				boolean elementFindFlg=false;
				ListIterator<WebElement> elementList = AttachmentNameList.listIterator();
				while(elementList.hasNext())
				{
					WebElement element=elementList.next();
					if(element.getText().contains(AttachmentName))
					{
						AttachmentViewBtn.get(temp).click();
						elementFindFlg=true;
					}
					temp++;
				}
				if(!elementFindFlg)
					return null;
				SwitchtoNewWindow();
				value=driver.getTitle();
				driver.close();
				SwitchtoParentWindow();
				return value;
			}
			
			public boolean AttachmentDelete(String AttachmentName)
			{
				int temp=0;
				ListIterator<WebElement> elementList = AttachmentNameList.listIterator();
				while(elementList.hasNext())
				{
					WebElement element=elementList.next();
					if(element.getText().contains(AttachmentName))
					{
						AttachmentDeleteBtn.get(temp).click();
					}
					temp++;
				}
				return DeleteWarningHdr.isDisplayed();
			}
			
			public boolean Delete_Confirm()
			{
				DeleteConfirmBtn.click();
				WaitforElementInVisible(DeleteWarningHdr);
				return true;
			}
			public boolean Delete_Cancel()
			{
				CancelConfirm.click();
				WaitforElementInVisible(DeleteWarningHdr);
				return true;
			}

			//Reference Section
			public String ViewReference(String ReferenceName)
			{
				String value="";
				ParentWindow=GetParentWindow();
				int temp=0;
				boolean elementFindFlg=false;
				ListIterator<WebElement> elementList = ReferenceNameList.listIterator();
				while(elementList.hasNext())
				{
					WebElement element=elementList.next();
					if(element.getText().contains(ReferenceName))
					{
						ReferenceViewBtn.get(temp).click();
						elementFindFlg=true;
					}
					temp++;
				}
				if(!elementFindFlg)
					return null;
				SwitchtoNewWindow();
				value=driver.getTitle();
				driver.close();
				SwitchtoParentWindow();
				return value;
			}
			
			public boolean ReferenceDelete(String ReferenceName)
			{
				int temp=0;
				ListIterator<WebElement> elementList = ReferenceNameList.listIterator();
				while(elementList.hasNext())
				{
					WebElement element=elementList.next();
					if(element.getText().contains(ReferenceName))
					{
						ReferenceDeleteBtn.get(temp).click();
					}
					temp++;
				}
				return DeleteWarningHdr.isDisplayed();
			}
			
			//PLMReference Section
			public String ViewPLMReference(String PLMReferenceName)
			{
				String value="";
				ParentWindow=GetParentWindow();
				int temp=0;
				boolean elementFindFlg=false;
				ListIterator<WebElement> elementList = PLMReferenceNameList.listIterator();
				while(elementList.hasNext())
				{
					WebElement element=elementList.next();
					if(element.getText().contains(PLMReferenceName))
					{
						PLMReferenceViewBtn.get(temp).click();
						elementFindFlg=true;
					}
					temp++;
				}
				if(!elementFindFlg)
					return null;
				SwitchtoNewWindow();
				value=driver.getTitle();
				driver.close();
				SwitchtoParentWindow();
				return value;
			}
			
			public boolean PLMReferenceDelete(String PLMReferenceName)
			{
				int temp=0;
				ListIterator<WebElement> elementList = PLMReferenceNameList.listIterator();
				while(elementList.hasNext())
				{
					WebElement element=elementList.next();
					if(element.getText().contains(PLMReferenceName))
					{
						PLMReferenceDeleteBtn.get(temp).click();
					}
					temp++;
				}
				return DeleteWarningHdr.isDisplayed();
			}
			


}
