package com.setupfc.qa.pages.SetupFC;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.pagefactory.AjaxElementLocatorFactory;

import com.setupfc.qa.util.TestUtil;

/**
 * @author smallika
 *
 * @ModifiedBy rhalli
 */
public class NewConfigPage extends TestUtil {

	// Initializing the page object
	public NewConfigPage() {
		
		PageFactory.initElements(driver, this);
		PageFactory.initElements(new AjaxElementLocatorFactory(driver, TestUtil.AjaxTimeout), this);
	}


	// ***** OR *****
	@FindBy(id = "newWorkflowsConfigForm")
	private WebElement NewConfigLable;

	@FindBy(xpath = "//input[@name='newWorkflowsConfigForm:j_id332']")
	private WebElement Title;

	@FindBy(xpath = "//input[@id='newWorkflowsConfigForm:classCombocomboboxField']")
	private WebElement Class;

	@FindBy(xpath = "//input[@id='newWorkflowsConfigForm:subclassCombocomboboxField']")
	private WebElement SubClass;

	@FindBy(xpath = "//input[@id='newWorkflowsConfigForm:j_id341comboboxField']")
	private WebElement Department;

	@FindBy(id = "newWorkflowsConfigForm:approvalTable:0:j_id380")
	private WebElement Normal_ApprovalTierBtn;

	@FindBy(id = "newWorkflowsConfigForm:approvalTable:0:j_id387:0:j_id390comboboxField")
	private WebElement Normal_GroupConfig;

	@FindBy(id = "newWorkflowsConfigForm:j_id463")
	private WebElement SaveBtn;

	@FindBy(id = "newWorkflowsConfigForm:j_id464")
	private WebElement ClearBtn;

	@FindBy(id = "newWorkflowsConfigForm:j_id465")
	private WebElement CancelBtn;

	@FindBy(xpath = "//div[@id='statusContainer']//td[text()='Ready']")
	private WebElement LoadingProgress;

	@FindBy(id = "workflowConfigs:workflowConfigId")
	private WebElement NewConfigID;

	@FindBy(id = "workflowConfigs:workflowConfigTitle")
	private WebElement NewConfigName;

	@FindBy(xpath = "//div[@id='workflowConfigs:userActionMessagePanel']")
	private WebElement SuccessMsg;


	// ***** Actions *****

	public boolean IsLoadingCompleted() {

		waitForVisibility(LoadingProgress);
		return LoadingProgress.isDisplayed();
	}
	
	public boolean AddTitle(String name) {
		
		Title.sendKeys(name);
		return true;
	}

	public boolean SelectClass(String ClassName) {
		
		Class.sendKeys(ClassName + Keys.ENTER);
		return IsLoadingCompleted();
	}

	public boolean SelectSubClass(String SubClassName) {
		
		SubClass.sendKeys(SubClassName + Keys.ENTER);
		return IsLoadingCompleted();
	}

	public boolean SelectDepartment(String DepartmentName) {
		
		Department.sendKeys(DepartmentName + Keys.ENTER);
		return IsLoadingCompleted();
	}

	public boolean AddNormalApprovalTier() {
		
		Normal_ApprovalTierBtn.click();
		return IsLoadingCompleted();
	}

	public boolean AddNormalGroupConfig(String GroupName) {
		
		Normal_GroupConfig.sendKeys(GroupName + Keys.ENTER);
		return IsLoadingCompleted();
	}

	
	public boolean SaveConfig() {
		
		ScrollToBottom();
		WaitforElementVisibleAndClickable(ClearBtn);
		SaveBtn.click();
		return IsLoadingCompleted();
	}

	public String GetNewConfigID() {
		
		return NewConfigID.getText();
	}

	public String GetNewConfigName() {
		
		return NewConfigName.getText();
	}

	public boolean VerifySuccessMessage() {
		
		return SuccessMsg.isDisplayed();
	}

	public boolean ClearNewConfig() {
		
		ScrollToBottom();
		WaitforElementVisibleAndClickable(ClearBtn);
		ClearBtn.click();
		return IsLoadingCompleted();
	}

	public boolean CancelNewConfig() {
		
		ScrollToBottom();
		WaitforElementVisibleAndClickable(ClearBtn);
		CancelBtn.click();
		return IsLoadingCompleted();
	}

}
