/**
 * 
 */
package com.setupfc.qa.pages.SetupFC;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.setupfc.qa.util.TestUtil;

/**
 * @author smallika
 *
 */
public class SearchSpecPage extends TestUtil {
	
	// Initializing the page object
	public SearchSpecPage() {
		PageFactory.initElements(driver, this);
	}
	
	// ***** Page Factory OR ****
	@FindBy(xpath = "//div[@id='statusContainer']//td[text()='Ready']")
	private WebElement LoadingProgress;
	
	@FindBy(id = "specContent:searchIdInput")
	private WebElement SearchIdInput;
	
	@FindBy(id = "specContent:searchTitleInput")
	private WebElement SearchTitleInput;
	
	@FindBy(id = "specContent:searchButton")
	private WebElement SearchButton;
	
	@FindBy(id = "specContent:clearButton")
	private WebElement ClearButton;
	
	@FindBy(xpath = "//table[@id='specContent:specTable']/tbody/tr/td[4]/label")
	private WebElement SearchResultSpecID;
	
	@FindBy(id = "specContent:editSpec")
	private WebElement LockforEditBtn;
	
	@FindBy(xpath = "//label[contains(@for,'editNewSpecPanelForm:createdSpecWorkflowTypeItems')]")
	private List<WebElement> ListofEditTypes;
	
	@FindBy(id = "editSpecPanelForm:OkButton")
	private WebElement SpecEditConfirmBtn;
	
	// ***** Actions *****
	public boolean IsLoadingCompleted() {

		waitForVisibility(LoadingProgress);
		return LoadingProgress.isDisplayed();
	}

	public boolean searchSpecByID(String SpecID) {
		
		IsLoadingCompleted();
		WaitforElementVisibleAndClickable(SearchIdInput);
		SearchIdInput.click();
		SearchIdInput.sendKeys(SpecID);
		SearchButton.click();
		waitForVisibility(SearchResultSpecID);
		SearchResultSpecID.click();
		IsLoadingCompleted();
		
		String SearchedSpecID = SearchResultSpecID.getText();
		if(SearchedSpecID.equalsIgnoreCase(SpecID)) {
			return true;
		} else {
			return false;
		}
		
	}
	
	public boolean LockForEdit(String Editype) {

		ScrollToViewElement(LockforEditBtn);
		LockforEditBtn.click();
		for (WebElement element : ListofEditTypes)
			if (element.getText().equalsIgnoreCase(Editype)) {
				element.click();
				break;
			}
		SpecEditConfirmBtn.click();
		IsLoadingCompleted();
		return true;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
