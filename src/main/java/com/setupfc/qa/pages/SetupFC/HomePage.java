/**
 * 
 */
package com.setupfc.qa.pages.SetupFC;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.setupfc.qa.util.TestUtil;

/**
 * @author smallika
 *
 * @ModifiedBy rhalli
 * 
 */
public class HomePage extends TestUtil {
	
	// Initializing the page object
	public HomePage() {
		
		PageFactory.initElements(driver, this);
	}


	// ***** Page factory OR *****
	
	@FindBy(xpath = "//li[@class='infomsg']//span[text()='Login failed']")
	private WebElement LoginFailMessage;
	
	@FindBy(xpath = "//span[contains(text(),'Welcome to setup.FC')]")
	private WebElement WelcomeMsg;

	@FindBy(xpath = "//div[@id='menu']//tr//*[contains(text(),'Home')]")
	private WebElement HomeTab;

	@FindBy(xpath = "//div[@id='menu']//tr//*[contains(text(),'Tasks')]")
	private WebElement TasksTab;

	@FindBy(xpath = "//div[@id='menu']//tr//*[contains(text(),'Specifications')]")
	private WebElement SpecificationsTab;

	@FindBy(xpath = "//div[@id='menu']//tr//*[contains(text(),'Reports')]")
	private WebElement ReportsTab;

	@FindBy(xpath = "//div[@id='menu']//tr//*[contains(text(),'Admin')]")
	private WebElement AdminTab;

	@FindBy(id = "j_id22:menuLogoutId")
	private WebElement LogoutBtn;

	@FindBy(xpath = "//span[@id='j_id22:menuWelcomeId']")
	private WebElement UserID;
	
	@FindBy(xpath = "//div[@id='statusContainer']//td[text()='Ready']")
	private WebElement LoadingProgress;

	
	// *****  Actions  *****

	public boolean IsLoadingCompleted() {

		waitForVisibility(LoadingProgress);
		return LoadingProgress.isDisplayed();
	}
	
	public boolean VerifyLoginFailMsg() {
		
		return LoginFailMessage.isDisplayed();
	}
	
	public boolean VerifyWelcomeMsg() {
		
		return WelcomeMsg.isDisplayed();
	}

	public String GetLoggedUserID() {
		
		return UserID.getText();
	}

	public TaskPage NavigateToTaskPage() {
		
		TasksTab.click();
		IsLoadingCompleted();
		return new TaskPage();
	}

	public SpecificationPage NavigateToSpecificationPage() {
		
		SpecificationsTab.click();
		IsLoadingCompleted();
		return new SpecificationPage();
	}

	public ReportPage NavigateToReportPage() {
		
		ReportsTab.click();
		IsLoadingCompleted();
		return new ReportPage();
	}

	public AdminPage NavigateToAdminPage() {
		
		AdminTab.click();
		IsLoadingCompleted();
		return new AdminPage();
	}

	public LoginPage Logout() {
		
		LogoutBtn.click();
		IsLoadingCompleted();
		return new LoginPage();
	}

}
