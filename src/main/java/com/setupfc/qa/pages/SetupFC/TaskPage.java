/**
 * 
 */
package com.setupfc.qa.pages.SetupFC;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.setupfc.qa.EditorCommon.EditorMainPage;
import com.setupfc.qa.util.TestUtil;

/**
 * @author smallika
 * 
 * @ModifiedBy rhalli
 *
 */
public class TaskPage extends TestUtil {

	// Initializing the page object
	public TaskPage() {
		
		PageFactory.initElements(driver, this);
	}


	//	***** Page Factory OR *****	

	@FindBy(xpath = "//div[@id='statusContainer']//td[text()='Ready']")
	private WebElement LoadingProgress;

	// Task filters
	@FindBy(id = "taskTable:specTitle_filter")
	private WebElement SpecTitle_filterTxtFld;

	@FindBy(id = "taskTable:specId_filter")
	private WebElement SpecID_filterTxtFld;

	@FindBy(id = "taskTable:0:j_id215")
	private WebElement GridTaskTypeValue;

	@FindBy(id = "taskTable:0:j_id240")
	private WebElement GridTaskWKValue;

	@FindBy(id = "taskTable:0:j_id243")
	private WebElement GridTaskAuthorValue;

	@FindBy(id = "taskTable:0:j_id273")
	private WebElement FirstRowTaskSpecID;

	@FindBy(id = "taskTable:0:j_id277")
	private WebElement GridTaskSpecTitleValue;

	@FindBy(id = "taskTable:0:j_id281")
	private WebElement GridTaskSpecVersionValue;

	// Refresh button
	@FindBy(name = "taskTable:j_id293")
	private WebElement RefreshBtn;

	// Task Actions: Content Edit
	@FindBy(id = "taskTitle")
	private WebElement TaskTitle;

	private WebElement SelectAction(String ActionType) {
		return driver.findElement(By.xpath("//label[text()='" + ActionType +"']/preceding-sibling::input"));
	}

	@FindBy(xpath = "//table[@id='taskActions']/following-sibling::div//span[text()='Confirm']/parent::a")
	private WebElement ConfirmAction;

	@FindBy(xpath = "//*[contains(@id,'submit')]")
	private WebElement TaskOtherConfirmBtn;

	@FindBy(className = "fakeButton")
	private WebElement ApprovalConfirmBtn;

	@FindBy(xpath = "//textarea[contains(@id,'changeDescription')]")
	private WebElement ChangeDesc_TxtFld;

	@FindBy(xpath = "//textarea[contains(@id,'changeReason')]")
	private WebElement ChangeReason_TxtFld;

	// Property change task details
	@FindBy(xpath = "//div[@id='actionDetail']//td/input[@type='text']")
	private WebElement PC_SpecTitle_TxtFld;

	@FindBy(xpath = "//input[contains(@id,'subclassCombocomboboxField')]")
	private WebElement PC_SubClass_TxtFld;

	@FindBy(xpath = "//input[contains(@id,'departmentCombocomboboxField')]")
	private WebElement PC_Department_TxtFld;

	@FindBy(xpath = "//input[contains(@id,'workflowConfigCombocomboboxField')]")
	private WebElement PC_WorkFlow_TxtFld;

	@FindBy(xpath = "//input[contains(@id,'userNameInputField')]")
	private WebElement PC_SpecOwner_TxtFld;

	// Approval Task details

	@FindBy(id = "taskActions:submitComment")
	private WebElement Approval_CommentTxtFld;

	@FindBy(id = "taskActions:approveTask")
	private WebElement Approval_ApproveBtn;

	@FindBy(id = "taskActions:rejectTask")
	private WebElement Approval_RejectBtn;

	@FindBy(id = "_approveTaskForm:OkButton")
	private WebElement Approve_ConfirmBtn;
	
	@FindBy(xpath = "//label[text()='Comment']/../../following-sibling::td/textarea")
	private WebElement ActionsCommentTextbox;
	
	// Logout button	
	@FindBy(id = "j_id22:menuLogoutId")
	private WebElement LogoutBtn;
	
	
	// ***** Actions *****
	
	public boolean IsLoadingCompleted() {
		
		waitForVisibility(LoadingProgress);
		return LoadingProgress.isDisplayed();
	}
	
	public boolean refreshTaskList() {
		
		IsLoadingCompleted();
		RefreshBtn.click();
		IsLoadingCompleted();
		return true;
	}

	public boolean SearchBySpecTitle(String Title) {
		
		SpecTitle_filterTxtFld.sendKeys(Title);
		RefreshBtn.click();
		return IsLoadingCompleted();
	}

	public boolean SearchBySpecID(String ID) {
		
		SpecID_filterTxtFld.click();
		ClearByAction();
		SpecID_filterTxtFld.sendKeys(ID);
		RefreshBtn.click();
		return IsLoadingCompleted();
	}

	public String GetSpecDetails() {
		
		String value = "";
		value += GridTaskTypeValue.getText();
		value += "," + GridTaskWKValue.getText();
		value += "," + GridTaskAuthorValue.getText();
		value += "," + FirstRowTaskSpecID.getText();
		value += "," + GridTaskSpecTitleValue.getText();
		value += "," + GridTaskSpecVersionValue.getText();
		return value;
	}

	public boolean SelectTask(String requiredSpecID) {
	
		refreshTaskList();
		String actualFirstRowTaskID = FirstRowTaskSpecID.getText();
		
		if(actualFirstRowTaskID.equalsIgnoreCase(requiredSpecID)) {
			FirstRowTaskSpecID.click();
			IsLoadingCompleted();
			ScrollToBottom();
			return TaskTitle.isDisplayed();
		} else {
			return false;
		}
		
	}
	
	public boolean verifyTaskTitleInTaskDetails(String expectedTaskTitle) {
		
		IsLoadingCompleted();
		ScrollToBottom();
		
		String actualTaskTitle = TaskTitle.getText();
		if(actualTaskTitle.equalsIgnoreCase(expectedTaskTitle)) {
			return true;
		} else {
			return false;
		}
	}

	public EditorMainPage confirmEditSpecificationAction() {
		
		WaitforElementVisibleAndClickable(SelectAction("Edit Specification"));
		SelectAction("Edit Specification").click();
		ConfirmAction.click();
		SwitchtoNewWindow();
		waitForPageLoad();
		WaitInSeconds("3");
		System.out.println(driver.getTitle());
		return new EditorMainPage();
	}
	
	public TaskActionsPage selectTakeAction() {
		
		WaitforElementVisibleAndClickable(SelectAction("Take Action"));
		SelectAction("Take Action").click();
		ConfirmAction.click();
		return new TaskActionsPage();
		
	}
	
	public boolean updateSpecificationMetadata(String UpdatedTitle, String ChangeReason, String ChangeDescription) {
		
		WaitforElementVisibleAndClickable(SelectAction("Update Specification"));
		SelectAction("Update Specification").click();
		WaitforElementVisibleAndClickable(PC_SpecTitle_TxtFld);
		PC_SpecTitle_TxtFld.sendKeys(UpdatedTitle);
		ChangeReason_TxtFld.sendKeys(ChangeReason);
		ChangeDesc_TxtFld.sendKeys(ChangeDescription);
		WaitforElementVisibleAndClickable(TaskOtherConfirmBtn);
		TaskOtherConfirmBtn.click();
		MaxWait();
		MediumWait();
		IsLoadingCompleted();
		
		return true;
		
	}
	
	public boolean confirmTerminationSpecification(String ChangeReason, String ChangeDescription) {
		
		WaitforElementVisibleAndClickable(SelectAction("Terminate Specification"));
		SelectAction("Terminate Specification").click();
		WaitforElementVisibleAndClickable(ChangeReason_TxtFld);
		ChangeReason_TxtFld.sendKeys(ChangeReason);
		ChangeDesc_TxtFld.sendKeys(ChangeDescription);
		WaitforElementVisibleAndClickable(TaskOtherConfirmBtn);
		TaskOtherConfirmBtn.click();
		MaxWait();
		MediumWait();
		IsLoadingCompleted();
		
		return true;
	}
	
	public boolean confirmRetrySpecification(String Comment) {
		
		WaitforElementVisibleAndClickable(SelectAction("Retry"));
		SelectAction("Retry").click();
		IsLoadingCompleted();
		ActionsCommentTextbox.click();
		ActionsCommentTextbox.sendKeys(Comment);
		WaitforElementVisibleAndClickable(TaskOtherConfirmBtn);
		TaskOtherConfirmBtn.click();
		IsLoadingCompleted();
		MaxWait();
		
		return true;
		
	}
	
	public boolean confirmReEditTaskAction(String Comment) {
		
		WaitforElementVisibleAndClickable(SelectAction("Re-edit"));
		SelectAction("Re-edit").click();
		IsLoadingCompleted();
		ActionsCommentTextbox.click();
		ActionsCommentTextbox.sendKeys(Comment);
		WaitforElementVisibleAndClickable(TaskOtherConfirmBtn);
		TaskOtherConfirmBtn.click();
		IsLoadingCompleted();
		MediumWait();
		
		return true;		
	}
	
	public boolean confirmDiscardChangesTaskAction(String Comment) {
		
		SwitchtoParentWindow();
		WaitforElementVisibleAndClickable(SelectAction("Discard Changes"));
		SelectAction("Discard Changes").click();
		IsLoadingCompleted();
		ActionsCommentTextbox.click();
		ActionsCommentTextbox.sendKeys(Comment);
		WaitforElementVisibleAndClickable(TaskOtherConfirmBtn);
		TaskOtherConfirmBtn.click();
		IsLoadingCompleted();
		MediumWait();
		
		return true;
	}
	
	public LoginPage Logout() {

		WaitforElementVisibleAndClickable(LogoutBtn);
		LogoutBtn.click();
		IsLoadingCompleted();
		return new LoginPage();
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
