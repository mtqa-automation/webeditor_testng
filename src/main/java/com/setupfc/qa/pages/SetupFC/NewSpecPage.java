/**
 * 
 */
package com.setupfc.qa.pages.SetupFC;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.setupfc.qa.util.TestUtil;

/**
 * @author smallika
 *
 * @ModifiedBy rhalli
 * 
 */
public class NewSpecPage extends TestUtil {

	// Initializing the page object
	public NewSpecPage() {
		
		PageFactory.initElements(driver, this);
	}

	
	// ***** Page Factory OR *****	

	@FindBy(xpath = "//div[@id='statusContainer']//td[text()='Ready']")
	private WebElement LoadingProgress;

	@FindBy(id = "newSpecForm:title")
	private WebElement TitleTxtFld;

	@FindBy(id = "newSpecForm:classCombocomboboxField")
	private WebElement ClassDropdown;

	@FindBy(id = "newSpecForm:subclassCombocomboboxField")
	private WebElement SubClassDropdown;

	@FindBy(id = "newSpecForm:departmentCombocomboboxField")
	private WebElement DepartmentDropdown;

	@FindBy(id = "newSpecForm:metadataSpecOwner:userNameInputField")
	private WebElement OwnerDropdown;

	@FindBy(xpath = "//input[contains(@id,'newSpecForm') and @value='Next >']")
	private WebElement NextBtn;

	@FindBy(xpath = "//input[contains(@id,'newSpecForm') and @value='< Back']")
	private WebElement BackBtn;

	@FindBy(xpath = "//input[contains(@id,'newSpecForm') and @value='Create']")
	private WebElement CreateBtn;

	private List<WebElement> WorkFlowRadioBtn(String WorkFlowID) {
		return driver.findElements(By.xpath("//tbody[@id='newSpecForm:workflowConfigTable:tb']//td[(text()='"
				+ WorkFlowID + "')]/ancestor::tr[2]/td[1]/input"));
	}
	
	@FindBy(xpath = "//table[contains(@class,'dtascroller-table')]//tr/td[7]")
	private WebElement ConfigNextPageButton;

	@FindBy(xpath = "//div[@id='newSpecForm:specificationNewPanel_body']//tr//td[2]")
	private WebElement CreatedSpecID;

	@FindBy(xpath = "//div[@id='newSpecForm:specificationNewPanel_body']//p//label")
	private WebElement SpecCreatedMsg;

	@FindBy(id = "newSpecForm:editNewSpec")
	private WebElement LockforEditBtn;

	@FindBy(id = "editNewSpecPanelHeader")
	private WebElement EditSpecWindowTitle;

	@FindBy(xpath = "//label[contains(@for,'editNewSpecPanelForm:createdSpecWorkflowTypeItems')]")
	private List<WebElement> ListofEditTypes;

	@FindBy(id = "editNewSpecPanelForm:OkButton")
	private WebElement SpecEditConfirmBtn;

	@FindBy(id = "editNewSpecPanelForm:CancelButton")
	private WebElement SpecEditCanceBtn;

	
	// ***** Actions *****
	
	public boolean IsLoadingCompleted() {
		
		waitForVisibility(LoadingProgress);
		return LoadingProgress.isDisplayed();
	}

	public boolean AddGeneralSettings(String Title, String Class, String Subclass, String Department) {
		
		TitleTxtFld.sendKeys(Title);
		ClassDropdown.sendKeys(Class + Keys.ENTER);
		IsLoadingCompleted();
		SubClassDropdown.sendKeys(Subclass + Keys.ENTER);
		IsLoadingCompleted();
		DepartmentDropdown.sendKeys(Department + Keys.ENTER);
		return IsLoadingCompleted();
	}

	public boolean Next() {
		
		ScrollToBottom();
		NextBtn.click();
		return IsLoadingCompleted();
	}

	public boolean SelectWorkFlow(String workflowid) {
		
		while(WorkFlowRadioBtn(workflowid).size() <= 0) {
			
			ScrollToViewElement(ConfigNextPageButton);
			ConfigNextPageButton.click();
			IsLoadingCompleted();		
			break;
		}
		
		for(int i=0; i<=WorkFlowRadioBtn(workflowid).size()-1; i++) {
			
			if(WorkFlowRadioBtn(workflowid).size() > 0) {
				ScrollToViewElement(WorkFlowRadioBtn(workflowid).get(i));
				WorkFlowRadioBtn(workflowid).get(i).click();
				break;
			}
			
		}
		return true;
	}

	public boolean CreateSpec() {
		
		CreateBtn.click();
		return true;
	}

	public String VerifySuccessMsg() {
		
		return SpecCreatedMsg.getText();
	}

	public String GetCreateSpecID() {
		
		return CreatedSpecID.getText();
	}

	public boolean LockForEdit(String Editype) {
		
		LockforEditBtn.click();
		for (WebElement element : ListofEditTypes)
			if (element.getText().equalsIgnoreCase(Editype)) {
				element.click();
				break;
			}
		SpecEditConfirmBtn.click();
		return true;
	}

}
