package com.setupfc.qa.pages.SetupFC;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.setupfc.qa.util.TestUtil;

/**
 * 
 * @author rhalli
 * 
 **/
public class EditConfigPage extends TestUtil {

	// Initializing the page object
	public EditConfigPage() {
		
		PageFactory.initElements(driver, this);
	}
	
	
	// ***** Page Factory OR *****
	
	@FindBy(xpath = "//label[contains(text(),'Title')]/../following-sibling::td/input")
	private WebElement Title;
	
	@FindBy(xpath = "//div[@id='statusContainer']//td[text()='Ready']")
	private WebElement LoadingProgress;
	
	@FindBy(xpath = "//input[@value='Save']")
	private WebElement SaveBtn;
	
	@FindBy(xpath = "//input[@value='Cancel']")
	private WebElement CancelBtn;
	
	@FindBy(xpath = "//div[@id='workflowConfigs:userActionMessagePanel']/span[contains(text(),'updated')]")
	private WebElement UpdateSuccessMsg;
	
	
	
	// ***** Actions *****
	
	public boolean IsLoadingCompleted() {

		waitForVisibility(LoadingProgress);
		return LoadingProgress.isDisplayed();
	}
	
	
	public boolean editTitle(String updatedTitle) {
		
		Title.sendKeys(updatedTitle);
		return true;
	}
	
	public boolean saveUpdatedConfig() {
		
		ScrollToBottom();
		SaveBtn.click();
		return IsLoadingCompleted();
	}
	
	public boolean cancelUpdateConfig() {
		
		ScrollToBottom();
		CancelBtn.click();
		return IsLoadingCompleted();
	}
	
	public boolean verifyUpdatedConfigMessage() {
		
		IsLoadingCompleted();
		return UpdateSuccessMsg.isDisplayed();
	}	
	
}
