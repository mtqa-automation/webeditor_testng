/**
 * 
 */
package com.setupfc.qa.pages.SetupFC;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.setupfc.qa.util.TestUtil;

/**
 * @author smallika
 * 
 * @ModifiedBy rhalli
 *
 */
public class SpecificationPage extends TestUtil {

	//Initializing the page object 
	public SpecificationPage() {
		PageFactory.initElements(driver, this);
	}

	//	***** Page Factory OR *****	
	
	@FindBy(xpath = "//div[@id='statusContainer']//td[text()='Ready']")
	private WebElement LoadingProgress;

	@FindBy(id = "j_id79:j_id86")
	private WebElement NewOption;

	@FindBy(id = "j_id79:j_id88")
	private WebElement SearchOption;

	@FindBy(id = "j_id79:j_id92")
	private WebElement MyOwnOption;

	@FindBy(xpath = "//div[@id='newSpecForm:specificationNewPanel_header']/label")
	private WebElement HeaderLable;

	
	// ***** Actions *****
	
	public boolean IsLoadingCompleted() {

		waitForVisibility(LoadingProgress);
		return LoadingProgress.isDisplayed();
	}
	
	public NewSpecPage NavigateToNewSpecification() {
		
		NewOption.click();
		IsLoadingCompleted();
		return new NewSpecPage();
	}

	public SearchSpecPage NavigateToSearchSpecification() {
		
		SearchOption.click();
		IsLoadingCompleted();
		return new SearchSpecPage();
	}

	public MyOwnSpecPage NavigateToMyOwnSpecification() {
		
		SearchOption.click();
		IsLoadingCompleted();
		return new MyOwnSpecPage();
	}

}
