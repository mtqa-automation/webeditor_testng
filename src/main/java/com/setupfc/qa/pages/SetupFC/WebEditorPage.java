/**
 * 
 */
package com.setupfc.qa.pages.SetupFC;

import org.openqa.selenium.support.PageFactory;

import com.setupfc.qa.EditorCommon.EditorMainPage;
import com.setupfc.qa.pages.BaseEditor.WebbaseMainPage;
import com.setupfc.qa.pages.TableEditor.WebTableMainPage;
import com.setupfc.qa.util.TestUtil;

/**
 * @author smallika
 * 
 * @ModifiedBy rhalli
 *
 */
public class WebEditorPage extends TestUtil {
	
	// Initializing the page object
	public WebEditorPage() {
		
		PageFactory.initElements(driver, this);
	}

	public WebbaseMainPage SwitchToWebBaseEditor() {
		
		MaxWait();
		return new WebbaseMainPage();
	}
	
	public EditorMainPage SwitchToEditor() {
		
		MaxWait();
		return new EditorMainPage();
	}

	public WebTableMainPage SwitchToWebTableEditor() {
		
		MaxWait();
		return new WebTableMainPage();
	}

}
