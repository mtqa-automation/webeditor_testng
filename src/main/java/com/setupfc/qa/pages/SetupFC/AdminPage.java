/**
 * 
 */
package com.setupfc.qa.pages.SetupFC;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.setupfc.qa.base.TestBase;
import com.setupfc.qa.util.TestUtil;

/**
 * @author smallika
 * 
 * @ModifiedBy rhalli
 *
 */
public class AdminPage extends TestUtil {
	
	// Initializing the page object
	public AdminPage() {
		
		PageFactory.initElements(driver, this);
	}

	// ***** Page Factory OR *****
	@FindBy(id = "j_id78:j_id106")
	private WebElement NewConfig;

	@FindBy(id = "j_id78:j_id108")
	private WebElement SearchConfig;

	@FindBy(id = "j_id78:j_id110")
	private WebElement UserTaskMaint;

	@FindBy(id = "j_id78:j_id113")
	private WebElement Maintenance;
	
	@FindBy(xpath = "//div[@id='statusContainer']//td[text()='Ready']")
	private WebElement LoadingProgress;

	
	// ***** Actions *****
	
	public boolean IsLoadingCompleted() {

		waitForVisibility(LoadingProgress);
		return LoadingProgress.isDisplayed();
	}

	public NewConfigPage NavigateToNewConfigPage() {
		
		NewConfig.click();
		IsLoadingCompleted();
		return new NewConfigPage();
	}

	public SearchConfigPage NavigateToSearchConfigPage() {
		
		SearchConfig.click();
		IsLoadingCompleted();
		return new SearchConfigPage();
	}

}
