/**
 * 
 */
package com.setupfc.qa.pages.SetupFC;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.setupfc.qa.util.TestUtil;

/**
 * @author rhalli
 *
 */
public class SearchConfigPage extends TestUtil{

	// Initializing the page object
	public SearchConfigPage() {
		
		PageFactory.initElements(driver, this);
	}
	
	
	// ***** Page Factory OR *****
	
	@FindBy(xpath = "//input[@id='searchIdInput']")
	private WebElement ID;
	
	@FindBy(xpath = "//input[@id='searchTitleInput']")
	private WebElement Title;
	
	@FindBy(xpath = "//input[@id='searchAuthorInput']")
	private WebElement Author;
	
	@FindBy(xpath = "//input[@id='classCombocomboboxField']")
	private WebElement Class;
	
	@FindBy(xpath = "//input[@id='subclassCombocomboboxField']")
	private WebElement Subclass;
	
	@FindBy(xpath = "//input[@id='departmentFilterCombo1comboboxField']")
	private WebElement Department;
	
	@FindBy(xpath = "//input[@id='searchButton']")
	private WebElement SearchBtn;
	
	@FindBy(xpath = "//input[@id='clearButton']")
	private WebElement ClearBtn;
	
	@FindBy(xpath = "(//table[@id='configTable']/tbody//tr/td)[1]")
	private WebElement SearchResultConfigID;
	
	@FindBy(xpath = "//table[@class='details wfconfigs']//td/span[@id='workflowConfigId']")
	private WebElement SearchWorkflowConfigDetailID;
	
	@FindBy(xpath = "//input[@value='Edit']")
	private WebElement EditBtn;
	
	@FindBy(xpath = "//input[@value='Duplicate']")
	private WebElement DuplicateBtn;
	
	@FindBy(xpath = "//input[@value='Delete']")
	private WebElement DeleteBtn;
	
	@FindBy(xpath = "//div[text()='Delete Workflow Config']")
	private WebElement DeleteWorkflowPopup;
	
	@FindBy(xpath = "//input[@id='_deleteConfigForm:OkButton']")
	private WebElement OkBtnDeletePopup;
	
	@FindBy(xpath = "//input[@id='_deleteConfigForm:CancelButton']")
	private WebElement CancelBtnDeletePopup;
	
	@FindBy(xpath = "//li[@class='infomsg']/span[contains(text(),'deleted')]")
	private WebElement DeletionSuccessMsg;
	
	@FindBy(xpath = "//div[@id='statusContainer']//td[text()='Ready']")
	private WebElement LoadingProgress;
	
	
	// ***** Actions *****
	
	public boolean IsLoadingCompleted() {

		waitForVisibility(LoadingProgress);
		return LoadingProgress.isDisplayed();
	}

	public boolean enterID(String id) {
		
		ID.sendKeys(id);
		return true;
	}
	
	public boolean enterTitle(String title) {
		
		Title.sendKeys(title);
		return true;
	}
	
	public boolean selectClass(String ClassName) {
		
		Class.sendKeys(ClassName + Keys.ENTER);
		return IsLoadingCompleted();
	}
	
	public boolean selectSubclass(String SubclassName) {
		
		Subclass.sendKeys(SubclassName + Keys.ENTER);
		return IsLoadingCompleted();
	}
	
	public boolean selectDepartment(String departmentName) {
		
		Department.sendKeys(departmentName + Keys.ENTER);
		return IsLoadingCompleted();
	}
	
	public boolean searchConfig() {
		
		ScrollToBottom();
		WaitforElementVisibleAndClickable(SearchBtn);
		SearchBtn.click();
		return IsLoadingCompleted();
	}
	
	public boolean clearConfigDetails() {
		
		ScrollToBottom();
		WaitforElementVisibleAndClickable(ClearBtn);
		ClearBtn.click();
		return IsLoadingCompleted();
	}	
	
	public boolean clickOnSearchResult(String workflowID) {
		
		ScrollToViewElement(SearchResultConfigID);
		WaitforElementVisibleAndClickable(SearchResultConfigID);
		SearchResultConfigID.click();
		IsLoadingCompleted();
		
		String SearchWorkflowID = SearchWorkflowConfigDetailID.getText();
		if(workflowID.equalsIgnoreCase(SearchWorkflowID)) {
			return true;
		} else {
			return false;
		}
		
	}
	
	public EditConfigPage clickOnEditConfig() {
		
		ScrollToBottom();
		WaitforElementVisibleAndClickable(EditBtn);
		EditBtn.click();
		IsLoadingCompleted();
		return new EditConfigPage();
	}
	
	public boolean clickOnDuplicateConfig() {
		
		ScrollToBottom();
		WaitforElementVisibleAndClickable(DuplicateBtn);
		DuplicateBtn.click();
		return IsLoadingCompleted();
	}
	
	public boolean clickOnDeleteConfig() {
		
		ScrollToBottom();
		WaitforElementVisibleAndClickable(DeleteBtn);
		DeleteBtn.click();
		return IsLoadingCompleted();
	}
	
	public boolean verifyDeletePopupPresence() {
		
		return DeleteWorkflowPopup.isDisplayed();
	}
	
	public boolean confirmWorkflowDeletion() {
		
		WaitforElementVisibleAndClickable(OkBtnDeletePopup);
		OkBtnDeletePopup.click();
		return IsLoadingCompleted();
	}
	
	public boolean cancelWorkflowDeletion() {
		
		WaitforElementVisibleAndClickable(CancelBtnDeletePopup);
		CancelBtnDeletePopup.click();
		return IsLoadingCompleted();
	}
	
	public boolean verifyWorkflowDeletionMsg() {
		
		IsLoadingCompleted();
		return DeletionSuccessMsg.isDisplayed();
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
