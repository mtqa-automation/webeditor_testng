package com.setupfc.qa.pages.SetupFC;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.setupfc.qa.util.TestUtil;

public class TaskActionsPage extends TestUtil{

	public TaskActionsPage() {
		PageFactory.initElements(driver, this);
	}
	
	// ***** Page Factory OR *****
	private WebElement ApprovalOrReject_Btn(String BtnName) {
		return driver.findElement(By.xpath("//input[@value='" + BtnName + "']"));
	}
	
	@FindBy(id = "taskActions:submitComment")
	private WebElement SubmitComment;
	
	@FindBy(xpath = "//input[@id='_approveTaskForm:OkButton']")
	private WebElement ApprovalOkButton;
	
	@FindBy(xpath = "//input[@id='_approveTaskForm:CancelButton']")
	private WebElement ApprovalCancelButton;
	
	@FindBy(xpath = "//div[@id='statusContainer']//td[text()='Ready']")
	private WebElement LoadingProgress;
	
	// ***** Actions *****
	
	public boolean IsLoadingCompleted() {

		waitForVisibility(LoadingProgress);
		return LoadingProgress.isDisplayed();
	}
	
	
	public boolean approveSpecification(String ApproveComment) {
		
		WaitforElementVisibleAndClickable(SubmitComment);
		SubmitComment.click();
		SubmitComment.sendKeys(ApproveComment);
		WaitforElementVisibleAndClickable(ApprovalOrReject_Btn("Approve"));
		ApprovalOrReject_Btn("Approve").click();
		
		waitForVisibility(ApprovalOkButton);
		ApprovalOkButton.click();
		IsLoadingCompleted();
		
		return true;
		
	}

	public boolean rejectSpecification(String ApproveComment) {

		WaitforElementVisibleAndClickable(SubmitComment);
		SubmitComment.click();
		SubmitComment.sendKeys(ApproveComment);
		WaitforElementVisibleAndClickable(ApprovalOrReject_Btn("Reject"));
		ApprovalOrReject_Btn("Reject").click();
		
		MaxWait();
		return true;
	}
	
	
	
	
	
	
	
}
