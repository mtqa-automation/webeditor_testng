/**
 * 
 */
package com.setupfc.qa.pages.SetupFC;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.setupfc.qa.base.TestBase;

/**
 * @author smallika
 *
 * @ModifiedBy rhalli
 * 
 */
public class LoginPage extends TestBase {
	
	// Initializing the page object
	public LoginPage() {

		PageFactory.initElements(driver, this);
	}
		
	// ***** Page factory OR *****

	@FindBy(xpath = "//a[contains(text(),'Follow this link to automatically re-login (single')]")
	private WebElement SingleSigninBtn;

	@FindBy(xpath = "//input[@id='loginForm:username']")
	private WebElement UserName;

	@FindBy(xpath = "//input[@id='loginForm:password']")
	private WebElement Password;

	@FindBy(xpath = "//input[@id='loginForm:submit']")
	private WebElement LoginBtn;


	// ***** Actions *****
	
	public HomePage LoginbySingleSignOn() {
		
		SingleSigninBtn.click();
		return new HomePage();
	}

	public HomePage Login(String un, String pwd) {
		
		UserName.sendKeys(un);
		Password.sendKeys(pwd);
		LoginBtn.click();
		return new HomePage();
	}

}
