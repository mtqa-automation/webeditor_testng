/**
 * 
 */
package com.setupfc.qa.pages.TableEditor;

import org.openqa.selenium.support.PageFactory;

import com.setupfc.qa.util.TestUtil;

/**
 * @author smallika
 *
 */
public class WebTableMainPage extends TestUtil{
	//Initializing the page object 
	public WebTableMainPage()
	{
		PageFactory.initElements(driver, this);
	}
	
}
