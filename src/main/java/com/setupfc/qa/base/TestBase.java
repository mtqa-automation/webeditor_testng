/**
 * 
 */
package com.setupfc.qa.base;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.setupfc.qa.config.IniFile;
import com.setupfc.qa.listeners.Webdrivereventlistener;
import com.setupfc.qa.util.RandomValueGenerator;
import com.setupfc.qa.util.TestUtil;

import io.github.bonigarcia.wdm.WebDriverManager;

/**
 * @author smallika
 *
 */
@SuppressWarnings("deprecation")
public class TestBase {

	public static WebDriver driver;
	public static EventFiringWebDriver driver1;
	public static IniFile ini = IniFile.GetInstance();
	static String Env = TestUtil.Environment;

	public void initilization() {
		
		RandomValueGenerator.assignRandomValues();
		driver = BrowerLuanch(ini.ReadIniFile(Env, "EnvironmentBrowser"));
		TestUtil.ParentWindow = driver.getWindowHandle();
		driver.get(ini.ReadIniFile(Env, "EnvironmentUrl"));
		
	}

	private static WebDriver BrowerLuanch(String BrowserName) {
		
		WebDriver driver = null;
		
		if (BrowserName.equalsIgnoreCase("CHROME")) {
			
			WebDriverManager.chromedriver().setup();
			driver = new ChromeDriver();
		} 
		
		else if (BrowserName.equalsIgnoreCase("FF")) {
			
			WebDriverManager.firefoxdriver().setup();
			driver = new FirefoxDriver();
		} 
		
		else if (BrowserName.equalsIgnoreCase("IE")) {
			
			WebDriverManager.iedriver().setup();
			driver = new InternetExplorerDriver();
		} 
		
		else {
			WebDriverManager.chromedriver().setup();
			driver = new ChromeDriver();
		}
		
		driver.manage().deleteAllCookies();
		driver.manage().timeouts().pageLoadTimeout(TestUtil.PAGELAYOT_TIMEOUT, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(TestUtil.IMPLICT_TIMEOUT, TimeUnit.SECONDS);
		driver.manage().window().maximize();

		// Event firing driver
		driver1 = new EventFiringWebDriver(driver);
		driver1.register(new Webdrivereventlistener());
		return driver1;
	}

	public void CloseBrowser() {
		
		System.out.println("closing the browser");
		driver.quit();
	}
}
