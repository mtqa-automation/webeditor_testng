package com.setupfc.qa.config;

import java.io.File;
import java.io.IOException;

import org.apache.log4j.Logger;
import org.ini4j.Wini;

/**
 * @author smallika
 *
 */
public class IniFile {
	
	private static Wini Ini;
	private static Logger logger = Logger.getLogger(IniFile.class);
	private static IniFile inifile;

	public static void main(String[] args) {

	}

	/**
	 * private constructor
	 */
	private IniFile() {
		LoadIniFile();
	}

	/**
	 * defined COnstructer
	 * 
	 * @param IniFilePath
	 */
	private IniFile(String IniFilePath) {
		LoadIniFile(IniFilePath);
	}

	/**
	 * Instance of IniFile class
	 * 
	 * @return IniFile
	 */
	public static IniFile GetInstance() {
		if (inifile == null)
			inifile = new IniFile();
		return inifile;
	}

	/**
	 * @param IniFilePath
	 * @return IniFile
	 */
	public static IniFile GetInstance(String IniFilePath) {
		if (inifile == null)
			inifile = new IniFile(IniFilePath);
		return inifile;
	}

	private void LoadIniFile() {
		
		try {
			Ini = new Wini(new File(".//src/main/resources/IniFile.ini"));
		} catch (IOException e) {
			logger.error("Error occured while loading the file: " + e);
		}
	}

	/**
	 * @param IniFilePath
	 */
	private void LoadIniFile(String IniFilePath) {
		
		try {
			if (IniFilePath.endsWith(".ini"))
				Ini = new Wini(new File(IniFilePath));
			else {
				logger.warn(IniFilePath + " is not valid ini file and loading default iniFile");
				LoadIniFile();
			}

		} catch (IOException e) {
			logger.error("Error occured while loading the file: " + e);
		}
	}

	/**
	 * @param section
	 * @param Key
	 * @return String
	 */
	public String ReadIniFile(String section, String Key) {

		return Ini.get(section, Key);

	}

	/**
	 * @param Section
	 * @param Key
	 * @param value
	 */
	public void WriteIniFile(String Section, String Key, String value) {

		try {
			Ini.put(Section, Key, value);
			logger.info("[ " + Key + " ] is updated with values [ " + value + "] in the section [" + Section + " ]");
			Ini.store();
		} catch (IOException e) {
			logger.error("Error occured while loading the file: " + e);
		}

	}
}
