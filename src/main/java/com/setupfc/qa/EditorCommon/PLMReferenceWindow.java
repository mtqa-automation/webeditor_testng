package com.setupfc.qa.EditorCommon;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.setupfc.qa.util.TestUtil;

/**
 * @author rhalli
 * 
 **/
public class PLMReferenceWindow extends TestUtil{
	
	// Initializing the page object
	public PLMReferenceWindow() {
		
		PageFactory.initElements(driver, this);
	}
	
	// ***** Page Factory *****
	
	@FindBy(id = "inputSpecId")
	private WebElement SpecIDInputTbox;
	
	@FindBy(id = "confirm-add-plm-reference")
	private WebElement OkButton;
	
	@FindBy(xpath = "//button[@class='btn btn-secondary']")
	private WebElement CancelBtn;
	
	// ***** Actions *****
	
	public boolean enterSpecID(String SpecId) {

		WaitforElementVisibleAndClickable(SpecIDInputTbox);
		SpecIDInputTbox.sendKeys(SpecId);
		WaitInSeconds("2");
		return true;
	}
	
	public EditorMainPage addPLMReferenceOk() {
		
		WaitforElementVisibleAndClickable(OkButton);
		ScrollToViewElement(OkButton);
		OkButton.click();
		WaitInSeconds("3");
		return new EditorMainPage();
	}
	
	public EditorMainPage cancelPLMReferenceAdd() {
		
		WaitforElementVisibleAndClickable(CancelBtn);
		ScrollToViewElement(CancelBtn);
		CancelBtn.click();
		WaitInSeconds("3");
		return new EditorMainPage();
	}

}
