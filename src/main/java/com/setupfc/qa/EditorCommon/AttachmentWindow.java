package com.setupfc.qa.EditorCommon;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.setupfc.qa.util.TestUtil;


/**
 * 
 * @author rhalli
 * 
 * */
public class AttachmentWindow extends TestUtil {
	
	// Initializing the page object
	public AttachmentWindow() {
		PageFactory.initElements(driver, this);
	}

	
	// ***** Page Factory *****
	@FindBy(xpath = "//span[contains(@class,'dialog-title')]")
	private WebElement DialogHeader;
	
	@FindBy(xpath = "//span[text()='Choose']//parent::span/input")
	private WebElement ChooseButton;
	
	@FindBy(xpath = "//span[text()='Upload']//parent::button")
	private WebElement UploadButton;
	
	@FindBy(xpath = "//span[text()='Cancel']//parent::button")
	private WebElement CancelButton;
	
	@FindBy(xpath = "//span[contains(@class,'header-close-icon')]/parent::button")
	private WebElement CloseWindowButton;
	
	
	// ***** Actions *****
	public boolean chooseAttachment(String FileName) {
		if (!FileName.contains(":")) {
			FileName = FileName.replace("./", "").replace("/", "\\");
			FileName = System.getProperty("user.dir") + "\\" + FileName;
		}
		ChooseButton.sendKeys(FileName);
		return true;
	}
	
	public EditorMainPage uploadChoosenAttachment() {
		
		WaitforElementVisibleAndClickable(UploadButton);
		UploadButton.click();
		//WaitforElementInVisible(DialogHeader);
		WaitforElementInVisible(By.xpath("//span[contains(@class,'dialog-title')]"));
		return new EditorMainPage();
	}
	
	public EditorMainPage cancelAttachmentUpload() {
		
		WaitforElementVisibleAndClickable(CancelButton);
		CancelButton.click();
		//WaitforElementInVisible(DialogHeader);
		WaitforElementInVisible(By.xpath("//span[contains(@class,'dialog-title')]"));
		return new EditorMainPage();
	}
	
	public EditorMainPage closeAttachmentWindow() {
		
		WaitforElementVisibleAndClickable(CloseWindowButton);
		CloseWindowButton.click();
		//WaitforElementInVisible(DialogHeader);
		WaitforElementInVisible(By.xpath("//span[contains(@class,'dialog-title')]"));
		return new EditorMainPage();
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
