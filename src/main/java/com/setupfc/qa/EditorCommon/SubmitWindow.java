package com.setupfc.qa.EditorCommon;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.setupfc.qa.pages.SetupFC.TaskPage;
import com.setupfc.qa.util.TestUtil;

/**
 * 
 * @author rhalli
 * */
public class SubmitWindow extends TestUtil{
	
	// Initializing the page object
	public SubmitWindow() {
		PageFactory.initElements(driver, this);
	}
	
	// Object Repository
	@FindBy(xpath = "//span[contains(@class,'dialog-title')]")
	private WebElement WindowTitle;
	
	@FindBy(xpath = "//span[text()='Open full screen']//parent::button")
	private WebElement OpenFullScreenBtn;
	
	@FindBy(xpath = "//span[text()='Cancel']//parent::button")
	private WebElement CancelButton;
	
	@FindBy(xpath = "//span[text()='Submit']//parent::div//parent::button")
	private WebElement SubmitButton;
	
	@FindBy(xpath = "//span[text()='Submit successful']")
	private WebElement SubmitSuccessPopup;
	
	@FindBy(xpath = "//div[@class='modal-body']//p[text()='Specification successfully submitted.']")
	private WebElement SubmitSuccessMessage;
	
	@FindBy(xpath = "//span[text()='Back to setup.FC']//parent::button/parent::a")
	private WebElement BackToSetupFC;
	
	@FindBy(xpath = "//span[text()='Go to Home']//parent::button")
	private WebElement GoToHome;
	
	// ***** Actions *****
	
	public boolean submitSpecification() {
		
		WaitforElementVisibleAndClickable(SubmitButton);
		ScrollToViewElement(SubmitButton);
		SubmitButton.click();
		waitForVisibility(SubmitSuccessPopup);
		if(SubmitSuccessMessage.isDisplayed()) {
			return true;
		} else {
			return false;
		}
		
	}
	
	public TaskPage clickOnBackToSetupFC() {
		
		waitForVisibility(BackToSetupFC);
		MaxWait();
		BackToSetupFC.click();
		return new TaskPage();
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
