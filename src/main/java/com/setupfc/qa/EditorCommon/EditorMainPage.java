package com.setupfc.qa.EditorCommon;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ListIterator;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.setupfc.qa.pages.SetupFC.TaskPage;
import com.setupfc.qa.util.TestUtil;

/**
 * 
 * @author rhalli
 * 
 **/
public class EditorMainPage extends TestUtil{
	
	//Initializing the page object
	public EditorMainPage() {
		PageFactory.initElements(driver, this);
	}
	
	// ***** Page Factory *****
	@FindBy(xpath = "//div[@class='p-scrollpanel-content']/gf-chapter-tree-viewer")
	private WebElement ChapterTreeContent;
	
	@FindBy(id = "toggleChapterMenue")
	private WebElement ChapterTreeButton;
	
	@FindBy(className="navbar-title")
	private WebElement EditorTitle;
	
	@FindBy(xpath="//a[@id='account-menu']//span/span")
	private WebElement LoggedUser;
	
	@FindBy(id="menu-spec")
	private WebElement SpecificationDropDown;
	
	@FindBy(id="menu-add")
	private WebElement AddDropDown;
	
	// ** Specification metadata locators **
	
	@FindBy(xpath="//table[@id='MetaInformation']//tbody//tr//th[1]")
	private WebElement MetaInfo_SpecID;
	
	@FindBy(xpath="//table[@id='MetaInformation']//tbody//tr//th[@colspan='2']")
	private WebElement MetaInfo_Status;
	
	@FindBy(xpath="//table[@id='MetaInformation']//tbody//tr//td[@colspan='3']")
	private WebElement MetaInfo_Name;
	
	@FindBy(xpath="//span[contains(text(),'Author:')]/following-sibling::span")
	private WebElement MetaInfo_Author;
	
	@FindBy(xpath="//span[contains(text(),'Department:')]/following-sibling::span")
	private WebElement MetaInfo_Department;
	
	@FindBy(xpath="//span[contains(text(),'Owner:')]/following-sibling::span")
	private WebElement MetaInfo_Owner;
	
	@FindBy(xpath="//span[contains(text(),'WorkflowConfiguration:')]/following-sibling::span")
	private WebElement MetaInfo_Workflow;
	
	@FindBy(xpath="//span[contains(text(),'SubClass:')]/following-sibling::span")
	private WebElement MetaInfo_Subclass;
	
	@FindBy(xpath="//span[contains(text(),'GroupAccessLevel:')]/following-sibling::span")
	private WebElement MetaInfo_GroupAccessLevel;
	
	@FindBy(xpath="//span[contains(text(),'Template:')]/following-sibling::span")
	private WebElement MetaInfo_Template;
	
	@FindBy(xpath="//span[contains(text(),'UseCase:')]/following-sibling::span")
	private WebElement MetaInfo_UseCase;
	
	@FindBy(xpath="//span[contains(text(),'UseCase:')]/following-sibling::button")
	private WebElement MetaInfo_ChangeUseCaseBtn;
	
	@FindBy(xpath = "//select[@id='selectUseCase']")
	private WebElement MetaInfo_SelectUseCase;
	
	@FindBy(xpath = "//span[text()='OK']//parent::button")
	private WebElement MetaInfo_ConfirmUseCaseChange;
	
	@FindBy(xpath = "//span[text()='Cancel']//parent::button")
	private WebElement MetaInfo_CancelUseCaseChange;
	
	// ** Main View detail fields locators**
	
	@FindBy(id = "field_pcrb")
	private WebElement PCRB;
	
	@FindBy(id = "field_changeReason")
	private WebElement ChangeReason;
	
	@FindBy(xpath = "//*[@id='field_changeReason']/following-sibling::div//small")
	private WebElement ChangeReason_ErrorMsg;
	
	@FindBy(id = "field_changeDescription")
	private WebElement ChangeDescription;
	
	@FindBy(xpath = "//*[@id='field_changeDescription']/following-sibling::div//small")
	private WebElement ChangeDescription_ErrorMsg;
	
	@FindBy(xpath = "//*[@id='edit-purpose']//editor/div")
	private WebElement Purpose;
	
	@FindBy(xpath = "//*[@id='edit-purpose']//span[text()='Click for quick editing (text only)']")
	private WebElement PurposeQuickEdit;
	
	@FindBy(xpath = "//*[@id='edit-purpose']//div//small")
	private WebElement Purpose_ErrorMsg;
	
	@FindBy(xpath = "//*[@id='edit-scope']//editor/div")
	private WebElement Scope;
	
	@FindBy(xpath = "//*[@id='edit-scope']//span[text()='Click for quick editing (text only)']")
	private WebElement ScopeQuickEdit;
	
	@FindBy(xpath = "//*[@id='edit-scope']//div//small")
	private WebElement Scope_ErrorMsg;
	
	@FindBy(xpath = "//*[@id='edit-definitions']//editor/div")
	private WebElement Definitions;
	
	@FindBy(xpath = "")
	private WebElement DefinitionsQuickEdit;
	
	// ** Specification and Add drop down list locators- **
	
	private WebElement MenuDropdown(String dropdownName) {
		return driver.findElement(By.xpath("//span[text()='"+ dropdownName +"']/parent::a"));
	}
	
	@FindBy(xpath = "//a[contains(@id,'menu-spec-')]/span[2]")
	private List<WebElement> SpecificationDpDwOptions;
	
	@FindBy(xpath = "//a[contains(@id,'menu-add-')]/span[2]")
	private List<WebElement> AddDpDwOptions;
	
	// Dialog header xpath
	private WebElement DialogHeader(String dialogTitle) {
		return driver.findElement(By.xpath("//span[contains(@class,'dialog-title')][text()='"+ dialogTitle +"']"));
	}
		
	// Attachments or References locators
	
	private WebElement ReferenceOrAttachmentHeading(String sectionName) {
		return driver.findElement(By.xpath("//span[text()='"+ sectionName +"']"));
	}
	
	private List<WebElement> ReferenceOrAttachmentList(String sectionName) {
		return driver.findElements(By.xpath("//span[text()='"+ sectionName +"']/following-sibling::div//fa-icon/parent::div"));
	}
	
	private List<WebElement> ReferenceOrAttachmentViewBtn(String sectionName) {
		return driver.findElements(By.xpath("//span[text()='"+ sectionName +"']/following-sibling::div//a[@rel='noopener']"));
	}
	
	private List<WebElement> ReferenceOrAttachmentDeleteBtn(String sectionName) {
		return driver.findElements(By.xpath("//span[text()='"+ sectionName +"']/following-sibling::div//button[@class='btn btn-danger btn-sm ng-star-inserted']"));
	}
	
	// ** PLM Reference section locators **
	
	@FindBy(xpath="//div[@id='plm-references-container']//fa-icon/parent::div")
	private List<WebElement>  PLMReferenceNameList;
	
	@FindBy(xpath="//div[@id='plm-references-container']//a[@rel='noopener']")
	private List<WebElement>  PLMReferenceViewBtn;
	
	@FindBy(xpath="//div[@id='plm-references-container']//div//button[@class='btn btn-danger btn-sm ng-star-inserted']")
	private List<WebElement>  PLMReferenceDeleteBtn;
	
	// ** Delete dialogs locators
	
	@FindBy(xpath = "//span[text()='Delete Attachment']")
	private WebElement DeleteAttachmentDialog;
	
	@FindBy(xpath = "//span[text()='Remove Reference']")
	private WebElement RemoveReferenceDialog;
	
	@FindBy(xpath = "//span[text()='Remove PLM Reference']")
	private WebElement RemovePLMReferenceDialog;
	
	@FindBy(id = "gf-confirm-delete-attachment")
	private WebElement DeleteConfirmation;
	
	@FindBy(xpath = "//span[text()='Cancel']/parent::button")
	private WebElement CancelConfirmation;
	
	@FindBy(xpath = "//span[contains(@class,'dialog-header-close-icon')]/parent::button")
	private WebElement CloseDeleteDialog;
	
	// Alerts in the web editors
	@FindBy(xpath = "//p-toastitem//div[@role='alert']")
	private WebElement AlertMessages;
	
	@FindBy(xpath = "//div[text()='Successfully uploaded to setup.FC']")
	private WebElement SaveSuccessAlert;
	
	@FindBy(xpath = "//div[text()='Validation ok!']")
	private WebElement ValidationSuccessAlert;
	
	@FindBy(xpath = "//div[text()='There are validation errors!']")
	private WebElement ValidationErrorAlert;
	
	@FindBy(xpath = "//div[@role='dialog']//span[contains(@class,'dialog-title')]")
	private WebElement DiscardDialogTitle;
	
	@FindBy(xpath = "//div[@role='dialog']//p[1]")
	private WebElement DiscardDialogContent;
	
	@FindBy(xpath = "//div[@role='dialog']//span[text()='OK']/parent::button")
	private WebElement DiscardDialogOkBtn;
	
	// ****** Actions *****
	
	public String getEditorTitle() {
		
		return EditorTitle.getText();
	}
	
	public String getLoggedUserID() {
		
		return LoggedUser.getText();
	}
	
	public String getMetaInformation() {
		
		String value = "";
		value+= MetaInfo_SpecID.getText().split(" - ")[0].trim();
		value+= ", " + MetaInfo_Status.getText().trim();
		value+= ", " + MetaInfo_Name.getText().trim();
		value+= ", " + MetaInfo_Author.getText().trim();
		value+= ", " + MetaInfo_Department.getText().trim();
		value+= ", " + MetaInfo_Owner.getText().trim();
		value+= ", " + MetaInfo_Workflow.getText().trim();
		value+= ", " + MetaInfo_Subclass.getText().trim();
		value+= ", " + MetaInfo_Template.getText().trim();
		value+= ", " + MetaInfo_UseCase.getText().trim();
		
		return value;
	}
	
	public String getSpecID() {
		
		return MetaInfo_SpecID.getText().split(" - ")[0].trim();
	}
	
	public String getSpecStatus() {

		return MetaInfo_Status.getText();
	}

	public String getSpecTitle() {
		
		return MetaInfo_Name.getText();
	}
	
	public String getSpecAuthor() {
		
		return MetaInfo_Author.getText();
	}
	
	public String getSpecDepartment() {
		
		return MetaInfo_Department.getText();
	}
	
	public String getSpecOwner() {
		
		return MetaInfo_Owner.getText();
	}
	
	public String getSpecWorkflowConfig() {
		
		return MetaInfo_Workflow.getText();
	}
	
	public String getSpecSubClass() {
		
		return MetaInfo_Subclass.getText();
	}
	
	public String getTemplate() {
		
		return MetaInfo_Template.getText();
	}
	
	public String getUseCase() {
		
		return MetaInfo_UseCase.getText();
	}
	
	public void closeChapterTree() {
		
		waitForPageLoad();
		if(ChapterTreeContent.isDisplayed()) {
			ChapterTreeButton.click();
			WaitInSeconds("5");
		}
	}
	
	public boolean verifyDropdownOptions(String dpdwName) {
		
		if(dpdwName.equalsIgnoreCase("Specification")) {
			
			List<String> expectedOptions = new ArrayList<String>();
			List<String> actualOptions = Arrays
					.asList("Preview", "Save to setup.FC", "Submit", "Validate", "Compare", 
							"Reload from setup.FC", "Export", "Import", "View XML data", "View SUBCLASS config");
			
			WaitforElementVisibleAndClickable(MenuDropdown("Specification"));
			MenuDropdown("Specification").click();
			
			ListIterator<WebElement> Listiterator = SpecificationDpDwOptions.listIterator();
			while (Listiterator.hasNext()) {
				WebElement ele = Listiterator.next();
				String eleText = ele.getText();
				expectedOptions.add(eleText);
			}
			
			return expectedOptions.containsAll(actualOptions);
			
		}
		
		else if(dpdwName.equalsIgnoreCase("Add")) {
			
			List<String> expectedOptions = new ArrayList<String>();
			List<String> actualOptions = Arrays
					.asList("Attachment", "Reference", "PLM Reference", "Keyword");

			WaitforElementVisibleAndClickable(MenuDropdown("Add"));
			MenuDropdown("Add").click();

			ListIterator<WebElement> Listiterator = AddDpDwOptions.listIterator();
			while (Listiterator.hasNext()) {
				WebElement ele = Listiterator.next();
				String eleText = ele.getText();
				expectedOptions.add(eleText);
			}

			return expectedOptions.containsAll(actualOptions);
		}
		
		else {
			return false;
		}
		
	}
	
	public boolean addPCRB(String pcrbValue) {
		
		WaitforElementVisibleAndClickable(PCRB);
		PCRB.click();
		ClearByAction();
		if(!pcrbValue.isEmpty())
			PCRB.sendKeys(pcrbValue);
		return true;
	}
	
	public boolean addChangeReason(String changeReasonValue) {
		
		WaitforElementVisibleAndClickable(ChangeReason);
		ScrollToViewElement(ChangeReason);
		ChangeReason.click();
		WaitInSeconds("2");
		ClearByAction();
		if(!changeReasonValue.isEmpty())
			ChangeReason.sendKeys(changeReasonValue);
		WaitInSeconds("1");
		return true;
	}
	
	public boolean verifyChangeReasonErrorMessage(String expectedErorMsg) {
		
		ChangeReason.click();
		ClearByAction();
		waitForVisibility(ChangeReason_ErrorMsg);
		String actualErrorMsg = ChangeReason_ErrorMsg.getText();
		
		if(actualErrorMsg.contains(expectedErorMsg))
			return true;
		else
			return false;
	}
	
	public boolean addChangeDescription(String changeDescriptionValue) {
		
		WaitforElementVisibleAndClickable(ChangeDescription);
		ScrollToViewElement(ChangeDescription);
		ChangeDescription.click();
		WaitInSeconds("2");
		ClearByAction();
		if(!changeDescriptionValue.isEmpty())
			ChangeDescription.sendKeys(changeDescriptionValue);
		WaitInSeconds("1");
		return true;
			
	}
	
	public boolean verifyChangeDescriptionErrorMessage(String expectedErrorMsg) {
		
		ChangeDescription.click();
		ClearByAction();
		waitForVisibility(ChangeDescription_ErrorMsg);
		String actualErrorMsg = ChangeDescription_ErrorMsg.getText();
		
		if(actualErrorMsg.contains(expectedErrorMsg))
			return true;
		else
			return false;
	}
	
	public boolean addPurpose(String purposeValue) {
		
		WaitforElementVisibleAndClickable(Purpose);
		ScrollToViewElement(Purpose);
		Purpose.click();
		WaitforElementVisibleAndClickable(Purpose);
		WaitInSeconds("2");
		ClearByAction();
		if(!purposeValue.isEmpty())
			Purpose.sendKeys(purposeValue);
		WaitInSeconds("1");
		return true;
	}
	
	public boolean verifyPurposeErrorMessage(String expectedErrorMsg) {

		Purpose.click();
		WaitforElementVisibleAndClickable(Purpose);
		WaitInSeconds("2");
		ClearByAction();
		waitForVisibility(Purpose_ErrorMsg);
		String actualErrorMsg = Purpose_ErrorMsg.getText();

		if (actualErrorMsg.contains(expectedErrorMsg))
			return true;
		else
			return false;
	}
	
	public boolean addScope(String scopeValue) {
		
		WaitforElementVisibleAndClickable(Scope);
		ScrollToViewElement(Scope);
		Scope.click();
		WaitforElementVisibleAndClickable(Scope);
		WaitInSeconds("2");
		ClearByAction();
		if(!scopeValue.isEmpty())
			Scope.sendKeys(scopeValue);
		WaitInSeconds("1");
		return true;
	}
	
	public boolean verifyScopeErrorMessage(String expectedErrorMsg) {

		Scope.click();
		WaitforElementVisibleAndClickable(Scope);
		WaitInSeconds("2");
		ClearByAction();
		waitForVisibility(Scope_ErrorMsg);
		String actualErrorMsg = Scope_ErrorMsg.getText();

		if (actualErrorMsg.contains(expectedErrorMsg))
			return true;
		else
			return false;
	}
	
	public boolean addDefinitions(String definitionsValue) {
		
		WaitforElementVisibleAndClickable(Definitions);
		ScrollToViewElement(Definitions);
		Definitions.click();
		WaitforElementVisibleAndClickable(Definitions);
		WaitInSeconds("2");
		ClearByAction();
		if(!definitionsValue.isEmpty())
			Definitions.sendKeys(definitionsValue);
		WaitInSeconds("1");
		return true;
	}
	
	public boolean validateSuccess() {
		
		WaitforElementVisibleAndClickable(MenuDropdown("Specification"));
		MenuDropdown("Specification").click();
		WaitforElementVisibleAndClickable(MenuDropdown("Validate"));
		MenuDropdown("Validate").click();
		waitForVisibility(ValidationSuccessAlert);
		if(ValidationSuccessAlert.isDisplayed()) {
			return true;
		} else {
			return false;
		}
	}
	
	public boolean validateError() {
		
		WaitforElementVisibleAndClickable(MenuDropdown("Specification"));
		MenuDropdown("Specification").click();
		WaitforElementVisibleAndClickable(MenuDropdown("Validate"));
		MenuDropdown("Validate").click();
		waitForVisibility(ValidationErrorAlert);
		if(ValidationErrorAlert.isDisplayed()) {
			return true;
		} else {
			return false;
		}
	}
	
	public AttachmentWindow openAttachmentWindow() {
		
		WaitforElementVisibleAndClickable(MenuDropdown("Add"));
		MenuDropdown("Add").click();
		WaitforElementVisibleAndClickable(MenuDropdown("Attachment"));
		MenuDropdown("Attachment").click();
		waitForVisibility(DialogHeader("Upload Attachment"));
		return new AttachmentWindow();
	}
	
	public ReferenceWindow openReferenceWindow() {

		WaitforElementVisibleAndClickable(MenuDropdown("Add"));
		MenuDropdown("Add").click();
		WaitforElementVisibleAndClickable(MenuDropdown("Reference"));
		MenuDropdown("Reference").click();
		waitForVisibility(DialogHeader("Append References"));
		return new ReferenceWindow();
	}
	
	public PLMReferenceWindow openPLMReferenceWindow() {
		
		WaitforElementVisibleAndClickable(MenuDropdown("Add"));
		MenuDropdown("Add").click();
		WaitforElementVisibleAndClickable(MenuDropdown("PLM Reference"));
		MenuDropdown("PLM Reference").click();
		waitForVisibility(DialogHeader("Append PLM References"));
		return new PLMReferenceWindow();
	}
	
	public String viewReferenceOrAttachment(String SectionName, String AttachmentName) {
		
		ScrollToViewElement(ReferenceOrAttachmentHeading(SectionName));
		
		String value="";
		ParentWindow=GetParentWindow();
		int temp=0;
		boolean elementFindFlg=false;
		ListIterator<WebElement> elementList = ReferenceOrAttachmentList(SectionName).listIterator();
		
		while(elementList.hasNext()) {
			
			WebElement element=elementList.next();
			if(element.getText().contains(AttachmentName)) {
				
				ScrollToViewElement(ReferenceOrAttachmentViewBtn(SectionName).get(temp));
				ReferenceOrAttachmentViewBtn(SectionName).get(temp).click();
				elementFindFlg=true;
			}
			temp++;
		}
		if(!elementFindFlg)
			return null;
		SwitchtoNewWindow();
		value=driver.getTitle();
		driver.close();
		SwitchtoParentWindow();
		return value;
	}
	
	public boolean deleteReferenceOrAttachment(String sectionName, String AttachmentName, String deleteDialogTitle) {
		
		int temp=0;
		ListIterator<WebElement> elementList = ReferenceOrAttachmentList(sectionName).listIterator();
		
		while(elementList.hasNext()) {
			
			WebElement element=elementList.next();
			if(element.getText().contains(AttachmentName)) {
				ReferenceOrAttachmentDeleteBtn(sectionName).get(temp).click();
			}
			temp++;
		}
		return DialogHeader(deleteDialogTitle).isDisplayed();
	}
	
	public boolean confirmDelete() {
		
		WaitforElementVisibleAndClickable(DeleteConfirmation);
		DeleteConfirmation.click();
		WaitforElementInVisible(By.xpath("//span[contains(@class,'dialog-title')]"));
		return true;
	}
	
	public boolean confirmCancel() {
		
		WaitforElementVisibleAndClickable(CancelConfirmation);
		CancelConfirmation.click();
		WaitforElementInVisible(By.xpath("//span[contains(@class,'dialog-title')]"));
		return true;
	}
	
	public boolean saveToSetupFC() {
		
		WaitforElementVisibleAndClickable(MenuDropdown("Specification"));
		MenuDropdown("Specification").click();
		WaitforElementVisibleAndClickable(MenuDropdown("Save to setup.FC"));
		MenuDropdown("Save to setup.FC").click();
		waitForVisibility(SaveSuccessAlert);
		if(SaveSuccessAlert.isDisplayed()) {
			return true;
		} else {
			return false;
		}
	}
	
	public SubmitWindow openSubmitWindow() {
		
		WaitforElementVisibleAndClickable(MenuDropdown("Specification"));
		MenuDropdown("Specification").click();
		WaitforElementVisibleAndClickable(MenuDropdown("Submit"));
		MenuDropdown("Submit").click();
		return new SubmitWindow();
	}
	
	public TaskPage switchBackToSetupfcTaskPage() {
		
		SwitchtoParentWindow();
		return new TaskPage();
	}
	
	public boolean verifySpecContentOutdatedPopup() {
		
		waitForVisibility(DiscardDialogTitle);
		String dialogTitle = DiscardDialogTitle.getText();
		String dialogContent = DiscardDialogContent.getText();
		if(dialogTitle.equalsIgnoreCase("Specification content outdated") && dialogContent.equalsIgnoreCase("The content of this specification might be outdated!")) {
			ScrollToViewElement(DiscardDialogOkBtn);
			DiscardDialogOkBtn.click();
			MediumWait();
			return true;
		} else {
			return false;
		}
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
