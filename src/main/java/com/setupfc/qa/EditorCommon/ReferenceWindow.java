package com.setupfc.qa.EditorCommon;

import java.util.List;
import java.util.ListIterator;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.setupfc.qa.util.TestUtil;


/**
 * 
 * @author rhalli
 * 
 * */
public class ReferenceWindow extends TestUtil{
	
	// Initializing the page object
	public ReferenceWindow() {
		PageFactory.initElements(driver, this); 
	}
	
	// ***** Page Factory *****
	
	@FindBy(id = "inputSpecId")
	private WebElement SpecIDInputTbox;
	
	@FindBy(id = "start-search")
	private WebElement SearchButton;
	
	@FindBy(xpath = "//table/tbody[@class='p-datatable-tbody']//td[1]")
	private List<WebElement> SearchResults;
	
	@FindBy(id = "add-ref-ok")
	private WebElement OkButton;
	
	@FindBy(xpath = "//button[@class='btn btn-secondary']")
	private WebElement CancelBtn;
	
	
	// ***** Actions *****
	
	public boolean enterSpecID(String SpecId) {
		
		WaitforElementVisibleAndClickable(SpecIDInputTbox);
		SpecIDInputTbox.sendKeys(SpecId);
		WaitInSeconds("2");
		return true;
	}
	
	public boolean startReferenceSearch() {
		
		ScrollToViewElement(SearchButton);
		SearchButton.click();
		return true;
	}
	
	public boolean selectSearchedResult(String specId) {
		
		ListIterator<WebElement> results = SearchResults.listIterator();
		while(results.hasNext()) {
			
			WebElement ele = results.next();			
			if(ele.getText().contains(specId)) {
				ScrollToViewElement(ele);
				ele.click();
				break;
			}
			
		}
		
		return true;
	}
	
	public EditorMainPage addReferenceOk() {
		
		WaitforElementVisibleAndClickable(OkButton);
		ScrollToViewElement(OkButton);
		OkButton.click();
		WaitInSeconds("3");
		return new EditorMainPage();
	}
	
	public EditorMainPage cancelReferenceAdd() {
		
		WaitforElementVisibleAndClickable(CancelBtn);
		ScrollToViewElement(CancelBtn);
		CancelBtn.click();
		WaitInSeconds("3");
		return new EditorMainPage();
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
