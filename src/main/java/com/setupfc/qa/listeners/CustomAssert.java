/**
 * 
 */
package com.setupfc.qa.listeners;




import java.util.regex.Pattern;

import org.testng.asserts.IAssert;
import org.testng.asserts.SoftAssert;

import com.setupfc.qa.util.TestReport;

/**
 * @author smallika
 *
 */
public class CustomAssert extends SoftAssert{


	@Override
	protected void doAssert(IAssert<?> a) {
		// TODO Auto-generated method stub
		super.doAssert(a);
	}

	@Override
	public void assertAll() {
		// TODO Auto-generated method stub
		super.assertAll();
	}

	@Override
	public void assertAll(String message) {
		// TODO Auto-generated method stub
		super.assertAll(message);
	}

	@Override
	public void assertTrue(boolean condition, String message) {
		// TODO Auto-generated method stub
		if(condition)
			TestReport.GetInstance().pass(message);
		else
			TestReport.GetInstance().fail(message);
		super.assertTrue(condition, message);
	}

	@Override
	public void assertTrue(boolean condition) {
		// TODO Auto-generated method stub
		super.assertTrue(condition);
	}

	@Override
	public void assertFalse(boolean condition, String message) {
		// TODO Auto-generated method stub
		if(!condition)
			TestReport.GetInstance().pass(message);
		else
			TestReport.GetInstance().fail(message);
		super.assertFalse(condition, message);
	}

	@Override
	public void assertFalse(boolean condition) {
		// TODO Auto-generated method stub
		super.assertFalse(condition);
	}

	@Override
	public void assertEquals(String actual, String expected, String message) {
		// TODO Auto-generated method stub
		if(actual.equalsIgnoreCase(expected))
			TestReport.GetInstance().pass(message+"["+actual+"]");
		else
			TestReport.GetInstance().fail(message+":- Failed due to Actual ["+actual+"] but expected ["+expected+"]");
		super.assertEquals(actual, expected, message);
	}

	@Override
	public void assertEquals(String actual, String expected) {
		// TODO Auto-generated method stub
		if(actual.equalsIgnoreCase(expected))
			TestReport.GetInstance().pass("Actual ["+actual+"] And expected ["+expected+"]");
		else
			TestReport.GetInstance().fail("Failed due to Actual ["+actual+"] but expected ["+expected+"]");
		
		super.assertEquals(actual, expected);
	}

	@Override
	public void assertEquals(boolean actual, boolean expected, String message) {
		// TODO Auto-generated method stub
		if(actual==expected)
			TestReport.GetInstance().pass(message+"["+actual+"]");
		else
			TestReport.GetInstance().fail(message+":- Failed due to Actual ["+actual+"] but expected ["+expected+"]");
		super.assertEquals(actual, expected, message);
	}

	public void VerifyContent(String ReferencePattern, String Referencematcher,String message) {
		String Actual = ReferencePattern.replaceAll("\\s|\n|\r|\t", "");
		String Expected = Referencematcher.replaceAll("\\s|\n|\r|\t", "");
		if(Pattern.compile(Actual, Pattern.CASE_INSENSITIVE).matcher(Expected).find())	
		{
			TestReport.GetInstance().pass(message+"["+ReferencePattern+"]"+" value is found in the value ["+Referencematcher+"]");
		}
		else
		{
			TestReport.GetInstance().fail(message+":- Failed to find the Actual value ["+ReferencePattern+"] in the ["+Referencematcher+"]");
			super.assertEquals(ReferencePattern, Referencematcher, message);
		}
	}
	@Override
	public void assertEquals(boolean actual, boolean expected) {
		// TODO Auto-generated method stub
		if(actual==expected)
			TestReport.GetInstance().pass("Actual ["+actual+"] And expected ["+expected+"]");
		else
			TestReport.GetInstance().fail("Failed due to Actual ["+actual+"] but expected ["+expected+"]");
		
		super.assertEquals(actual, expected);
	}
	
	@Override
	public void assertNotNull(Object object) {
		// TODO Auto-generated method stub
		
		super.assertNotNull(object);
	}

	@Override
	public void assertNotNull(Object object, String message) {
		// TODO Auto-generated method stub
		if(object!=null)
			TestReport.GetInstance().pass(message);
		else
			TestReport.GetInstance().fail(message+":- Failed due to Object is null");
		super.assertNotNull(object, message);
	}

	@Override
	public void assertNull(Object object) {
		// TODO Auto-generated method stub
		super.assertNull(object);
	}

	@Override
	public void assertNull(Object object, String message) {
		// TODO Auto-generated method stub
		if(object==null)
			TestReport.GetInstance().pass(message);
		else
			TestReport.GetInstance().fail(message+":- Failed due to Object is not null");
		super.assertNull(object, message);
	}

	@Override
	public void assertNotEquals(String actual, String expected,
			String message) {
		// TODO Auto-generated method stub
		if(!actual.equalsIgnoreCase(expected))
			TestReport.GetInstance().pass(message+"["+actual+"]");
		else
			TestReport.GetInstance().fail(message+":- Failed due to Actual ["+actual+"] but expected ["+expected+"]");
		super.assertNotEquals(actual, expected, message);
	}

	@Override
	public void assertNotEquals(String actual, String expected) {
		// TODO Auto-generated method stub
		if(!actual.equalsIgnoreCase(expected))
			TestReport.GetInstance().pass("Actual ["+actual+"] And expected ["+expected+"]");
		else
			TestReport.GetInstance().fail("Failed due to Actual ["+actual+"] but expected ["+expected+"]");
		super.assertNotEquals(actual, expected);
	}

	@Override
	public void assertNotEquals(boolean actual, boolean expected,
			String message) {
		// TODO Auto-generated method stub
		if(actual!=expected)
			TestReport.GetInstance().pass(message+"["+actual+"]");
		else
			TestReport.GetInstance().fail(message+":- Failed due to Actual ["+actual+"] but expected ["+expected+"]");
		super.assertEquals(actual, expected, message);
		super.assertNotEquals(actual, expected, message);
	}

	@Override
	public void assertNotEquals(boolean actual, boolean expected) {
		// TODO Auto-generated method stub
		if(actual!=expected)
			TestReport.GetInstance().pass("Actual ["+actual+"] And expected ["+expected+"]");
		else
			TestReport.GetInstance().fail("Failed due to Actual ["+actual+"] but expected ["+expected+"]");
		super.assertNotEquals(actual, expected);
	}

	@Override
	public void assertNotEquals(int actual, int expected, String message) {
		// TODO Auto-generated method stub
		super.assertNotEquals(actual, expected, message);
		if(actual!=expected)
			TestReport.GetInstance().pass(message+"["+actual+"]");
		else
			TestReport.GetInstance().fail(message+":- Failed due to Actual ["+actual+"] but expected ["+expected+"]");
		super.assertEquals(actual, expected, message);
	}

	@Override
	public void assertNotEquals(int actual, int expected) {
		// TODO Auto-generated method stub
		if(actual!=expected)
			TestReport.GetInstance().pass("Actual ["+actual+"] And expected ["+expected+"]");
		else
			TestReport.GetInstance().fail("Failed due to Actual ["+actual+"] but expected ["+expected+"]");
		super.assertNotEquals(actual, expected);
	}

	
	

}
