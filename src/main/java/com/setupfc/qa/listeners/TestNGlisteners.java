/**
 * 
 */
package com.setupfc.qa.listeners;

import org.testng.IClassListener;
import org.testng.ITestClass;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestNGMethod;
import org.testng.ITestResult;
import org.testng.asserts.IAssert;
import org.testng.asserts.IAssertLifecycle;

import com.setupfc.qa.Interface.ICustomReport;
import com.setupfc.qa.util.TestReport;
import com.setupfc.qa.util.TestUtil;

/**
 * @author smallika
 *
 */
@SuppressWarnings("unused")
public class TestNGlisteners extends TestUtil implements ITestListener, IClassListener {
	
	private String Author = null;
	private String TestTitle = null;
	private String Category = null;
	private String Suite = "Regression";
	

	@Override
	public void onTestStart(ITestResult result) {
		
		// TODO Auto-generated method stub
		ITestNGMethod m1 = result.getMethod();
		System.out.println("test");
		
		if (m1.getConstructorOrMethod().getMethod().isAnnotationPresent(ICustomReport.class)) {
			
			// initialize metadata information.
			this.Author = m1.getConstructorOrMethod().getMethod().getAnnotation(ICustomReport.class).Author();
			TestUtil.TestCaseName = this.TestTitle = m1.getConstructorOrMethod().getMethod().getName();
			
			if (!m1.getConstructorOrMethod().getMethod().getAnnotation(ICustomReport.class).Suite().trim().isEmpty())
				this.Suite = m1.getConstructorOrMethod().getMethod().getAnnotation(ICustomReport.class).Suite();
			this.Category = m1.getConstructorOrMethod().getMethod().getAnnotation(ICustomReport.class).Category();
		}
		
		TestReport.Endtest();
		TestReport.RemoveInstance();
		TestReport.GetInstance(Suite, TestTitle.split("__")[0], TestTitle, Category, Author);
	}

	@Override
	public void onTestSuccess(ITestResult result) {
		
		// TODO Auto-generated method stub
		TestReport.GetInstance().pass("All the Steps from scripts are passed successfully!");
		TestReport.RemoveInstance();

	}

	@Override
	public void onTestFailure(ITestResult result) {
		
		// TODO Auto-generated method stub
		AddScreenshortInReport(result.getThrowable());
		TestReport.Endtest();
		TestReport.RemoveInstance();
	}

	@Override
	public void onTestSkipped(ITestResult result) {
		
		// TODO Auto-generated method stub
		TestReport.GetInstance().skip("Test scripts is skipped due to:-" + result.getThrowable().getMessage());
		TestReport.Endtest();
		TestReport.RemoveInstance();
	}

	@Override
	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
		
		// TODO Auto-generated method stub
	}

	@Override
	public void onStart(ITestContext context) {
		
		// TODO Auto-generated method stub
	}

	@Override
	public void onFinish(ITestContext context) {
		
		// TODO Auto-generated method stub
	}

	@Override
	public void onBeforeClass(ITestClass testClass) {
		
		// TODO Auto-generated method stub
	}

	@Override
	public void onAfterClass(ITestClass testClass) {
		
		// TODO Auto-generated method stub
		TestReport.Endtest();
		TestReport.RemoveInstance();
		TestReport.RemoveTestInstance();
	}

}
