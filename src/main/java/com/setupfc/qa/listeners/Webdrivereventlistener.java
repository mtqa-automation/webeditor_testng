/**
 * 
 */
package com.setupfc.qa.listeners;


import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.events.WebDriverEventListener;

import com.setupfc.qa.util.TestReport;

//import com.setupfc.qa.util.TestUtil;


/**
 * @author smallika
 *
 */

public class Webdrivereventlistener implements WebDriverEventListener {
	
	private static Logger logger = Logger.getLogger(Webdrivereventlistener.class);
	
	@Override
	public void beforeAlertAccept(WebDriver driver) {
		logger.info(">>Entered into Alert Accept");
		
	}

	@Override
	public void afterAlertAccept(WebDriver driver) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void afterAlertDismiss(WebDriver driver) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void beforeAlertDismiss(WebDriver driver) {
		
		logger.info(">>Entered into Alert Dismiss");
	}

	@Override
	public void beforeNavigateTo(String url, WebDriver driver) {
		logger.info(">>Navigated to URL:--"+url);
		
	}

	@Override
	public void afterNavigateTo(String url, WebDriver driver) {
		// TODO Auto-generated method stub
		//TestReport.GetInstance().info(url+ " : URL is luanched successfully in the browser");
	}

	@Override
	public void beforeNavigateBack(WebDriver driver) {
		logger.info(">>Entering into Navigating back");
		
	}

	@Override
	public void afterNavigateBack(WebDriver driver) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void beforeNavigateForward(WebDriver driver) {
		logger.info(">>Entering into Navigating Forward");
		
	}

	@Override
	public void afterNavigateForward(WebDriver driver) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void beforeNavigateRefresh(WebDriver driver) {
		logger.info(">>Entering into Navigating Refresh");
		
	}

	@Override
	public void afterNavigateRefresh(WebDriver driver) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void beforeFindBy(By by, WebElement element, WebDriver driver) {
		logger.debug(">>Entered into Finding the WebElement:-" + by.toString());
		
	}

	@Override
	public void afterFindBy(By by, WebElement element, WebDriver driver) {
		logger.debug("WebElement Created for " + by.toString());
	}

	@Override
	public void beforeClickOn(WebElement element, WebDriver driver) {
		logger.info(">>Entered into Click on the Element:--" + element.toString());	
	}

	@Override
	public void afterClickOn(WebElement element, WebDriver driver) {
		TestReport.GetInstance().info("Clicked on the webelement>> " + element.toString().split("->",2)[1]);
	}

	@Override
	public void beforeChangeValueOf(WebElement element, WebDriver driver,
			CharSequence[] keysToSend) {
		String temp="";
		if(keysToSend!=null)
		for(CharSequence Char:keysToSend)
			temp+=Char;
		logger.info(">>Entered into Send Key to the Element:--"+temp);
			
	}

	@Override
	public void afterChangeValueOf(WebElement element, WebDriver driver,
			CharSequence[] keysToSend) {
		
		String temp="";
		if(keysToSend!=null)
		for(CharSequence Char:keysToSend)
			temp+=Char;
		logger.info(temp+":-- value is successfully added to text field:--");
		TestReport.GetInstance().info(" text is added to the webelement>>"+element.toString().split("->",2)[1]);
	
	}

	@Override
	public void beforeScript(String script, WebDriver driver) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void afterScript(String script, WebDriver driver) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void beforeSwitchToWindow(String windowName, WebDriver driver) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void afterSwitchToWindow(String windowName, WebDriver driver) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onException(Throwable throwable, WebDriver driver) {
		// TODO Auto-generated method stub
		
		logger.error("error in Programm::" + throwable);
		TestReport.GetInstance().error("Error message is throwing due to: " + throwable);
	}

	@Override
	public <X> void beforeGetScreenshotAs(OutputType<X> target) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public <X> void afterGetScreenshotAs(OutputType<X> target, X screenshot) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void beforeGetText(WebElement element, WebDriver driver) {
		// TODO Auto-generated method stub
		logger.info("Getting the text from the element:--" + element.toString());
	}

	@Override
	public void afterGetText(WebElement element, WebDriver driver,
			String text) {
		logger.info(text+" >> displaying text for element:--" + element.toString());
		
	}
	

}
