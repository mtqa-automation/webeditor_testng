/**
 * 
 */
package com.setupfc.qa.Interface;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Retention;

/*
import org.testng.annotations.IAnnotation;
import org.testng.internal.annotations.IAnnotationFinder;
import org.testng.internal.annotations.IListeners;
*/

/**
 * @author smallika
 *
 */
@Retention(RUNTIME)
public @interface ICustomReport {
	
	String Author() default "Admin";
	String Suite() default "";
	String TestTitle() default "";
	String Category() default "";
	
}
