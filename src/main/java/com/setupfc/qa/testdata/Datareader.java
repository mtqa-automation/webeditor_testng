/**
 * 
 */
package com.setupfc.qa.testdata;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import java.util.List;
import java.util.Map;

import org.testng.ITestNGMethod;
import org.testng.ITestResult;
import org.testng.annotations.DataProvider;


import com.setupfc.qa.config.IniFile;
import com.setupfc.qa.util.TestUtil;

import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;

/**
 * @author smallika
 *
 */
public class Datareader {

	private static Workbook workbook;
	private static String Env = TestUtil.Environment;
	private static String TestDataExcelFile = TestUtil.TestDataFilePath+"//"+TestUtil.TestDataFileName;
			
	private static List<Map<String, String>>  readExceldata(String TestCaseID) {
		List<Map<String, String>> listofdata = new ArrayList<Map<String, String>>();
		try {
			
			Sheet testdataSheet = getExcelSheet(TestDataExcelFile,
			IniFile.GetInstance().ReadIniFile(Env, "TestDataSheet"));
			int temp1;
			Cell[] UniqueIDColumn = testdataSheet.getColumn(0);
			for (Cell cell : UniqueIDColumn)
			{
				Map<String, String> Testdata = new HashMap<String, String>();
				if (cell.getContents().trim().equalsIgnoreCase(TestCaseID)) {
					temp1 = cell.getRow();
					Cell[] HeaderRow = testdataSheet.getRow(0);
					Cell[] TestdataRow = testdataSheet.getRow(temp1);
					int t1 = 0;
					for (Cell cell1 : TestdataRow) {
						if (cell1.getContents().startsWith("Environment"))
							Testdata.put(HeaderRow[t1].getContents().trim(),
									IniFile.GetInstance().ReadIniFile(Env, cell1.getContents().trim()));
						else
							Testdata.put(HeaderRow[t1].getContents().trim(), cell1.getContents().trim());
						t1++;
					}
					if(Testdata.get(TestUtil.TestdataRunFlag).equalsIgnoreCase("YES")||Testdata.get(TestUtil.TestdataRunFlag).equalsIgnoreCase("Y"))
					listofdata.add(Testdata);
				}
				
		}
			
		} catch (Exception e) {
			
		}
		finally
		{
			if(workbook!=null)
				workbook.close();
		}
		return listofdata;
	}

	private static Sheet getExcelSheet(String FileName, String SheetName) {
		try {
			workbook = Workbook.getWorkbook(new File(FileName));
			Sheet sheet = workbook.getSheet(SheetName);
			return sheet;
		} catch (Exception e) {
			
			System.out.println();
			return null;
		}
	}

	public static List<Map<String, String>> Readtestdata(String TestCaseID) {
		return readExceldata(TestCaseID);
	}

	@DataProvider(name="data")
	 public Object[][] dataSupplier(ITestNGMethod  result){
		String TestCaseID=result.getMethodName().split("__")[0];
		List<Map<String, String>> ListofData = Readtestdata(TestCaseID);
		Object[][] Obj= new Object[ListofData.size()][1];
		for(int i=0;i<ListofData.size();i++)
			Obj[i][0]=	ListofData.get(i);
		return Obj;
		
	}
	
}
