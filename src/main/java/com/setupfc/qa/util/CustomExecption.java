package com.setupfc.qa.util;

import org.apache.log4j.Logger;

@SuppressWarnings("serial")
public class CustomExecption extends RuntimeException {
	private static Logger logger = Logger.getLogger(CustomExecption.class.getName());
	public CustomExecption(String message)
	{
		super(message);
		logger.error(message);
	}

	public CustomExecption(String message,Throwable e)
	{
		super(message,e);
		logger.error(message+ ": "+e);
		//TestReport.GetInstance().fail(message+" due to:--"+e.getMessage());
	}
	
}
