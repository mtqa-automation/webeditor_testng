package com.setupfc.qa.util;

import org.apache.log4j.Logger;
import org.apache.poi.openxml4j.exceptions.InvalidOperationException;

import com.aventstack.extentreports.AnalysisStrategy;
import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.MediaEntityModelProvider;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.Protocol;
import com.aventstack.extentreports.reporter.configuration.Theme;
import com.setupfc.qa.config.IniFile;

public class TestReport {

	private static ExtentTest Test = null;
	private static ExtentTest Node = null;
	private static ExtentReports _instance = null;
	private static ExtentHtmlReporter htmlreporter = null;
	public static IniFile ini = IniFile.GetInstance();
	static String Env = TestUtil.Environment;
	private static Logger logger = Logger.getLogger(TestReport.class);

	public static ExtentReports Instance() {

		if (_instance == null) {

			htmlreporter = new ExtentHtmlReporter("./"+TestUtil.TestReportPath+"/"+TestUtil.TestReportName);
			htmlreporter.config().setCSS("css-string");
			htmlreporter.config().setDocumentTitle(TestUtil.TestReportTitle);
			htmlreporter.config().setEncoding("utf-8");
			htmlreporter.config().setProtocol(Protocol.HTTPS);
			htmlreporter.config().setReportName("Automation Report");
			htmlreporter.config().setTheme(Theme.DARK);
			htmlreporter.config().setTimeStampFormat("MMM dd, yyyy HH:mm:ss a");
			htmlreporter.config().enableTimeline(true);
			htmlreporter.config().setJS("js-string");
			htmlreporter.config().setAutoCreateRelativePathMedia(true);

			_instance = new ExtentReports();
			String nameOS = "os.name";
			String architectureOS = "os.arch";
			String UserName = "user.name";

			nameOS = System.getProperty(nameOS);
			architectureOS = System.getProperty(architectureOS);
			UserName = System.getProperty(UserName);

			_instance.setSystemInfo("OS : ", nameOS);
			_instance.setSystemInfo("OS Architecture : ", architectureOS);
			_instance.setSystemInfo("User Name : ", UserName);
			_instance.setSystemInfo("Browser",
					ini.ReadIniFile(Env, "EnvironmentBrowser"));
			_instance.attachReporter(htmlreporter);
			_instance.setAnalysisStrategy(AnalysisStrategy.SUITE);
			return _instance;
		}
		return _instance;
	}

	/**
	 * @param SuiteName
	 * @param TestName
	 * @param TestDescri
	 * @param Category
	 * @param Author
	 * @return ExtentTest
	 */
	public static ExtentTest GetInstance(String SuiteName, String TestName,
			String TestDesc, String Category, String Author) {
		try {
			String[] ArrayOfCat = null;
			if (Category != null && !Category.isEmpty()) {
				ArrayOfCat = Category.split(",");
			}
			if (Test == null) {
				Test = Instance().createTest(SuiteName);
			}
			if (Node == null) {
				Node = Test.createNode(TestName, TestDesc);
				if (ArrayOfCat != null)
					Node.assignCategory(ArrayOfCat);
				if (Author != null && !Author.isEmpty()
						&& !Author.equalsIgnoreCase("empty"))
					Node.assignAuthor(Author);
				return Node;
			}
		} catch (Exception e) {
			logger.error("Error occured while loading the file: " + e);
		}
		return Node;
	}

	/**
	 * @return ExtentTest
	 */
	public static ExtentTest GetInstance() {
		if (Node == null) {
			throw new InvalidOperationException(
					"Singleton not created - use GetInstance(arg1, arg2,arg3, arg4)");
		}
		return Node;
	}

	public static void RemoveInstance() {
		if (Node != null)
			Node = null;
	}

	public static void RemoveTestInstance() {
		if (Test != null)
			Test = null;
	}

	public static void Endtest() {
		// TestReport.Instance().removeTest(TestReport.GetInstance());;
		Instance().flush();
	}

	/**
	 * @param Screenshotpath
	 * @return MediaEntityModelProvider
	 */
	public static MediaEntityModelProvider AddScreenshot(
			String Screenshotpath) {

		MediaEntityModelProvider _scrnshot = null;
		try {
			_scrnshot = MediaEntityBuilder
					.createScreenCaptureFromPath(Screenshotpath).build();
		} catch (Exception e) {
			logger.error(
					"Error occured while Adding the screenshot file: " + e);
		}
		return _scrnshot;

	}

}
