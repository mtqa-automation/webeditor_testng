/**
 * 
 */
package com.setupfc.qa.util;

import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.setupfc.qa.base.TestBase;
import com.setupfc.qa.config.IniFile;
import com.setupfc.qa.listeners.Webdrivereventlistener;

/**
 * @author smallika
 *
 */
@SuppressWarnings("deprecation")
public class TestUtil extends TestBase {

	private static Logger logger = Logger.getLogger(Webdrivereventlistener.class);

	// ***** Global Variables *****

	public static String Environment = IniFile.GetInstance().ReadIniFile("START_GLOBAL", "Environment");
	private static String ScreenShotFolderPath = IniFile.GetInstance().ReadIniFile("START_GLOBAL",
			"ScreenShotFolderPath");
	public static String TestReportPath = "TestReport";
	public static String TestReportName = "Report.html";
	public static String TestDataFilePath = IniFile.GetInstance().ReadIniFile("START_GLOBAL", "TestDataFilePath");
	public static String TestDataFileName = IniFile.GetInstance().ReadIniFile("START_GLOBAL", "TestDataFileName");

	public static String TestCaseName;
	public static String TestReportTitle = "Setup Automation Report";
	public static String TestdataRunFlag = "Active";

	public static long PAGELAYOT_TIMEOUT = 20;
	public static long IMPLICT_TIMEOUT = 5;
	public static final int AjaxTimeout = 20;
	public static String MinimumWait = "3000";
	public static String ExtMinimumWait = "1000";
	public static String MediumWait = "5000";
	public static String MaximumWait = "10000";

	public static String ParentWindow = null;

	
	// ***** Actions *****
	
	protected boolean waitForPageLoad() {
		
		boolean browserLoaded = false;
		
		try {
			long timeOut = 5000;
			long end = System.currentTimeMillis() + timeOut;
			
			while (System.currentTimeMillis() < end) {
				
				if(String.valueOf(((JavascriptExecutor) driver).executeScript("return document.readyState"))
						.equals("Ready")) {
					browserLoaded = true;
					break;
				}
			}
			
		} catch (Exception e) {
			logger.error("Error occured while waiting for element visble" + e);
			throw new CustomExecption(e.getMessage());
		}
		return browserLoaded;
	}
	
	
	protected int numberOfWebElements(WebElement element) {
		
		String extractedXpath = element.toString();
		String actualXpath = (extractedXpath.substring(0, extractedXpath.length() - 1).split("-> ")[1]).replace("xpath: ", "");
		List<WebElement> elements = driver.findElements(By.xpath(actualXpath));
		System.out.println(elements.size());
		return elements.size();
	
	}

	protected void waitForVisibility(WebElement element) {

		try {
			WebDriverWait wait = new WebDriverWait(driver, TestUtil.PAGELAYOT_TIMEOUT);
			wait.until(ExpectedConditions.visibilityOf(element));

		} catch (Exception e) {
			logger.error("Error occured while waiting for element visble" + e);
			throw new CustomExecption(e.getMessage());
		}
	}

	protected void WaitforElementVisibleAndClickable(WebElement element) {

		try {
			WebDriverWait wait = new WebDriverWait(driver, TestUtil.PAGELAYOT_TIMEOUT);
			wait.until(ExpectedConditions.visibilityOf(element));
			wait.until(ExpectedConditions.elementToBeClickable(element));
		} catch (Exception e) {
			logger.error("Error occured while waiting for element visble" + e);
			throw new CustomExecption(e.getMessage());
		}
	}

	protected void WaitforElementInVisible(WebElement element) {

		try {
			WebDriverWait wait = new WebDriverWait(driver, TestUtil.PAGELAYOT_TIMEOUT);
			wait.until(ExpectedConditions.invisibilityOf(element));
		} catch (Exception e) {
			logger.error("Error occured while waiting for element visble" + e);
			throw new CustomExecption(e.getMessage());
		}
	}

	protected void WaitforElementInVisible(By byLocator) {

		try {
			WebDriverWait wait = new WebDriverWait(driver, TestUtil.PAGELAYOT_TIMEOUT);
			wait.until(ExpectedConditions.invisibilityOfElementLocated(byLocator));
		} catch (Exception e) {
			logger.error("Error occured while waiting for element to not visble" + e);
			throw new CustomExecption(e.getMessage());
		}
	}

	protected void ScrollToViewElement(WebElement element) throws Error {

		((JavascriptExecutor) driver).
			executeScript("arguments[0].scrollIntoView({block: 'center', inline: 'nearest'})", element);
		WaitInSeconds("2");
	}

	protected void ScrollToBottom() throws Error {

		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,250)", "");
	}

	protected String CaptureScreenShot(String TestCaseID) {

		String _PathofScrnshot = "";

		try {
			TakesScreenshot scrShot = ((TakesScreenshot) driver);
			File SrcFile = scrShot.getScreenshotAs(OutputType.FILE);
			DateFormat df = new SimpleDateFormat("yyyyMMdd");
			DateFormat dft = new SimpleDateFormat("yyyyMMdd_hhmmss");
			String date = df.format(new Date());
			String datetime = dft.format(new Date());
			_PathofScrnshot = TestUtil.ScreenShotFolderPath + "\\" + date + "\\" + datetime + "_" + TestCaseID + ".png";
			File DestFile = new File(_PathofScrnshot);
			FileUtils.copyFile(SrcFile, DestFile);
			return System.getProperty("user.dir") + "\\" + _PathofScrnshot;
		} catch (Exception e) {

			logger.error("error occured while capturing the Screenshot:-" + e);
			throw new CustomExecption(e.getMessage());
		}
	}

	protected void AddScreenshortInReport(Throwable error) {

		try {
			String errorDetails = error.getMessage();
			if (errorDetails == null)
				errorDetails = "";
			logger.error(errorDetails);
			String ScrnshtPath = CaptureScreenShot(TestUtil.TestCaseName.split("__")[0]);
			String scrnshot = "<img class='r-img' onerror='this.style.display=\"none\"" + "' data-featherlight='"
					+ ScrnshtPath + "' src='" + ScrnshtPath + "' data-src='" + ScrnshtPath + "'>";

			String msg = "<details>" + "<summary>" + "<b>" + "<font color =" + "red>" + "Error Occured: Click to view"
					+ "</font>" + "</b>" + "</summary>" + "<b style=\"background-color:powderblue;color:black;\">"
					+ errorDetails.replaceAll(",", "<br>") + "</b>" + "<br>" + scrnshot + "</details>" + "\n";
			TestReport.GetInstance().fail(error);
			TestReport.GetInstance().fail(msg);
			// AfterSuite();

		} catch (Exception e) {
			logger.error("error occured while Adding the Screenshot to report:-" + e);
			throw new CustomExecption(e.getMessage());
		}
	}

	protected void SwitchtoNewWindow() {

		try {
			logger.debug(" Entered into SwitchtoNewWindow method");
			for (String windows : GetActiveWindows()) {
				driver.switchTo().window(windows);
				if (!ParentWindow.equalsIgnoreCase(windows)) {
					TestReport.GetInstance()
							.pass("Switched to child window [" + driver.getTitle() + "] and window ID " + windows);
					logger.info("Switched to child window [" + driver.getTitle() + "] and window ID >>" + windows);
				}
			}
			logger.debug("  Left from SwitchtoNewWindow method");
		} catch (Exception e) {
			throw new CustomExecption(e.getMessage());
		}
	}

	private String[] GetActiveWindows() {

		try {
			logger.debug(" Entered into GetActiveWindows method");
			int temp = 0;
			Set<String> windows = driver.getWindowHandles();
			GetParentWindow();
			String[] Wndws = new String[windows.size()];
			Iterator<String> Window_Iterator = windows.iterator();
			while (Window_Iterator.hasNext()) {
				Wndws[temp] = Window_Iterator.next();
				temp++;
			}
			logger.debug("  Left from GetActiveWindows method");
			return Wndws;
		} catch (Exception e) {
			throw new CustomExecption(e.getMessage());
		}
	}

	/**
	 * @author smallika void
	 * 
	 */
	protected String GetParentWindow() {
		// TODO Auto-generated method stub
		return driver.getWindowHandle();
	}

	protected void SwitchtoParentWindow() {

		try {
			logger.debug("  Entered into SwitchParentWindow method");
			driver.switchTo().window(ParentWindow);
			TestReport.GetInstance().pass(
					"Switched to parent window: [" + ParentWindow + "] and Window Title:[ " + driver.getTitle() + "]");
			logger.info(
					"Switched to parent window: [" + ParentWindow + "] and Window Title:[ " + driver.getTitle() + "]");
		} catch (Exception e) {
			throw new CustomExecption(e.getMessage());
		}
		logger.debug("  Left from SwitchParentWindow method");
	}

	protected void SwitchToWindow(String WindowID) {

		try {
			logger.debug("  Entered into Swith window method");
			driver.switchTo().window(WindowID);
			TestReport.GetInstance()
					.pass("Switched to window: [" + WindowID + "] and Window Title:[ " + driver.getTitle() + "]");
			logger.info("Switched to window: [" + WindowID + "] and Window Title:[ " + driver.getTitle() + "]");
		} catch (Exception e) {
			throw new CustomExecption(e.getMessage());
		}
	}

	protected boolean WaitInSeconds(String WaitSeconds) {

		boolean _status = false;

		try {
			logger.debug("  Entered into MaxWait method");
			int Min = Integer.parseInt(WaitSeconds);
			logger.info("Wait in Seconds: " + Min / 1000);
			Thread.sleep(Min);
			_status = true;
			logger.debug("  Left from MaxWait method");
		} catch (Exception e) {
			throw new CustomExecption("error occured while waiting: " + e);
		}
		return _status;
	}

	protected boolean MinWait() {
		return WaitInSeconds(MinimumWait);
	}

	protected boolean ExtMinWait() {
		return WaitInSeconds(ExtMinimumWait);
	}

	protected boolean MediumWait() {
		return WaitInSeconds(MediumWait);

	}

	protected boolean MaxWait() {
		return WaitInSeconds(MaximumWait);
	}

	protected void ClearByAction() {

		Actions action = new Actions(driver);
		action.keyDown(Keys.CONTROL).sendKeys("a").keyUp(Keys.CONTROL).sendKeys(Keys.BACK_SPACE).build().perform();

	}
	
	protected Alert switchToAlert() {
		return driver.switchTo().alert();
	}

}
