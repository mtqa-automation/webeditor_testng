/**
 * 
 */
package com.setupfc.qa.testcases;

import java.util.HashMap;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.setupfc.qa.Interface.ICustomReport;
import com.setupfc.qa.base.TestBase;
import com.setupfc.qa.config.IniFile;
import com.setupfc.qa.listeners.CustomAssert;
import com.setupfc.qa.listeners.DataproviderListener;
import com.setupfc.qa.listeners.TestNGlisteners;
import com.setupfc.qa.pages.SetupFC.HomePage;
import com.setupfc.qa.pages.SetupFC.LoginPage;
import com.setupfc.qa.testdata.Datareader;

/**
 * @author smallika
 *
 */
@Listeners({ TestNGlisteners.class, DataproviderListener.class })
public class LoginTest extends TestBase {
	
	LoginPage loginpage;
	HomePage homepage;
	CustomAssert Assert = new CustomAssert();
	IniFile ini = IniFile.GetInstance();

	@BeforeMethod
	public void setup() {
		
		initilization();
		loginpage = new LoginPage();
	}

	@AfterMethod
	public void TearDown() {
		CloseBrowser();
	}


	@Test(dataProvider = "data", dataProviderClass = Datareader.class, priority = 1)
	@ICustomReport(Author = "Siddharth", Suite = "Smoke")
	public void SFC_Login001__LoginWithValidCred(HashMap<String, String> testdata) {
		
		homepage = loginpage.Login(testdata.get("UserName1"), testdata.get("Password1"));
		Assert.assertTrue(homepage.VerifyWelcomeMsg());
		System.out.println(homepage.GetLoggedUserID());
	}
	
	
	@Test(priority = 2)
	@ICustomReport(Author = "Siddharth", Suite = "Smoke")
	public void SFC_Login002__LoginBySingleSignOn() {
		
		homepage = loginpage.LoginbySingleSignOn();
		Assert.assertTrue(homepage.VerifyWelcomeMsg());
		System.out.println(homepage.GetLoggedUserID());
	}

	
	@Test(priority = 3)
	@ICustomReport(Author = "Rashmi", Suite = "Smoke")
	public void SFC_Login003__LoginWithInvalidCred() {
		
		homepage = loginpage.Login("Invaliduser", "Invalidpassword");
		Assert.assertTrue(homepage.VerifyLoginFailMsg());

	}

}
