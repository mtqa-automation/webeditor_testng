/**
 * 
 */
package com.setupfc.qa.testcases;

import java.util.HashMap;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.setupfc.qa.EditorCommon.EditorMainPage;
import com.setupfc.qa.EditorCommon.SubmitWindow;
import com.setupfc.qa.Interface.ICustomReport;
import com.setupfc.qa.base.TestBase;
import com.setupfc.qa.config.IniFile;
import com.setupfc.qa.listeners.CustomAssert;
import com.setupfc.qa.listeners.DataproviderListener;
import com.setupfc.qa.listeners.TestNGlisteners;

import com.setupfc.qa.pages.SetupFC.HomePage;
import com.setupfc.qa.pages.SetupFC.LoginPage;
import com.setupfc.qa.pages.SetupFC.NewSpecPage;
import com.setupfc.qa.pages.SetupFC.SearchSpecPage;
import com.setupfc.qa.pages.SetupFC.SpecificationPage;
import com.setupfc.qa.pages.SetupFC.TaskActionsPage;
import com.setupfc.qa.pages.SetupFC.TaskPage;
import com.setupfc.qa.pages.SetupFC.WebEditorPage;
import com.setupfc.qa.testdata.Datareader;
import com.setupfc.qa.util.TestUtil;

/**
 * @author smallika
 * 
 * @ModifiedBy rhalli
 *
 */
@Listeners({ TestNGlisteners.class, DataproviderListener.class })
public class SpecificationTest extends TestBase {

	LoginPage loginpage;
	HomePage homepage;
	SpecificationPage specpage;
	NewSpecPage newspecpage;
	SearchSpecPage searchspecpage;
	TaskPage taskpage;
	EditorMainPage editormainpage;
	SubmitWindow submitwindow;
	TaskActionsPage taskactionspage;
	CustomAssert Assert = new CustomAssert();
	IniFile ini = IniFile.GetInstance();

	@BeforeMethod
	public void setup() {
		
		initilization();
		loginpage = new LoginPage();
	}

	@AfterMethod
	public void TearDown() {
		CloseBrowser();
	}

	
	@Test(dataProvider = "data", dataProviderClass = Datareader.class, priority = 1)
	@ICustomReport(Author = "Rashmi", Suite = "Smoke")
	public void SFC_SP001__CreateNewSpecification(HashMap<String, String> testdata) {
		
		homepage = loginpage.Login(testdata.get("UserName1"), testdata.get("Password1"));
		specpage = homepage.NavigateToSpecificationPage();
		Assert.assertNotNull(specpage, "Navigate to the specification page");
		
		newspecpage = specpage.NavigateToNewSpecification();
		Assert.assertNotNull(newspecpage, "Navigated to new specification page");
		
		Assert.assertTrue(newspecpage.AddGeneralSettings(testdata.get("Title"), testdata.get("Class"), testdata.get("SubClass"), testdata.get("Department"))
				, "General settings data is added");
		Assert.assertTrue(newspecpage.Next(), "Clicked on Next button");
		Assert.assertTrue(newspecpage.SelectWorkFlow(testdata.get("WorkflowID")), "Selected workflow ID: " + testdata.get("WorkflowID"));
		Assert.assertTrue(newspecpage.Next(), "Clicked on Next button");
		Assert.assertTrue(newspecpage.CreateSpec(), "Clicked on Create button");
		
		Assert.assertNotNull(newspecpage.VerifySuccessMsg(), "Verified the message: " + newspecpage.VerifySuccessMsg());
	
	}
	
	
	@Test(dataProvider = "data", dataProviderClass = Datareader.class, priority = 2)
	@ICustomReport(Author = "Siddharth", Suite = "Smoke")
	public void SFC_SP002__CreateContentEditSpecification(HashMap<String, String> testdata) {
		
		homepage = loginpage.Login(testdata.get("UserName1"), testdata.get("Password1"));
		specpage = homepage.NavigateToSpecificationPage();
		Assert.assertNotNull(specpage, "Navigate to the specification page");
		
		// Create a new specification
		newspecpage = specpage.NavigateToNewSpecification();
		Assert.assertNotNull(newspecpage, "Navigated to new specification page");
		
		Assert.assertTrue(newspecpage.AddGeneralSettings(testdata.get("Title"), testdata.get("Class"), testdata.get("SubClass"), testdata.get("Department"))
				, "General settings data is added");
		Assert.assertTrue(newspecpage.Next(), "Clicked on Next button");
		Assert.assertTrue(newspecpage.SelectWorkFlow(testdata.get("WorkflowID")), "Selected workflow ID: " + testdata.get("WorkflowID"));
		Assert.assertTrue(newspecpage.Next(), "Clicked on Next button");
		Assert.assertTrue(newspecpage.CreateSpec(), "Clicked on Create button");
		
		Assert.assertNotNull(newspecpage.VerifySuccessMsg(), "Verified the message: " + newspecpage.VerifySuccessMsg());
		
		Assert.assertNotNull(newspecpage.GetCreateSpecID(), "Specification is created: " + newspecpage.GetCreateSpecID());		
		ini.WriteIniFile(TestUtil.Environment, "EnvironmentSpecID", newspecpage.GetCreateSpecID());
		
		// Lock for edit and create a Content Change task
		Assert.assertTrue(newspecpage.LockForEdit(testdata.get("Parameter3")), "Locked for Spec Edit: " + testdata.get("Parameter3"));
		
		// Select the created task 
		taskpage = homepage.NavigateToTaskPage();
		Assert.assertNotNull(taskpage, "");
		Assert.assertTrue(taskpage.SearchBySpecID(ini.ReadIniFile(TestUtil.Environment, "EnvironmentSpecID")), "Search for the spec ID");
		Assert.assertTrue(taskpage.SelectTask(ini.ReadIniFile(TestUtil.Environment, "EnvironmentSpecID")), "Task is selected");
		Assert.assertTrue(taskpage.verifyTaskTitleInTaskDetails(testdata.get("Parameter4")), "Task Title is Edit Content Specification");
		
		// Confirm Edit SPecification action
		editormainpage = taskpage.confirmEditSpecificationAction();
		Assert.assertNotNull(editormainpage);
		
	}

	@Test(dataProvider = "data", dataProviderClass = Datareader.class, priority = 3)
	@ICustomReport(Author = "Siddharth", Suite = "Smoke")
	public void SFC_SP003__CreatePropertyChangeSpecification(HashMap<String, String> testdata) {
		
		homepage = loginpage.Login(testdata.get("UserName1"), testdata.get("Password1"));
		specpage = homepage.NavigateToSpecificationPage();
		Assert.assertNotNull(specpage, "Navigate to the specification page");
		
		// Create a new specification
		newspecpage = specpage.NavigateToNewSpecification();
		Assert.assertNotNull(newspecpage, "Navigated to new specification page");
		
		Assert.assertTrue(newspecpage.AddGeneralSettings(testdata.get("Title"), testdata.get("Class"), testdata.get("SubClass"), testdata.get("Department"))
				, "General settings data is added");
		Assert.assertTrue(newspecpage.Next(), "Clicked on Next button");
		Assert.assertTrue(newspecpage.SelectWorkFlow(testdata.get("WorkflowID")), "Selected workflow ID: " + testdata.get("WorkflowID"));
		Assert.assertTrue(newspecpage.Next(), "Clicked on Next button");
		Assert.assertTrue(newspecpage.CreateSpec(), "Clicked on Create button");
		
		Assert.assertNotNull(newspecpage.VerifySuccessMsg(), "Verified the message: " + newspecpage.VerifySuccessMsg());
		
		Assert.assertNotNull(newspecpage.GetCreateSpecID(), "Specification is created:-" + newspecpage.GetCreateSpecID());
		ini.WriteIniFile(TestUtil.Environment, "EnvironmentSpecID", newspecpage.GetCreateSpecID());
		
		// Lock for Edit and create a Property Change task
		Assert.assertTrue(newspecpage.LockForEdit(testdata.get("Parameter3")), "Locked for Spec Edit: " + testdata.get("Parameter3"));
		
		// Select the created task
		taskpage = homepage.NavigateToTaskPage();
		Assert.assertNotNull(taskpage, "");
		Assert.assertTrue(taskpage.SearchBySpecID(ini.ReadIniFile(TestUtil.Environment, "EnvironmentSpecID")), "Search for the spec ID");
		Assert.assertTrue(taskpage.SelectTask(ini.ReadIniFile(TestUtil.Environment, "EnvironmentSpecID")), "Task is selected");
		Assert.assertTrue(taskpage.verifyTaskTitleInTaskDetails(testdata.get("Parameter4")), "Task Title is Edit Specification Metadata");

	}

	@Test(dataProvider = "data", dataProviderClass = Datareader.class, priority = 4)
	@ICustomReport(Author = "Siddharth", Suite = "Smoke")
	public void SFC_SP004__CreateTerminationSpecification(HashMap<String, String> testdata) {
		
		homepage = loginpage.Login(testdata.get("UserName1"), testdata.get("Password1"));
		specpage = homepage.NavigateToSpecificationPage();
		Assert.assertNotNull(specpage, "Navigate to the specification page");
		
		// Create a new specification
		newspecpage = specpage.NavigateToNewSpecification();
		Assert.assertNotNull(newspecpage, "Navigated to new specification page");
		
		Assert.assertTrue(newspecpage.AddGeneralSettings(testdata.get("Title"), testdata.get("Class"), testdata.get("SubClass"), testdata.get("Department"))
				, "General settings data is added");
		Assert.assertTrue(newspecpage.Next(), "Clicked on Next button");
		Assert.assertTrue(newspecpage.SelectWorkFlow(testdata.get("WorkflowID")), "Selected workflow ID: " + testdata.get("WorkflowID"));
		Assert.assertTrue(newspecpage.Next(), "Clicked on Next button");
		Assert.assertTrue(newspecpage.CreateSpec(), "Clicked on Create button");
		
		Assert.assertNotNull(newspecpage.VerifySuccessMsg(), "Verified the message: " + newspecpage.VerifySuccessMsg());
		
		Assert.assertNotNull(newspecpage.GetCreateSpecID(), "Specification is created:-" + newspecpage.GetCreateSpecID());
		ini.WriteIniFile(TestUtil.Environment, "EnvironmentSpecID", newspecpage.GetCreateSpecID());
		
		// Lock for Edit and create a Termination task
		Assert.assertTrue(newspecpage.LockForEdit(testdata.get("Parameter3")), "Locked for Spec Edit: " + testdata.get("Parameter3"));
		
		// Select the created task
		taskpage = homepage.NavigateToTaskPage();
		Assert.assertNotNull(taskpage, "");
		Assert.assertTrue(taskpage.SearchBySpecID(ini.ReadIniFile(TestUtil.Environment, "EnvironmentSpecID")), "Search for the spec ID");
		Assert.assertTrue(taskpage.SelectTask(ini.ReadIniFile(TestUtil.Environment, "EnvironmentSpecID")), "Task is selected");
		Assert.assertTrue(taskpage.verifyTaskTitleInTaskDetails(testdata.get("Parameter4")), "Task Title is Terminate Specification");
		
	}
	
	@Test(dataProvider = "data", dataProviderClass = Datareader.class, priority = 5)
	@ICustomReport(Author = "Rashmi", Suite = "Regression")
	public void SFC_SP005__ApproveContentChangeSpecification(HashMap<String, String> testdata) {
		
		homepage = loginpage.Login(testdata.get("UserName1"), testdata.get("Password1"));
		specpage = homepage.NavigateToSpecificationPage();
		Assert.assertNotNull(specpage, "Navigate to the specification page");
		
		// Create a new specification
		newspecpage = specpage.NavigateToNewSpecification();
		Assert.assertNotNull(newspecpage, "Navigated to new specification page");
		
		Assert.assertTrue(newspecpage.AddGeneralSettings(testdata.get("Title"), testdata.get("Class"), testdata.get("SubClass"), testdata.get("Department"))
				, "General settings data is added");
		Assert.assertTrue(newspecpage.Next(), "Clicked on Next button");
		Assert.assertTrue(newspecpage.SelectWorkFlow(testdata.get("WorkflowID")), "Selected workflow ID: " + testdata.get("WorkflowID"));
		Assert.assertTrue(newspecpage.Next(), "Clicked on Next button");
		Assert.assertTrue(newspecpage.CreateSpec(), "Clicked on Create button");
		
		Assert.assertNotNull(newspecpage.VerifySuccessMsg(), "Verified the message: " + newspecpage.VerifySuccessMsg());
		
		Assert.assertNotNull(newspecpage.GetCreateSpecID(), "Specification is created: " + newspecpage.GetCreateSpecID());		
		ini.WriteIniFile(TestUtil.Environment, "EnvironmentSpecID", newspecpage.GetCreateSpecID());
		
		// Lock spec for edit and create a task
		Assert.assertTrue(newspecpage.LockForEdit(testdata.get("Parameter3")), "Locked for Spec Edit: " + testdata.get("Parameter3"));
		
		// Navigate to Tasks section and load the spec in editor
		taskpage = homepage.NavigateToTaskPage();
		Assert.assertNotNull(taskpage, "");
		Assert.assertTrue(taskpage.SearchBySpecID(ini.ReadIniFile(TestUtil.Environment, "EnvironmentSpecID")), "Search for the spec ID");
		Assert.assertTrue(taskpage.SelectTask(ini.ReadIniFile(TestUtil.Environment, "EnvironmentSpecID")), "Task is selected");
		
		editormainpage = taskpage.confirmEditSpecificationAction();
		Assert.assertNotNull(editormainpage);
		
		// Fill mandatory and additional fields
		editormainpage.closeChapterTree();
	    Assert.assertTrue(editormainpage.addChangeReason(testdata.get("Change-Reason")), "Change Reason is entered");
	    Assert.assertTrue(editormainpage.addChangeDescription(testdata.get("Change-Desc")), "Change Description is entered");
	    Assert.assertTrue(editormainpage.addPurpose(testdata.get("Purpose")), "Purpose is entered");
	    Assert.assertTrue(editormainpage.addScope(testdata.get("Scope")), "Scope is entered");
	    Assert.assertTrue(editormainpage.addDefinitions(testdata.get("Definitions")), "Definition is enetered");
	    
	    // Validate and save the changes
	    Assert.assertTrue(editormainpage.validateSuccess(), "Successfully validated");
	    Assert.assertTrue(editormainpage.saveToSetupFC(), "Changes are saved");
	    
	    // Submit the Specification
	    submitwindow = editormainpage.openSubmitWindow();
	    Assert.assertNotNull(submitwindow, "Submit window is opened");
	    submitwindow.submitSpecification();
	    submitwindow.clickOnBackToSetupFC();
	    
	    // Logout from Setup.FC
	    loginpage = taskpage.Logout();
	    homepage = loginpage.Login("person0", "person0");
	    taskpage = homepage.NavigateToTaskPage();
	    Assert.assertNotNull(taskpage, "Task page is opened");
	    
	    Assert.assertTrue(taskpage.SearchBySpecID(ini.ReadIniFile(TestUtil.Environment, "EnvironmentSpecID")), "Search for the spec ID");
		Assert.assertTrue(taskpage.SelectTask(ini.ReadIniFile(TestUtil.Environment, "EnvironmentSpecID")), "Task is selected");
		
		taskactionspage = taskpage.selectTakeAction();
		Assert.assertTrue(taskactionspage.approveSpecification("MSTQA Test - Approve Task"), "Task is approved");
	    
	}
	
	
	@Test(dataProvider = "data", dataProviderClass = Datareader.class, priority = 6)
	@ICustomReport(Author = "Rashmi", Suite = "Regression")
	public void SFC_SP006__ApprovePropertyChangeSpecification(HashMap<String, String> testdata) {
		
		homepage = loginpage.Login(testdata.get("UserName1"), testdata.get("Password1"));
		specpage = homepage.NavigateToSpecificationPage();
		Assert.assertNotNull(specpage, "Navigate to the specification page");
		
		// Create a new specification
		newspecpage = specpage.NavigateToNewSpecification();
		Assert.assertNotNull(newspecpage, "Navigated to new specification page");
		
		Assert.assertTrue(newspecpage.AddGeneralSettings(testdata.get("Title"), testdata.get("Class"), testdata.get("SubClass"), testdata.get("Department"))
				, "General settings data is added");
		Assert.assertTrue(newspecpage.Next(), "Clicked on Next button");
		Assert.assertTrue(newspecpage.SelectWorkFlow(testdata.get("WorkflowID")), "Selected workflow ID: " + testdata.get("WorkflowID"));
		Assert.assertTrue(newspecpage.Next(), "Clicked on Next button");
		Assert.assertTrue(newspecpage.CreateSpec(), "Clicked on Create button");
		
		Assert.assertNotNull(newspecpage.VerifySuccessMsg(), "Verified the message: " + newspecpage.VerifySuccessMsg());
		
		Assert.assertNotNull(newspecpage.GetCreateSpecID(), "Specification is created: " + newspecpage.GetCreateSpecID());		
		ini.WriteIniFile(TestUtil.Environment, "EnvironmentSpecID", newspecpage.GetCreateSpecID());
		
		// Lock spec for edit and create a task
		Assert.assertTrue(newspecpage.LockForEdit(testdata.get("Parameter3")), "Locked for Spec Edit: " + testdata.get("Parameter3"));
		
		// Navigate to Tasks section and load the spec in editor
		taskpage = homepage.NavigateToTaskPage();
		Assert.assertNotNull(taskpage, "");
		Assert.assertTrue(taskpage.SearchBySpecID(ini.ReadIniFile(TestUtil.Environment, "EnvironmentSpecID")), "Search for the spec ID");
		Assert.assertTrue(taskpage.SelectTask(ini.ReadIniFile(TestUtil.Environment, "EnvironmentSpecID")), "Task is selected");
		
		Assert.assertTrue(taskpage.updateSpecificationMetadata("- Updated", testdata.get("Change-Reason"), testdata.get("Change-Desc")));
		
		// Logout from Setup.FC
	    loginpage = taskpage.Logout();
	    homepage = loginpage.Login("person0", "person0");
	    taskpage = homepage.NavigateToTaskPage();
	    Assert.assertNotNull(taskpage, "Task page is opened");
	    
	    Assert.assertTrue(taskpage.SearchBySpecID(ini.ReadIniFile(TestUtil.Environment, "EnvironmentSpecID")), "Search for the spec ID");
		Assert.assertTrue(taskpage.SelectTask(ini.ReadIniFile(TestUtil.Environment, "EnvironmentSpecID")), "Task is selected");
		
		taskactionspage = taskpage.selectTakeAction();
		Assert.assertTrue(taskactionspage.approveSpecification("MSTQA Test - Approve Task"), "Task is approved");

	}
	
	
	@Test(dataProvider = "data", dataProviderClass = Datareader.class, priority = 7)
	@ICustomReport(Author = "Rashmi", Suite = "Regression")
	public void SFC_SP007__ApproveTerminationSpecification(HashMap<String, String> testdata) {
		
		homepage = loginpage.Login(testdata.get("UserName1"), testdata.get("Password1"));
		specpage = homepage.NavigateToSpecificationPage();
		Assert.assertNotNull(specpage, "Navigate to the specification page");
		
		// Create a new specification
		newspecpage = specpage.NavigateToNewSpecification();
		Assert.assertNotNull(newspecpage, "Navigated to new specification page");
		
		Assert.assertTrue(newspecpage.AddGeneralSettings(testdata.get("Title"), testdata.get("Class"), testdata.get("SubClass"), testdata.get("Department"))
				, "General settings data is added");
		Assert.assertTrue(newspecpage.Next(), "Clicked on Next button");
		Assert.assertTrue(newspecpage.SelectWorkFlow(testdata.get("WorkflowID")), "Selected workflow ID: " + testdata.get("WorkflowID"));
		Assert.assertTrue(newspecpage.Next(), "Clicked on Next button");
		Assert.assertTrue(newspecpage.CreateSpec(), "Clicked on Create button");
		
		Assert.assertNotNull(newspecpage.VerifySuccessMsg(), "Verified the message: " + newspecpage.VerifySuccessMsg());
		
		Assert.assertNotNull(newspecpage.GetCreateSpecID(), "Specification is created: " + newspecpage.GetCreateSpecID());		
		ini.WriteIniFile(TestUtil.Environment, "EnvironmentSpecID", newspecpage.GetCreateSpecID());
		
		// Lock spec for edit and create a task
		Assert.assertTrue(newspecpage.LockForEdit(testdata.get("Parameter3")), "Locked for Spec Edit: " + testdata.get("Parameter3"));
		
		// Navigate to Tasks section and load the spec in editor
		taskpage = homepage.NavigateToTaskPage();
		Assert.assertNotNull(taskpage, "");
		Assert.assertTrue(taskpage.SearchBySpecID(ini.ReadIniFile(TestUtil.Environment, "EnvironmentSpecID")), "Search for the spec ID");
		Assert.assertTrue(taskpage.SelectTask(ini.ReadIniFile(TestUtil.Environment, "EnvironmentSpecID")), "Task is selected");
		
		Assert.assertTrue(taskpage.confirmTerminationSpecification(testdata.get("Change-Reason"), testdata.get("Change-Desc")));
		
		// Logout from Setup.FC
	    loginpage = taskpage.Logout();
	    homepage = loginpage.Login("person0", "person0");
	    taskpage = homepage.NavigateToTaskPage();
	    Assert.assertNotNull(taskpage, "Task page is opened");
	    
	    Assert.assertTrue(taskpage.SearchBySpecID(ini.ReadIniFile(TestUtil.Environment, "EnvironmentSpecID")), "Search for the spec ID");
		Assert.assertTrue(taskpage.SelectTask(ini.ReadIniFile(TestUtil.Environment, "EnvironmentSpecID")), "Task is selected");
		
		taskactionspage = taskpage.selectTakeAction();
		Assert.assertTrue(taskactionspage.approveSpecification("MSTQA Test - Approve Task"), "Task is approved");

	}
	
	
	@Test(dataProvider = "data", dataProviderClass = Datareader.class, priority = 8)
	@ICustomReport(Author = "Rashmi", Suite = "Regression")
	public void SFC_SP008__ApproveTheRejectedContentChangeTask(HashMap<String, String> testdata) {
		
		homepage = loginpage.Login(testdata.get("UserName1"), testdata.get("Password1"));
		specpage = homepage.NavigateToSpecificationPage();
		Assert.assertNotNull(specpage, "Navigate to the specification page");
		
		// Create a new specification
		newspecpage = specpage.NavigateToNewSpecification();
		Assert.assertNotNull(newspecpage, "Navigated to new specification page");
		
		Assert.assertTrue(newspecpage.AddGeneralSettings(testdata.get("Title"), testdata.get("Class"), testdata.get("SubClass"), testdata.get("Department"))
				, "General settings data is added");
		Assert.assertTrue(newspecpage.Next(), "Clicked on Next button");
		Assert.assertTrue(newspecpage.SelectWorkFlow(testdata.get("WorkflowID")), "Selected workflow ID: " + testdata.get("WorkflowID"));
		Assert.assertTrue(newspecpage.Next(), "Clicked on Next button");
		Assert.assertTrue(newspecpage.CreateSpec(), "Clicked on Create button");
		
		Assert.assertNotNull(newspecpage.VerifySuccessMsg(), "Verified the message: " + newspecpage.VerifySuccessMsg());
		
		Assert.assertNotNull(newspecpage.GetCreateSpecID(), "Specification is created: " + newspecpage.GetCreateSpecID());		
		ini.WriteIniFile(TestUtil.Environment, "EnvironmentSpecID", newspecpage.GetCreateSpecID());
		
		// Lock spec for edit and create a task
		Assert.assertTrue(newspecpage.LockForEdit(testdata.get("Parameter3")), "Locked for Spec Edit: " + testdata.get("Parameter3"));
		
		// Navigate to Tasks section and load the spec in editor
		taskpage = homepage.NavigateToTaskPage();
		Assert.assertNotNull(taskpage, "");
		Assert.assertTrue(taskpage.SearchBySpecID(ini.ReadIniFile(TestUtil.Environment, "EnvironmentSpecID")), "Search for the spec ID");
		Assert.assertTrue(taskpage.SelectTask(ini.ReadIniFile(TestUtil.Environment, "EnvironmentSpecID")), "Task is selected");
		
		editormainpage = taskpage.confirmEditSpecificationAction();
		Assert.assertNotNull(editormainpage);
		
		// Fill mandatory and additional fields
		editormainpage.closeChapterTree();
	    Assert.assertTrue(editormainpage.addChangeReason(testdata.get("Change-Reason")), "Change Reason is entered");
	    Assert.assertTrue(editormainpage.addChangeDescription(testdata.get("Change-Desc")), "Change Description is entered");
	    Assert.assertTrue(editormainpage.addPurpose(testdata.get("Purpose")), "Purpose is entered");
	    Assert.assertTrue(editormainpage.addScope(testdata.get("Scope")), "Scope is entered");
	    Assert.assertTrue(editormainpage.addDefinitions(testdata.get("Definitions")), "Definition is enetered");
	    
	    // Validate and save the changes
	    Assert.assertTrue(editormainpage.validateSuccess(), "Successfully validated");
	    Assert.assertTrue(editormainpage.saveToSetupFC(), "Changes are saved");
	    
	    // Submit the Specification
	    submitwindow = editormainpage.openSubmitWindow();
	    Assert.assertNotNull(submitwindow, "Submit window is opened");
	    submitwindow.submitSpecification();
	    submitwindow.clickOnBackToSetupFC();
	    
	    // Logout from Setup.FC
	    loginpage = taskpage.Logout();
	    homepage = loginpage.Login("person0", "person0");
	    taskpage = homepage.NavigateToTaskPage();
	    Assert.assertNotNull(taskpage, "Task page is opened");
	    
	    Assert.assertTrue(taskpage.SearchBySpecID(ini.ReadIniFile(TestUtil.Environment, "EnvironmentSpecID")), "Search for the spec ID");
		Assert.assertTrue(taskpage.SelectTask(ini.ReadIniFile(TestUtil.Environment, "EnvironmentSpecID")), "Task is selected");
		
		// Reject the task
		taskactionspage = taskpage.selectTakeAction();
		Assert.assertTrue(taskactionspage.rejectSpecification("MSTQA Test - Reject Task"), "Task is approved");
		
		// Logout as approver and login as owner
		loginpage = taskpage.Logout();
	    homepage = loginpage.Login(testdata.get("UserName1"), testdata.get("Password1"));
	    
	    // Search for the task
	    taskpage = homepage.NavigateToTaskPage();
	    Assert.assertNotNull(taskpage, "Task page is opened");
	    Assert.assertTrue(taskpage.SearchBySpecID(ini.ReadIniFile(TestUtil.Environment, "EnvironmentSpecID")), "Search for the spec ID");
		Assert.assertTrue(taskpage.SelectTask(ini.ReadIniFile(TestUtil.Environment, "EnvironmentSpecID")), "Task is selected");
		
		// Retry for approval
		Assert.assertTrue(taskpage.confirmRetrySpecification("Retry for Approval"), "Retry for approval");
		
		// Logout as owner and login as approver
		loginpage = taskpage.Logout();
	    homepage = loginpage.Login("person0", "person0");
	    taskpage = homepage.NavigateToTaskPage();
	    Assert.assertNotNull(taskpage, "Task page is opened");
	    
	    Assert.assertTrue(taskpage.SearchBySpecID(ini.ReadIniFile(TestUtil.Environment, "EnvironmentSpecID")), "Search for the spec ID");
		Assert.assertTrue(taskpage.SelectTask(ini.ReadIniFile(TestUtil.Environment, "EnvironmentSpecID")), "Task is selected");
		
		// Approve the task
		taskactionspage = taskpage.selectTakeAction();
		Assert.assertTrue(taskactionspage.approveSpecification("MSTQA Test - Approve Task"), "Task is approved");
		
	}
	
	
	@Test(dataProvider = "data", dataProviderClass = Datareader.class, priority = 9)
	@ICustomReport(Author = "Rashmi", Suite = "Regression")
	public void SFC_SP009__ApproveTheRejectedPropertyChangeTask(HashMap<String, String> testdata) {
		
		homepage = loginpage.Login(testdata.get("UserName1"), testdata.get("Password1"));
		specpage = homepage.NavigateToSpecificationPage();
		Assert.assertNotNull(specpage, "Navigate to the specification page");
		
		// Create a new specification
		newspecpage = specpage.NavigateToNewSpecification();
		Assert.assertNotNull(newspecpage, "Navigated to new specification page");
		
		Assert.assertTrue(newspecpage.AddGeneralSettings(testdata.get("Title"), testdata.get("Class"), testdata.get("SubClass"), testdata.get("Department"))
				, "General settings data is added");
		Assert.assertTrue(newspecpage.Next(), "Clicked on Next button");
		Assert.assertTrue(newspecpage.SelectWorkFlow(testdata.get("WorkflowID")), "Selected workflow ID: " + testdata.get("WorkflowID"));
		Assert.assertTrue(newspecpage.Next(), "Clicked on Next button");
		Assert.assertTrue(newspecpage.CreateSpec(), "Clicked on Create button");
		
		Assert.assertNotNull(newspecpage.VerifySuccessMsg(), "Verified the message: " + newspecpage.VerifySuccessMsg());
		
		Assert.assertNotNull(newspecpage.GetCreateSpecID(), "Specification is created: " + newspecpage.GetCreateSpecID());		
		ini.WriteIniFile(TestUtil.Environment, "EnvironmentSpecID", newspecpage.GetCreateSpecID());
		
		// Lock spec for edit and create a task
		Assert.assertTrue(newspecpage.LockForEdit(testdata.get("Parameter3")), "Locked for Spec Edit: " + testdata.get("Parameter3"));
		
		// Navigate to Tasks section and load the spec in editor
		taskpage = homepage.NavigateToTaskPage();
		Assert.assertNotNull(taskpage, "");
		Assert.assertTrue(taskpage.SearchBySpecID(ini.ReadIniFile(TestUtil.Environment, "EnvironmentSpecID")), "Search for the spec ID");
		Assert.assertTrue(taskpage.SelectTask(ini.ReadIniFile(TestUtil.Environment, "EnvironmentSpecID")), "Task is selected");
		
		Assert.assertTrue(taskpage.updateSpecificationMetadata("- Updated", testdata.get("Change-Reason"), testdata.get("Change-Desc")));
		
		// Logout from Setup.FC
	    loginpage = taskpage.Logout();
	    homepage = loginpage.Login("person0", "person0");
	    taskpage = homepage.NavigateToTaskPage();
	    Assert.assertNotNull(taskpage, "Task page is opened");
	    
	    Assert.assertTrue(taskpage.SearchBySpecID(ini.ReadIniFile(TestUtil.Environment, "EnvironmentSpecID")), "Search for the spec ID");
		Assert.assertTrue(taskpage.SelectTask(ini.ReadIniFile(TestUtil.Environment, "EnvironmentSpecID")), "Task is selected");
		
		taskactionspage = taskpage.selectTakeAction();
		Assert.assertTrue(taskactionspage.rejectSpecification("MSTQA Test - Reject Task"), "Task is approved");
		
		// Logout as approver and login as owner
		loginpage = taskpage.Logout();
		homepage = loginpage.Login(testdata.get("UserName1"), testdata.get("Password1"));

		// Search for the task
		taskpage = homepage.NavigateToTaskPage();
		Assert.assertNotNull(taskpage, "Task page is opened");
		Assert.assertTrue(taskpage.SearchBySpecID(ini.ReadIniFile(TestUtil.Environment, "EnvironmentSpecID")), "Search for the spec ID");
		Assert.assertTrue(taskpage.SelectTask(ini.ReadIniFile(TestUtil.Environment, "EnvironmentSpecID")), "Task is selected");

		// Retry for approval
		Assert.assertTrue(taskpage.confirmRetrySpecification("Retry for Approval"), "Retry for approval");

		// Logout as owner and login as approver
		loginpage = taskpage.Logout();
		homepage = loginpage.Login("person0", "person0");
		taskpage = homepage.NavigateToTaskPage();
		Assert.assertNotNull(taskpage, "Task page is opened");

		Assert.assertTrue(taskpage.SearchBySpecID(ini.ReadIniFile(TestUtil.Environment, "EnvironmentSpecID")), "Search for the spec ID");
		Assert.assertTrue(taskpage.SelectTask(ini.ReadIniFile(TestUtil.Environment, "EnvironmentSpecID")), "Task is selected");

		// Approve the task
		taskactionspage = taskpage.selectTakeAction();
		Assert.assertTrue(taskactionspage.approveSpecification("MSTQA Test - Approve Task"), "Task is approved");

	}
	
	
	@Test(dataProvider = "data", dataProviderClass = Datareader.class, priority = 10)
	@ICustomReport(Author = "Rashmi", Suite = "Regression")
	public void SFC_SP010__ApproveTheRejectedTerminationTask(HashMap<String, String> testdata) {
		
		homepage = loginpage.Login(testdata.get("UserName1"), testdata.get("Password1"));
		specpage = homepage.NavigateToSpecificationPage();
		Assert.assertNotNull(specpage, "Navigate to the specification page");
		
		// Create a new specification
		newspecpage = specpage.NavigateToNewSpecification();
		Assert.assertNotNull(newspecpage, "Navigated to new specification page");
		
		Assert.assertTrue(newspecpage.AddGeneralSettings(testdata.get("Title"), testdata.get("Class"), testdata.get("SubClass"), testdata.get("Department"))
				, "General settings data is added");
		Assert.assertTrue(newspecpage.Next(), "Clicked on Next button");
		Assert.assertTrue(newspecpage.SelectWorkFlow(testdata.get("WorkflowID")), "Selected workflow ID: " + testdata.get("WorkflowID"));
		Assert.assertTrue(newspecpage.Next(), "Clicked on Next button");
		Assert.assertTrue(newspecpage.CreateSpec(), "Clicked on Create button");
		
		Assert.assertNotNull(newspecpage.VerifySuccessMsg(), "Verified the message: " + newspecpage.VerifySuccessMsg());
		
		Assert.assertNotNull(newspecpage.GetCreateSpecID(), "Specification is created: " + newspecpage.GetCreateSpecID());		
		ini.WriteIniFile(TestUtil.Environment, "EnvironmentSpecID", newspecpage.GetCreateSpecID());
		
		// Lock spec for edit and create a task
		Assert.assertTrue(newspecpage.LockForEdit(testdata.get("Parameter3")), "Locked for Spec Edit: " + testdata.get("Parameter3"));
		
		// Navigate to Tasks section and load the spec in editor
		taskpage = homepage.NavigateToTaskPage();
		Assert.assertNotNull(taskpage, "");
		Assert.assertTrue(taskpage.SearchBySpecID(ini.ReadIniFile(TestUtil.Environment, "EnvironmentSpecID")), "Search for the spec ID");
		Assert.assertTrue(taskpage.SelectTask(ini.ReadIniFile(TestUtil.Environment, "EnvironmentSpecID")), "Task is selected");
		
		Assert.assertTrue(taskpage.confirmTerminationSpecification(testdata.get("Change-Reason"), testdata.get("Change-Desc")));
		
		// Logout from Setup.FC
	    loginpage = taskpage.Logout();
	    homepage = loginpage.Login("person0", "person0");
	    taskpage = homepage.NavigateToTaskPage();
	    Assert.assertNotNull(taskpage, "Task page is opened");
	    
	    Assert.assertTrue(taskpage.SearchBySpecID(ini.ReadIniFile(TestUtil.Environment, "EnvironmentSpecID")), "Search for the spec ID");
		Assert.assertTrue(taskpage.SelectTask(ini.ReadIniFile(TestUtil.Environment, "EnvironmentSpecID")), "Task is selected");
		
		taskactionspage = taskpage.selectTakeAction();
		Assert.assertTrue(taskactionspage.rejectSpecification("MSTQA Test - Reject Task"), "Task is approved");
		
		// Logout as approver and login as owner
		loginpage = taskpage.Logout();
		homepage = loginpage.Login(testdata.get("UserName1"), testdata.get("Password1"));

		// Search for the task
		taskpage = homepage.NavigateToTaskPage();
		Assert.assertNotNull(taskpage, "Task page is opened");
		Assert.assertTrue(taskpage.SearchBySpecID(ini.ReadIniFile(TestUtil.Environment, "EnvironmentSpecID")), "Search for the spec ID");
		Assert.assertTrue(taskpage.SelectTask(ini.ReadIniFile(TestUtil.Environment, "EnvironmentSpecID")), "Task is selected");

		// Retry for approval
		Assert.assertTrue(taskpage.confirmRetrySpecification("Retry for Approval"), "Retry for approval");

		// Logout as owner and login as approver
		loginpage = taskpage.Logout();
		homepage = loginpage.Login("person0", "person0");
		taskpage = homepage.NavigateToTaskPage();
		Assert.assertNotNull(taskpage, "Task page is opened");

		Assert.assertTrue(taskpage.SearchBySpecID(ini.ReadIniFile(TestUtil.Environment, "EnvironmentSpecID")), "Search for the spec ID");
		Assert.assertTrue(taskpage.SelectTask(ini.ReadIniFile(TestUtil.Environment, "EnvironmentSpecID")), "Task is selected");

		// Approve the task
		taskactionspage = taskpage.selectTakeAction();
		Assert.assertTrue(taskactionspage.approveSpecification("MSTQA Test - Approve Task"), "Task is approved");

	}
	
	
	@Test(dataProvider = "data", dataProviderClass = Datareader.class, priority = 11)
	@ICustomReport(Author = "Rashmi", Suite = "Regression")
	public void SFC_SP011__ReEditTheRejectedContentChangeTask(HashMap<String, String> testdata) {
		
		homepage = loginpage.Login(testdata.get("UserName1"), testdata.get("Password1"));
		specpage = homepage.NavigateToSpecificationPage();
		Assert.assertNotNull(specpage, "Navigate to the specification page");
		
		// Create a new specification
		newspecpage = specpage.NavigateToNewSpecification();
		Assert.assertNotNull(newspecpage, "Navigated to new specification page");
		
		Assert.assertTrue(newspecpage.AddGeneralSettings(testdata.get("Title"), testdata.get("Class"), testdata.get("SubClass"), testdata.get("Department"))
				, "General settings data is added");
		Assert.assertTrue(newspecpage.Next(), "Clicked on Next button");
		Assert.assertTrue(newspecpage.SelectWorkFlow(testdata.get("WorkflowID")), "Selected workflow ID: " + testdata.get("WorkflowID"));
		Assert.assertTrue(newspecpage.Next(), "Clicked on Next button");
		Assert.assertTrue(newspecpage.CreateSpec(), "Clicked on Create button");
		
		Assert.assertNotNull(newspecpage.VerifySuccessMsg(), "Verified the message: " + newspecpage.VerifySuccessMsg());
		
		Assert.assertNotNull(newspecpage.GetCreateSpecID(), "Specification is created: " + newspecpage.GetCreateSpecID());		
		ini.WriteIniFile(TestUtil.Environment, "EnvironmentSpecID", newspecpage.GetCreateSpecID());
		
		// Lock spec for edit and create a task
		Assert.assertTrue(newspecpage.LockForEdit(testdata.get("Parameter3")), "Locked for Spec Edit: " + testdata.get("Parameter3"));
		
		// Navigate to Tasks section and load the spec in editor
		taskpage = homepage.NavigateToTaskPage();
		Assert.assertNotNull(taskpage, "");
		Assert.assertTrue(taskpage.SearchBySpecID(ini.ReadIniFile(TestUtil.Environment, "EnvironmentSpecID")), "Search for the spec ID");
		Assert.assertTrue(taskpage.SelectTask(ini.ReadIniFile(TestUtil.Environment, "EnvironmentSpecID")), "Task is selected");
		
		editormainpage = taskpage.confirmEditSpecificationAction();
		Assert.assertNotNull(editormainpage);
		
		// Fill mandatory and additional fields
		editormainpage.closeChapterTree();
	    Assert.assertTrue(editormainpage.addChangeReason(testdata.get("Change-Reason")), "Change Reason is entered");
	    Assert.assertTrue(editormainpage.addChangeDescription(testdata.get("Change-Desc")), "Change Description is entered");
	    Assert.assertTrue(editormainpage.addPurpose(testdata.get("Purpose")), "Purpose is entered");
	    Assert.assertTrue(editormainpage.addScope(testdata.get("Scope")), "Scope is entered");
	    Assert.assertTrue(editormainpage.addDefinitions(testdata.get("Definitions")), "Definition is enetered");
	    
	    // Validate and save the changes
	    Assert.assertTrue(editormainpage.validateSuccess(), "Successfully validated");
	    Assert.assertTrue(editormainpage.saveToSetupFC(), "Changes are saved");
	    
	    // Submit the Specification
	    submitwindow = editormainpage.openSubmitWindow();
	    Assert.assertNotNull(submitwindow, "Submit window is opened");
	    submitwindow.submitSpecification();
	    submitwindow.clickOnBackToSetupFC();
	    
	    // Logout from Setup.FC
	    loginpage = taskpage.Logout();
	    homepage = loginpage.Login("person0", "person0");
	    taskpage = homepage.NavigateToTaskPage();
	    Assert.assertNotNull(taskpage, "Task page is opened");
	    
	    Assert.assertTrue(taskpage.SearchBySpecID(ini.ReadIniFile(TestUtil.Environment, "EnvironmentSpecID")), "Search for the spec ID");
		Assert.assertTrue(taskpage.SelectTask(ini.ReadIniFile(TestUtil.Environment, "EnvironmentSpecID")), "Task is selected");
		
		// Reject the task
		taskactionspage = taskpage.selectTakeAction();
		Assert.assertTrue(taskactionspage.rejectSpecification("MSTQA Test - Reject Task"), "Task is approved");
		
		// Logout as approver and login as owner
		loginpage = taskpage.Logout();
	    homepage = loginpage.Login(testdata.get("UserName1"), testdata.get("Password1"));
	    
	    // Search for the task
	    taskpage = homepage.NavigateToTaskPage();
	    Assert.assertNotNull(taskpage, "Task page is opened");
	    Assert.assertTrue(taskpage.SearchBySpecID(ini.ReadIniFile(TestUtil.Environment, "EnvironmentSpecID")), "Search for the spec ID");
		Assert.assertTrue(taskpage.SelectTask(ini.ReadIniFile(TestUtil.Environment, "EnvironmentSpecID")), "Task is selected");
		
		// Retry for approval
		Assert.assertTrue(taskpage.confirmReEditTaskAction("Re-edit the task"), "Retry for approval");
		
		Assert.assertTrue(taskpage.SearchBySpecID(ini.ReadIniFile(TestUtil.Environment, "EnvironmentSpecID")), "Search for the spec ID");
		Assert.assertTrue(taskpage.SelectTask(ini.ReadIniFile(TestUtil.Environment, "EnvironmentSpecID")), "Task is selected");
		editormainpage = taskpage.confirmEditSpecificationAction();
		Assert.assertNotNull(editormainpage);
		
	}
	
	
	@Test(dataProvider = "data", dataProviderClass = Datareader.class, priority = 12)
	@ICustomReport(Author = "Rashmi", Suite = "Regression")
	public void SFC_SP012__ValidateDiscardChangesFunctionality(HashMap<String, String> testdata) {
		
		homepage = loginpage.Login(testdata.get("UserName1"), testdata.get("Password1"));
		specpage = homepage.NavigateToSpecificationPage();
		Assert.assertNotNull(specpage, "Navigate to the specification page");
		
		// Create a new specification
		newspecpage = specpage.NavigateToNewSpecification();
		Assert.assertNotNull(newspecpage, "Navigated to new specification page");
		
		Assert.assertTrue(newspecpage.AddGeneralSettings(testdata.get("Title"), testdata.get("Class"), testdata.get("SubClass"), testdata.get("Department"))
				, "General settings data is added");
		Assert.assertTrue(newspecpage.Next(), "Clicked on Next button");
		Assert.assertTrue(newspecpage.SelectWorkFlow(testdata.get("WorkflowID")), "Selected workflow ID: " + testdata.get("WorkflowID"));
		Assert.assertTrue(newspecpage.Next(), "Clicked on Next button");
		Assert.assertTrue(newspecpage.CreateSpec(), "Clicked on Create button");
		
		Assert.assertNotNull(newspecpage.VerifySuccessMsg(), "Verified the message: " + newspecpage.VerifySuccessMsg());
		
		Assert.assertNotNull(newspecpage.GetCreateSpecID(), "Specification is created: " + newspecpage.GetCreateSpecID());		
		ini.WriteIniFile(TestUtil.Environment, "EnvironmentSpecID", newspecpage.GetCreateSpecID());
		
		// Lock spec for edit and create a task
		Assert.assertTrue(newspecpage.LockForEdit(testdata.get("Parameter3")), "Locked for Spec Edit: " + testdata.get("Parameter3"));
		
		// Navigate to Tasks section and load the spec in editor
		taskpage = homepage.NavigateToTaskPage();
		Assert.assertNotNull(taskpage, "");
		Assert.assertTrue(taskpage.SearchBySpecID(ini.ReadIniFile(TestUtil.Environment, "EnvironmentSpecID")), "Search for the spec ID");
		Assert.assertTrue(taskpage.SelectTask(ini.ReadIniFile(TestUtil.Environment, "EnvironmentSpecID")), "Task is selected");
		
		editormainpage = taskpage.confirmEditSpecificationAction();
		Assert.assertNotNull(editormainpage);
		
		// Fill mandatory and additional fields
		editormainpage.closeChapterTree();
	    Assert.assertTrue(editormainpage.addChangeReason(testdata.get("Change-Reason")), "Change Reason is entered");
	    Assert.assertTrue(editormainpage.addChangeDescription(testdata.get("Change-Desc")), "Change Description is entered");
	    Assert.assertTrue(editormainpage.addPurpose(testdata.get("Purpose")), "Purpose is entered");
	    Assert.assertTrue(editormainpage.addScope(testdata.get("Scope")), "Scope is entered");
	    Assert.assertTrue(editormainpage.addDefinitions(testdata.get("Definitions")), "Definition is enetered");
	    
	    // Validate and save the changes
	    Assert.assertTrue(editormainpage.validateSuccess(), "Successfully validated");
	    Assert.assertTrue(editormainpage.saveToSetupFC(), "Changes are saved");
	    
	    // confirm the Discard Changes Task Action
	    taskpage = editormainpage.switchBackToSetupfcTaskPage();
	    Assert.assertNotNull(taskpage, "Switched back to task page");
	    Assert.assertTrue(taskpage.SearchBySpecID(ini.ReadIniFile(TestUtil.Environment, "EnvironmentSpecID")), "Search for the spec ID");
	    Assert.assertTrue(taskpage.confirmDiscardChangesTaskAction("MSTQA Test - Discrad Changes"), "Confirmed discard changes");
	    
	    // Search the spec and lock for edit
	    specpage = homepage.NavigateToSpecificationPage();
	    searchspecpage = specpage.NavigateToSearchSpecification();
	    Assert.assertNotNull(searchspecpage, "Navigated to search");
	    
	    Assert.assertTrue(searchspecpage.searchSpecByID(ini.ReadIniFile(TestUtil.Environment, "EnvironmentSpecID")), "Search for the spec id");
	    Assert.assertTrue(searchspecpage.LockForEdit(testdata.get("Parameter3")), "Lock for Edit spec: " + testdata.get("Parameter3"));
	    
	    // Navigate to task list and open in editor
	    taskpage = homepage.NavigateToTaskPage();
	    Assert.assertNotNull(taskpage, "Navigated to Task list");
	    
	    Assert.assertTrue(taskpage.SearchBySpecID(ini.ReadIniFile(TestUtil.Environment, "EnvironmentSpecID")), "Search for the spec ID");
		Assert.assertTrue(taskpage.SelectTask(ini.ReadIniFile(TestUtil.Environment, "EnvironmentSpecID")), "Task is selected");
		
		editormainpage = taskpage.confirmEditSpecificationAction();
		Assert.assertNotNull(editormainpage);
		
		// Verify the Discard change pop-up
		Assert.assertTrue(editormainpage.verifySpecContentOutdatedPopup(), "Verify Content Outdated popup");
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
