/**
 * 
 */
package com.setupfc.qa.testcases;

import java.util.HashMap;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.setupfc.qa.Interface.ICustomReport;
import com.setupfc.qa.base.TestBase;
import com.setupfc.qa.config.IniFile;
import com.setupfc.qa.listeners.DataproviderListener;
import com.setupfc.qa.listeners.TestNGlisteners;
import com.setupfc.qa.pages.SetupFC.AdminPage;
import com.setupfc.qa.pages.SetupFC.EditConfigPage;
import com.setupfc.qa.pages.SetupFC.HomePage;
import com.setupfc.qa.pages.SetupFC.LoginPage;
import com.setupfc.qa.pages.SetupFC.NewConfigPage;
import com.setupfc.qa.pages.SetupFC.SearchConfigPage;
import com.setupfc.qa.testdata.Datareader;
import com.setupfc.qa.util.TestUtil;

/**
 * @author smallika
 * 
 * @ModifiedBy rhalli
 *
 */
@Listeners({ TestNGlisteners.class, DataproviderListener.class })
public class AdminTest extends TestBase {

	LoginPage loginpage;
	HomePage homepage;
	AdminPage adminpage;
	NewConfigPage newconfig;
	SearchConfigPage searchconfig;
	EditConfigPage editConfig;
	IniFile ini = IniFile.GetInstance();

	@BeforeMethod
	public void setup() {
		
		initilization();
		loginpage = new LoginPage();
	}

	@AfterMethod
	public void TearDown() {
		
		CloseBrowser();
	}

	
	@Test(dataProvider = "data", dataProviderClass = Datareader.class, priority = 1)
	@ICustomReport(Author = "Siddharth", Suite = "Smoke")
	public void SFC_WC001__CreateNewConfig(HashMap<String, String> testdata) {
		
		homepage = loginpage.Login(testdata.get("UserName1"), testdata.get("Password1"));
		adminpage = homepage.NavigateToAdminPage();
		newconfig = adminpage.NavigateToNewConfigPage();
		
		newconfig.AddTitle("MSTQA Test");
		newconfig.SelectClass("08");
		newconfig.SelectSubClass("WebBase");
		newconfig.SelectDepartment("ALL");
		newconfig.AddNormalApprovalTier();
		newconfig.AddNormalGroupConfig("MTQA-SetupfcAndEditor");
		newconfig.SaveConfig();
		
		Assert.assertTrue(newconfig.VerifySuccessMessage());
		System.out.println(newconfig.GetNewConfigID());
		System.out.println(newconfig.GetNewConfigName());
		
		ini.WriteIniFile(TestUtil.Environment, "NewWorkflowID", newconfig.GetNewConfigID());
	}
	
	@Test(dataProvider = "data", dataProviderClass = Datareader.class, priority = 2)
	@ICustomReport(Author = "Rashmi", Suite = "Smoke")
	public void SFC_WC002__SearchWorkflowConfig(HashMap<String, String> testdata) {
		
		homepage = loginpage.Login(testdata.get("UserName1"), testdata.get("Password1"));
		adminpage = homepage.NavigateToAdminPage();
		Assert.assertNotNull(adminpage, "Navigated to the Admin page");
		
		searchconfig = adminpage.NavigateToSearchConfigPage();
		Assert.assertNotNull(searchconfig, "Navigated to the search config page");
		
		searchconfig.enterID(ini.ReadIniFile(TestUtil.Environment, "NewWorkflowID"));
		searchconfig.searchConfig();
		Assert.assertTrue(searchconfig.clickOnSearchResult(ini.ReadIniFile(TestUtil.Environment, "NewWorkflowID")), "Search result is correct");
	}
	
	
	@Test(dataProvider = "data", dataProviderClass = Datareader.class, priority = 3)
	@ICustomReport(Author = "Rashmi", Suite = "Smoke")
	public void SFC_WC003__UpdateWorkflowConfig(HashMap<String, String> testdata) {
		
		homepage = loginpage.Login(testdata.get("UserName1"), testdata.get("Password1"));
		adminpage = homepage.NavigateToAdminPage();		
		Assert.assertNotNull(adminpage, "Navigated to the Admin page");
		
		searchconfig = adminpage.NavigateToSearchConfigPage();
		Assert.assertNotNull(searchconfig, "Navigated to the search config page");
		
		searchconfig.enterID(ini.ReadIniFile(TestUtil.Environment, "NewWorkflowID"));
		searchconfig.searchConfig();
		Assert.assertTrue(searchconfig.clickOnSearchResult(ini.ReadIniFile(TestUtil.Environment, "NewWorkflowID")), "Search result is correct");
		
		editConfig = searchconfig.clickOnEditConfig();
		
		editConfig.editTitle(" - Updated - ");		
		editConfig.saveUpdatedConfig();
		Assert.assertTrue(editConfig.verifyUpdatedConfigMessage(), "Config is updated successfully");
		
	}
	
	@Test(dataProvider = "data", dataProviderClass = Datareader.class, priority = 4)
	@ICustomReport(Author = "Rashmi", Suite = "Smoke")
	public void SFC_WC004__DeleteWorkflowConfig(HashMap<String, String> testdata) {
		
		homepage = loginpage.Login(testdata.get("UserName1"), testdata.get("Password1"));
		adminpage = homepage.NavigateToAdminPage();
		Assert.assertNotNull(adminpage, "Navigated to the Admin page");
		
		searchconfig = adminpage.NavigateToSearchConfigPage();
		Assert.assertNotNull(searchconfig, "Navigated to the Search page");
		
		searchconfig.enterID(ini.ReadIniFile(TestUtil.Environment, "NewWorkflowID"));
		searchconfig.searchConfig();
		Assert.assertTrue(searchconfig.clickOnSearchResult(ini.ReadIniFile(TestUtil.Environment, "NewWorkflowID")), "Search result is correct");
		
		searchconfig.clickOnDeleteConfig();
		Assert.assertTrue(searchconfig.verifyDeletePopupPresence(), "Workflow deletion popup is displayed");
		searchconfig.confirmWorkflowDeletion();
		Assert.assertTrue(searchconfig.verifyWorkflowDeletionMsg(), "Workflow Config Deletion Successful");
	}

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
