package com.setupfc.qa.testcases;

import java.util.HashMap;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.setupfc.qa.EditorCommon.AttachmentWindow;
import com.setupfc.qa.EditorCommon.EditorMainPage;
import com.setupfc.qa.EditorCommon.PLMReferenceWindow;
import com.setupfc.qa.EditorCommon.ReferenceWindow;
import com.setupfc.qa.Interface.ICustomReport;
import com.setupfc.qa.base.TestBase;
import com.setupfc.qa.config.IniFile;
import com.setupfc.qa.listeners.CustomAssert;
import com.setupfc.qa.listeners.DataproviderListener;
import com.setupfc.qa.listeners.TestNGlisteners;
import com.setupfc.qa.pages.SetupFC.HomePage;
import com.setupfc.qa.pages.SetupFC.LoginPage;
import com.setupfc.qa.pages.SetupFC.NewSpecPage;
import com.setupfc.qa.pages.SetupFC.SpecificationPage;
import com.setupfc.qa.pages.SetupFC.TaskPage;
import com.setupfc.qa.testdata.Datareader;
import com.setupfc.qa.util.TestUtil;

/**
 * @author rhalli
 *
 */
@Listeners({ TestNGlisteners.class, DataproviderListener.class })
public class CommonEditorTest extends TestBase {
	
	CustomAssert Assert = new CustomAssert();
	IniFile ini = IniFile.GetInstance();
	
	LoginPage loginpage;
	HomePage homepage;
	TaskPage taskpage;
	SpecificationPage specpage;
	NewSpecPage newspecpage;
	EditorMainPage editormainpage;
	AttachmentWindow attachmentwindow;
	ReferenceWindow referencewindow;
	PLMReferenceWindow plmreference;
	
	@BeforeMethod
	public void setup() {
		
		initilization();
		loginpage = new LoginPage();
	}

	@AfterMethod
	public void TearDown() {
		CloseBrowser();
	}
	
	@Test(dataProvider = "data", dataProviderClass = Datareader.class, priority = 1)
	@ICustomReport(Author = "Rashmi", Suite = "Regression")
	public void EDITOR_COMMON001__VerifySpecificationPageDetails(HashMap<String, String> testdata) {
		
		homepage = loginpage.Login(testdata.get("UserName1"), testdata.get("Password1"));
		specpage = homepage.NavigateToSpecificationPage();
		Assert.assertNotNull(specpage, "Navigate to the specification page");
		
		// Create a new specification
		newspecpage = specpage.NavigateToNewSpecification();
		Assert.assertNotNull(newspecpage, "Navigated to new specification page");
		
		Assert.assertTrue(newspecpage.AddGeneralSettings(testdata.get("Title"), testdata.get("Class"), testdata.get("SubClass"), testdata.get("Department"))
				, "General settings data is added");
		Assert.assertTrue(newspecpage.Next(), "Clicked on Next button");
		Assert.assertTrue(newspecpage.SelectWorkFlow(testdata.get("WorkflowID")), "Selected workflow ID: " + testdata.get("WorkflowID"));
		Assert.assertTrue(newspecpage.Next(), "Clicked on Next button");
		Assert.assertTrue(newspecpage.CreateSpec(), "Clicked on Create button");
		
		Assert.assertNotNull(newspecpage.VerifySuccessMsg(), "Verified the message: " + newspecpage.VerifySuccessMsg());
		
		Assert.assertNotNull(newspecpage.GetCreateSpecID(), "Specification is created: " + newspecpage.GetCreateSpecID());		
		ini.WriteIniFile(TestUtil.Environment, "EnvironmentSpecID", newspecpage.GetCreateSpecID());
		
		// Lock for edit and create a Content Change task
		Assert.assertTrue(newspecpage.LockForEdit(testdata.get("Parameter3")), "Locked for Spec Edit: " + testdata.get("Parameter3"));
		
		// Select the created task 
		taskpage = homepage.NavigateToTaskPage();
		Assert.assertNotNull(taskpage, "");
		Assert.assertTrue(taskpage.SearchBySpecID(ini.ReadIniFile(TestUtil.Environment, "EnvironmentSpecID")), "Search for the spec ID");
		Assert.assertTrue(taskpage.SelectTask(ini.ReadIniFile(TestUtil.Environment, "EnvironmentSpecID")), "Task is selected");
		Assert.assertTrue(taskpage.verifyTaskTitleInTaskDetails(testdata.get("Parameter4")), "Task Title is Edit Content Specification");
		
		// Confirm Edit SPecification action
		editormainpage = taskpage.confirmEditSpecificationAction();
		Assert.assertNotNull(editormainpage);

		// Verify the meta data
		Assert.VerifyContent(editormainpage.getSpecID(), ini.ReadIniFile(TestUtil.Environment, "EnvironmentSpecID"), "Verified the spec ID");
		Assert.VerifyContent(editormainpage.getSpecStatus(), "InUpdate", "Verified the spec status");
		Assert.VerifyContent(editormainpage.getSpecTitle(), testdata.get("Title"), "Verified the spec title");
		Assert.VerifyContent(editormainpage.getSpecAuthor(), "rhalli", "Verified the spec author");		
		Assert.VerifyContent(editormainpage.getSpecDepartment(), testdata.get("Department"), "Verified the spec department");
		Assert.VerifyContent(editormainpage.getSpecOwner(), "rhalli", "Verified the spec owner");
		Assert.VerifyContent(editormainpage.getSpecWorkflowConfig(), testdata.get("WorkflowID"), "Verified the spec workflow");
		Assert.VerifyContent(editormainpage.getSpecSubClass(), testdata.get("SubClass"), "Verified the spec sub class");
		
		// Verify error messages
		Assert.assertTrue(editormainpage.validateError(), "Validation error with blank fields");
		Assert.assertTrue(editormainpage.verifyChangeReasonErrorMessage("Change Reason is required."), "Change Reason Error Message");
		Assert.assertTrue(editormainpage.verifyChangeDescriptionErrorMessage("Change Description is required."), "Change Description Error Message");
		Assert.assertTrue(editormainpage.verifyPurposeErrorMessage("Purpose is required!"), "Purpose Error Message");
		Assert.assertTrue(editormainpage.verifyScopeErrorMessage("Scope is required!"), "Scope Error Message");
		
		// Verify dropdowns
		Assert.assertTrue(editormainpage.verifyDropdownOptions("Specification"));
		Assert.assertTrue(editormainpage.verifyDropdownOptions("Add"));
		
	}
	
	
	@Test(dataProvider = "data", dataProviderClass = Datareader.class, priority = 2)
	@ICustomReport(Author = "Rashmi", Suite = "Regression")
	public void EDITOR_COMMON002__AddandRemoveAttachment(HashMap<String, String> testdata) {
		
		homepage = loginpage.Login(testdata.get("UserName1"), testdata.get("Password1"));
		specpage = homepage.NavigateToSpecificationPage();
		Assert.assertNotNull(specpage, "Navigate to the specification page");
		
		// Create a new specification
		newspecpage = specpage.NavigateToNewSpecification();
		Assert.assertNotNull(newspecpage, "Navigated to new specification page");
		
		Assert.assertTrue(newspecpage.AddGeneralSettings(testdata.get("Title"), testdata.get("Class"), testdata.get("SubClass"), testdata.get("Department"))
				, "General settings data is added");
		Assert.assertTrue(newspecpage.Next(), "Clicked on Next button");
		Assert.assertTrue(newspecpage.SelectWorkFlow(testdata.get("WorkflowID")), "Selected workflow ID: " + testdata.get("WorkflowID"));
		Assert.assertTrue(newspecpage.Next(), "Clicked on Next button");
		Assert.assertTrue(newspecpage.CreateSpec(), "Clicked on Create button");
		
		Assert.assertNotNull(newspecpage.VerifySuccessMsg(), "Verified the message: " + newspecpage.VerifySuccessMsg());
		
		Assert.assertNotNull(newspecpage.GetCreateSpecID(), "Specification is created: " + newspecpage.GetCreateSpecID());		
		ini.WriteIniFile(TestUtil.Environment, "EnvironmentSpecID", newspecpage.GetCreateSpecID());
		
		// Lock for edit and create a Content Change task
		Assert.assertTrue(newspecpage.LockForEdit(testdata.get("Parameter3")), "Locked for Spec Edit: " + testdata.get("Parameter3"));
		
		// Select the created task 
		taskpage = homepage.NavigateToTaskPage();
		Assert.assertNotNull(taskpage, "");
		Assert.assertTrue(taskpage.SearchBySpecID(ini.ReadIniFile(TestUtil.Environment, "EnvironmentSpecID")), "Search for the spec ID");
		Assert.assertTrue(taskpage.SelectTask(ini.ReadIniFile(TestUtil.Environment, "EnvironmentSpecID")), "Task is selected");
		Assert.assertTrue(taskpage.verifyTaskTitleInTaskDetails(testdata.get("Parameter4")), "Task Title is Edit Content Specification");
		
		// Confirm Edit SPecification action
		editormainpage = taskpage.confirmEditSpecificationAction();
		Assert.assertNotNull(editormainpage);
		
		// Open attachment window and add an attachment
		attachmentwindow = editormainpage.openAttachmentWindow();
		Assert.assertTrue(attachmentwindow.chooseAttachment(testdata.get("Parameter1")), "Attachment is choosen");
		
		editormainpage = attachmentwindow.uploadChoosenAttachment();
		Assert.assertNotNull(editormainpage);
		
		// View the added attachment
		Assert.assertNotNull(editormainpage.viewReferenceOrAttachment("Attachments", testdata.get("Parameter2")), "Verified viewing attachment");
		
		// Delete the attachment
		Assert.assertTrue(editormainpage.deleteReferenceOrAttachment("Attachments", testdata.get("Parameter2"), "Delete Attachment"), "Delete dialog is displayed");
		Assert.assertTrue(editormainpage.confirmDelete(), "Attachment is deleted");
		
	}

	
	@Test(dataProvider = "data", dataProviderClass = Datareader.class, priority = 3)
	@ICustomReport(Author = "Rashmi", Suite = "Regression")
	public void EDITOR_COMMON003__AddandRemoveReference(HashMap<String, String> testdata) {
		
		homepage = loginpage.Login(testdata.get("UserName1"), testdata.get("Password1"));
		specpage = homepage.NavigateToSpecificationPage();
		Assert.assertNotNull(specpage, "Navigate to the specification page");
		
		// Create a new specification
		newspecpage = specpage.NavigateToNewSpecification();
		Assert.assertNotNull(newspecpage, "Navigated to new specification page");
		
		Assert.assertTrue(newspecpage.AddGeneralSettings(testdata.get("Title"), testdata.get("Class"), testdata.get("SubClass"), testdata.get("Department"))
				, "General settings data is added");
		Assert.assertTrue(newspecpage.Next(), "Clicked on Next button");
		Assert.assertTrue(newspecpage.SelectWorkFlow(testdata.get("WorkflowID")), "Selected workflow ID: " + testdata.get("WorkflowID"));
		Assert.assertTrue(newspecpage.Next(), "Clicked on Next button");
		Assert.assertTrue(newspecpage.CreateSpec(), "Clicked on Create button");
		
		Assert.assertNotNull(newspecpage.VerifySuccessMsg(), "Verified the message: " + newspecpage.VerifySuccessMsg());
		
		Assert.assertNotNull(newspecpage.GetCreateSpecID(), "Specification is created: " + newspecpage.GetCreateSpecID());		
		ini.WriteIniFile(TestUtil.Environment, "EnvironmentSpecID", newspecpage.GetCreateSpecID());
		
		// Lock for edit and create a Content Change task
		Assert.assertTrue(newspecpage.LockForEdit(testdata.get("Parameter3")), "Locked for Spec Edit: " + testdata.get("Parameter3"));
		
		// Select the created task 
		taskpage = homepage.NavigateToTaskPage();
		Assert.assertNotNull(taskpage, "");
		Assert.assertTrue(taskpage.SearchBySpecID(ini.ReadIniFile(TestUtil.Environment, "EnvironmentSpecID")), "Search for the spec ID");
		Assert.assertTrue(taskpage.SelectTask(ini.ReadIniFile(TestUtil.Environment, "EnvironmentSpecID")), "Task is selected");
		Assert.assertTrue(taskpage.verifyTaskTitleInTaskDetails(testdata.get("Parameter4")), "Task Title is Edit Content Specification");
		
		// Confirm Edit Specification action
		editormainpage = taskpage.confirmEditSpecificationAction();
		Assert.assertNotNull(editormainpage);
		
		// Open reference window
		referencewindow = editormainpage.openReferenceWindow();
		Assert.assertNotNull(referencewindow, "Reference window is opened");
		
		// Search reference
		Assert.assertTrue(referencewindow.enterSpecID("M08-00000413"), "Entered spec id");
		Assert.assertTrue(referencewindow.startReferenceSearch(), "Search button is clicked");
		Assert.assertTrue(referencewindow.selectSearchedResult("M08-00000413"), "Spec result is selected");
		
		// Add reference
		editormainpage = referencewindow.addReferenceOk();
		Assert.assertNotNull(editormainpage, "Reference is added");
		
		// View reference
		Assert.assertNotNull(editormainpage.viewReferenceOrAttachment("References", testdata.get("Parameter2")), "Verified viewing attachment");
		
		// Delete the attachment
		Assert.assertTrue(editormainpage.deleteReferenceOrAttachment("References", testdata.get("Parameter2"), "Remove Reference"), "Delete dialog is displayed");
		Assert.assertTrue(editormainpage.confirmDelete(), "Reference is deleted");
				
	}
	
	/*
	@Test(dataProvider = "data", dataProviderClass = Datareader.class, priority = 4)
	@ICustomReport(Author = "Rashmi", Suite = "Regression")
	public void EDITOR_COMMON004__AddandPLMRemoveReference(HashMap<String, String> testdata) {
		
		homepage = loginpage.Login(testdata.get("UserName1"), testdata.get("Password1"));
		specpage = homepage.NavigateToSpecificationPage();
		Assert.assertNotNull(specpage, "Navigate to the specification page");
		
		// Create a new specification
		newspecpage = specpage.NavigateToNewSpecification();
		Assert.assertNotNull(newspecpage, "Navigated to new specification page");
		
		Assert.assertTrue(newspecpage.AddGeneralSettings(testdata.get("Title"), testdata.get("Class"), testdata.get("SubClass"), testdata.get("Department"))
				, "General settings data is added");
		Assert.assertTrue(newspecpage.Next(), "Clicked on Next button");
		Assert.assertTrue(newspecpage.SelectWorkFlow(testdata.get("WorkflowID")), "Selected workflow ID: " + testdata.get("WorkflowID"));
		Assert.assertTrue(newspecpage.Next(), "Clicked on Next button");
		Assert.assertTrue(newspecpage.CreateSpec(), "Clicked on Create button");
		
		Assert.assertNotNull(newspecpage.VerifySuccessMsg(), "Verified the message: " + newspecpage.VerifySuccessMsg());
		
		Assert.assertNotNull(newspecpage.GetCreateSpecID(), "Specification is created: " + newspecpage.GetCreateSpecID());		
		ini.WriteIniFile(TestUtil.Environment, "EnvironmentSpecID", newspecpage.GetCreateSpecID());
		
		// Lock for edit and create a Content Change task
		Assert.assertTrue(newspecpage.LockForEdit(testdata.get("Parameter3")), "Locked for Spec Edit: " + testdata.get("Parameter3"));
		
		// Select the created task 
		taskpage = homepage.NavigateToTaskPage();
		Assert.assertNotNull(taskpage, "");
		Assert.assertTrue(taskpage.SearchBySpecID(ini.ReadIniFile(TestUtil.Environment, "EnvironmentSpecID")), "Search for the spec ID");
		Assert.assertTrue(taskpage.SelectTask(ini.ReadIniFile(TestUtil.Environment, "EnvironmentSpecID")), "Task is selected");
		Assert.assertTrue(taskpage.verifyTaskTitleInTaskDetails(testdata.get("Parameter4")), "Task Title is Edit Content Specification");
		
		
		// Confirm Edit Specification action
		editormainpage = taskpage.confirmEditSpecificationAction();
		Assert.assertNotNull(editormainpage);
	
		// Open reference window
		plmreference = editormainpage.openPLMReferenceWindow();
		Assert.assertNotNull(plmreference, "Reference window is opened");

		// Add PLM Reference
		Assert.assertTrue(plmreference.enterSpecID("M08-00000413"), "Entered spec id");

		editormainpage = plmreference.addPLMReferenceOk();
		Assert.assertNotNull(editormainpage, "PLM Reference is added");
	}
	*/	

	/*
	taskpage = homepage.NavigateToTaskPage();
	Assert.assertNotNull(taskpage, "");
	Assert.assertTrue(taskpage.SearchBySpecID("M08-00000414"), "Search for the spec ID");
	Assert.assertTrue(taskpage.SelectTask("M08-00000414"), "Task is selected");		
	*/
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
