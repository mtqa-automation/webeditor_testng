/**
 * 
 */
package com.setupfc.qa.testcases;

import java.util.HashMap;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.setupfc.qa.EditorCommon.PLMReferenceWindow;
import com.setupfc.qa.Interface.ICustomReport;
import com.setupfc.qa.base.TestBase;
import com.setupfc.qa.config.IniFile;
import com.setupfc.qa.listeners.CustomAssert;
import com.setupfc.qa.listeners.DataproviderListener;
import com.setupfc.qa.listeners.TestNGlisteners;
import com.setupfc.qa.pages.BaseEditor.AttachmentWindow;
import com.setupfc.qa.pages.BaseEditor.ReferenceWindow;
import com.setupfc.qa.pages.BaseEditor.WebbaseMainPage;
import com.setupfc.qa.pages.SetupFC.HomePage;
import com.setupfc.qa.pages.SetupFC.LoginPage;
import com.setupfc.qa.pages.SetupFC.TaskPage;
import com.setupfc.qa.pages.SetupFC.WebEditorPage;
import com.setupfc.qa.testdata.Datareader;

/**
 * @author smallika
 *
 */
@Listeners({ TestNGlisteners.class, DataproviderListener.class })
public class WebBaseTest extends TestBase {
	IniFile ini = IniFile.GetInstance();
	LoginPage loginpage;
	HomePage homepage;
	TaskPage taskpage;
	WebEditorPage webpage;
	WebbaseMainPage webbasemainpage;
	AttachmentWindow attchmntWndw;
	ReferenceWindow ReferenceWndw;
	//PLMReferenceWindow plmreferencewindow;

	@BeforeMethod
	public void setup() {
		initilization();
		loginpage = new LoginPage();
		homepage = loginpage.LoginbySingleSignOn();
	}

	@AfterMethod
	public void TearDown() {
		CloseBrowser();
	}

	@Test(dataProvider = "data", dataProviderClass = Datareader.class, enabled = false)
	@ICustomReport(Author = "Siddharth", Suite = "Specification")
	public void WEBASE001__AddMandate_fields(HashMap<String, String> testdata) {
		CustomAssert Assert = new CustomAssert();

		Assert.assertNotNull(taskpage = homepage.NavigateToTaskPage(), "Navigated to Task Tab");
		Assert.assertTrue(taskpage.SearchBySpecID(testdata.get("ID")), "Search for the spec ID");
		Assert.assertTrue(taskpage.SelectTask(""), "Task is selected");
		// Assert.assertNotNull(
		// webpage=taskpage.confirmEditSpecificationAction(),"Opened the Specification
		// in Web editor");
		Assert.assertNotNull(webbasemainpage = webpage.SwitchToWebBaseEditor(), "Navigated to Web Base editor");
		Assert.assertNotNull(webbasemainpage.GetEditorTitle(), "Verified the Web editor title");
		Assert.assertNotNull(webbasemainpage.GetLoggedUserID(), "Verified the Logged in user");
		Assert.assertTrue(webbasemainpage.SelectSpecificationOption("Validate"),
				"Selected the Specification drop down option ");
		Assert.assertTrue(webbasemainpage.AddPCRB(testdata.get("PCRB")), "Added PCRB values" + testdata.get("PCRB"));
		Assert.assertTrue(webbasemainpage.AddChangeReason(testdata.get("Change-Reason")),
				"Added Change reason values" + testdata.get("Change-Reason"));
		Assert.assertTrue(webbasemainpage.AddChangeDescription(testdata.get("Change-Desc")),
				"Added Change description values" + testdata.get("Change-Desc"));
		Assert.assertTrue(webbasemainpage.AddPurpose(testdata.get("Purpose")),
				"Added purpose values" + testdata.get("Purpose"));
		Assert.assertTrue(webbasemainpage.AddScope(testdata.get("Scope")),
				"Added scope values" + testdata.get("Scope"));
		Assert.assertTrue(webbasemainpage.ValidateSpecification(), "Clicked on specification validation");
		Assert.assertAll();
	}

	@Test(dataProvider = "data", dataProviderClass = Datareader.class, enabled = false)
	@ICustomReport(Author = "Siddharth", Suite = "Specification")
	public void WEBASE002__VerifyMandate(HashMap<String, String> testdata) {
		CustomAssert Assert = new CustomAssert();
		Assert.assertNotNull(taskpage = homepage.NavigateToTaskPage(), "Navigated to Task Tab");
		Assert.assertTrue(taskpage.SearchBySpecID(testdata.get("ID")), "Search for the spec ID");
		Assert.assertTrue(taskpage.SelectTask(""), "Task is selected");
		// Assert.assertNotNull(
		// webpage=taskpage.confirmEditSpecificationAction(),"Opened the Specification
		// in Web editor");
		Assert.assertNotNull(webbasemainpage = webpage.SwitchToWebBaseEditor(), "Navigated to Web Base editor");
		Assert.assertTrue(webbasemainpage.AddPCRB(""), "Remove PCRB values");
		Assert.assertTrue(webbasemainpage.AddChangeReason(""), "Removed Change reason values");
		Assert.assertTrue(webbasemainpage.AddChangeDescription(""), "Removed Change description values");
		Assert.assertTrue(webbasemainpage.AddPurpose(""), "Removed purpose values");
		Assert.assertTrue(webbasemainpage.AddScope(""), "Removed scope values");
		Assert.assertTrue(webbasemainpage.ValidateSpecification(), "Clicked on specification validation");
		Assert.assertEquals(webbasemainpage.Verify_ChangeDescErrorMsg(), webbasemainpage.Verify_ChangeDescErrorMsg(),
				"Verified the Change Description error message: ");
		Assert.assertEquals(webbasemainpage.Verify_ChangeReasonErrorMsg(),
				webbasemainpage.Verify_ChangeReasonErrorMsg(), "Verified the Change Reason  error message: ");
		Assert.assertEquals(webbasemainpage.Verify_PurposeErrorMsg(), webbasemainpage.Verify_PurposeErrorMsg(),
				"Verified the Purpose  error message: ");
		Assert.assertEquals(webbasemainpage.Verify_ScopeErrorMsg(), webbasemainpage.Verify_ScopeErrorMsg(),
				"Verified the Scope  error message: ");
		Assert.assertAll();
	}

	@Test(dataProvider = "data", dataProviderClass = Datareader.class, enabled = false)
	@ICustomReport(Author = "Siddharth", Suite = "Specification")
	public void WEBASE002__Verify_specification_Header_details_from_web_editor(HashMap<String, String> testdata) {
		CustomAssert Assert = new CustomAssert();
		Assert.assertNotNull(taskpage = homepage.NavigateToTaskPage(), "Navigated to Task Tab");
		Assert.assertTrue(taskpage.SearchBySpecID(testdata.get("ID")), "Search for the spec ID");
		Assert.assertTrue(taskpage.SelectTask(""), "Task is selected");
		// Assert.assertNotNull(
		// webpage=taskpage.confirmEditSpecificationAction(),"Opened the Specification
		// in Web editor");
		Assert.assertNotNull(webbasemainpage = webpage.SwitchToWebBaseEditor(), "Switching to web editor");
		Assert.assertNotNull(webbasemainpage, "Navigated to Web Base editor");
		Assert.VerifyContent(testdata.get("ID"), webbasemainpage.GetMetadata_SpecIDwithVersion(),
				"Verified the Specification ID");
		Assert.VerifyContent(testdata.get("Parameter2"), webbasemainpage.GetMetadata_Status(),
				"Verified the Specification status");
		Assert.VerifyContent(testdata.get("Title"), webbasemainpage.GetMetadata_Name(),
				"Verified the Specification Title");
		Assert.VerifyContent(testdata.get("Author"), webbasemainpage.GetMetadata_Author(),
				"Verified the Specification Author");
		Assert.VerifyContent(testdata.get("Department"), webbasemainpage.GetMetadata_Depart(),
				"Verified the Specification depart");
		Assert.VerifyContent(testdata.get("Owner"), webbasemainpage.GetMetadata_Owner(),
				"Verified the Specification Owner");
		Assert.VerifyContent(testdata.get("WorkflowID"), webbasemainpage.GetMetadata_Workflow(),
				"Verified the Specification workflow");
		Assert.VerifyContent(testdata.get("SubClass"), webbasemainpage.GetMetadata_Subclass(),
				"Verified the Specification Subclass");
		Assert.VerifyContent(testdata.get("Parameter3"), webbasemainpage.GetMetadata_Template(),
				"Verified the Specification Template");
		Assert.VerifyContent(testdata.get("Parameter1"), webbasemainpage.GetMetadata_UseCase(),
				"Verified the Specification UseCase");
		Assert.assertAll();
	}

	@Test(dataProvider = "data", dataProviderClass = Datareader.class, enabled = false)
	@ICustomReport(Author = "Siddharth", Suite = "Specification")
	public void WEBASE003__common_validate_mainview_details(HashMap<String, String> testdata) {
		CustomAssert Assert = new CustomAssert();

		Assert.assertNotNull(taskpage = homepage.NavigateToTaskPage(), "Navigated to Task Tab");
		Assert.assertTrue(taskpage.SearchBySpecID(testdata.get("ID")), "Search for the spec ID");
		Assert.assertTrue(taskpage.SelectTask(""), "Task is selected");
		// Assert.assertNotNull(
		// webpage=taskpage.confirmEditSpecificationAction(),"Opened the Specification
		// in Web editor");
		Assert.assertNotNull(webbasemainpage = webpage.SwitchToWebBaseEditor(), "Switching to web editor");
		Assert.assertNotNull(webbasemainpage, "Navigated to Web Base editor");
		Assert.assertTrue(webbasemainpage.AddChangeReason(""), "Removed Change reason values");
		Assert.assertTrue(webbasemainpage.AddChangeDescription(""), "Removed Change description values");
		Assert.assertTrue(webbasemainpage.AddPurpose(""), "Removed purpose values");
		Assert.assertTrue(webbasemainpage.AddScope(""), "Removed scope values");
		Assert.assertTrue(webbasemainpage.ValidateSpecification(), "Clicked on specification validation");

		Assert.VerifyContent(testdata.get("Parameter1"), webbasemainpage.Verify_ChangeReasonErrorMsg(),
				"Verified the Change Reason  error message: ");
		Assert.VerifyContent(testdata.get("Parameter2"), webbasemainpage.Verify_ChangeDescErrorMsg(),
				"Verified the Change Description error message: ");
		Assert.VerifyContent(testdata.get("Parameter3"), webbasemainpage.Verify_PurposeErrorMsg(),
				"Verified the Purpose  error message: ");
		Assert.VerifyContent(testdata.get("Parameter4"), webbasemainpage.Verify_ScopeErrorMsg(),
				"Verified the Scope  error message: ");
		Assert.assertTrue(webbasemainpage.AddPCRB(testdata.get("Parameter5")),
				"Add PCRB values with more than 1024 char");
		Assert.VerifyContent(testdata.get("Parameter6"), webbasemainpage.Verify_PCRBErrorMsg(),
				"Verified the Change Reason  error message");
		Assert.assertTrue(webbasemainpage.AddChangeReason(testdata.get("Parameter5")),
				"Add Change Reason values with more than 1024 char");
		Assert.VerifyContent(testdata.get("Parameter6"), webbasemainpage.Verify_ChangeReasonErrorMsg(),
				"Verified the Change Reason  error message");
		Assert.assertTrue(webbasemainpage.AddChangeDescription(testdata.get("Parameter5")),
				"Add Change Description values with more than 1024 char");
		Assert.VerifyContent(testdata.get("Parameter6"), webbasemainpage.Verify_ChangeDescErrorMsg(),
				"Verified the Change Description error message: ");
		Assert.assertTrue(webbasemainpage.AddPurpose(testdata.get("Parameter5")),
				"Add Purpose values with more than 1024 char");
		Assert.assertTrue(webbasemainpage.Verify_PurposeNoErrorMsg(), "Verified the Purpose No error message ");
		Assert.assertTrue(webbasemainpage.AddScope(testdata.get("Parameter5")),
				"Add Scope values with more than 1024 char");
		Assert.assertTrue(webbasemainpage.Verify_ScopeNoErrorMsg(), "Verified the Scope No error message");
		Assert.assertTrue(webbasemainpage.AddDefinitions(testdata.get("Parameter5")),
				"Add Change Description values with more than 1024 char");
		Assert.assertAll();
	}

	@Test(dataProvider = "data", dataProviderClass = Datareader.class, invocationCount = 10, threadPoolSize = 10)
	@ICustomReport(Author = "Siddharth", Suite = "Specification")
	public void WEBASE004__Validate_Add_andRemove_Reference(HashMap<String, String> testdata) {
		CustomAssert Assert = new CustomAssert();
		Assert.assertNotNull(taskpage = homepage.NavigateToTaskPage(), "Navigated to Task Tab");
		Assert.assertTrue(taskpage.SearchBySpecID(testdata.get("ID")), "Search for the spec ID");
		Assert.assertTrue(taskpage.SelectTask(""), "Task is selected");
		// Assert.assertNotNull(
		// webpage=taskpage.confirmEditSpecificationAction(),"Opened the Specification
		// in Web editor");
		Assert.assertNotNull(webbasemainpage = webpage.SwitchToWebBaseEditor(), "Switching to web editor");
		Assert.assertNotNull(webbasemainpage, "Navigated to Web Base editor");
		Assert.assertNotNull(ReferenceWndw = webbasemainpage.NavigateToReference(), "Selected Add Reference Option");
		Assert.VerifyContent("Append References", ReferenceWndw.GetHeaderTitle(), "Verified the reference header");
		ReferenceWndw.SearchReferenceByID("M08-00000151");
		// System.out.println( ReferenceWndw.GetRefernceDetails("M08-00000151"));

		Assert.assertAll();
	}

	@Test(dataProvider = "data", dataProviderClass = Datareader.class, enabled = false)
	@ICustomReport(Author = "Siddharth", Suite = "Specification")
	public void WEBASE006__Validate_Add_andRemove_attachments(HashMap<String, String> testdata) {
		CustomAssert Assert = new CustomAssert();
		Assert.assertNotNull(taskpage = homepage.NavigateToTaskPage(), "Navigated to Task Tab");
		Assert.assertTrue(taskpage.SearchBySpecID(testdata.get("ID")), "Search for the spec ID");
		Assert.assertTrue(taskpage.SelectTask(""), "Task is selected");
		// Assert.assertNotNull(
		// webpage=taskpage.confirmEditSpecificationAction(),"Opened the Specification
		// in Web editor");
		Assert.assertNotNull(webbasemainpage = webpage.SwitchToWebBaseEditor(), "Switching to web editor");
		Assert.assertNotNull(webbasemainpage, "Navigated to Web Base editor");

		Assert.assertNotNull(attchmntWndw = webbasemainpage.NavigateToAttachament(), "Selected Add attachment Option");
		attchmntWndw.ChooseFile(testdata.get("Parameter1"));
		Assert.VerifyContent(testdata.get("Parameter2"), attchmntWndw.GetAddedFileNames(), "Verified Added file name");
		System.out.println(attchmntWndw.GetAddedFileNames());
		Assert.assertNotNull(attchmntWndw.Uploadfile(), "Attachments uploaded successfully");
		Assert.assertNotNull(webbasemainpage.ViewAttachment(testdata.get("Parameter2")),
				"Verified viewing the attachment");
		Assert.assertTrue(webbasemainpage.AttachmentDelete(testdata.get("Parameter2")),
				"Selected the delete Attachment option");
		Assert.assertTrue(webbasemainpage.Delete_Cancel(), "Cancelled Deleting the attachment");
		Assert.assertTrue(webbasemainpage.AttachmentDelete(testdata.get("Parameter2")),
				"Selected the delete Attachment option");
		Assert.assertTrue(webbasemainpage.Delete_Confirm(), "Deleted the attachment");
		Assert.assertAll();
	}

}
